﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace MrDir.Controls
{
	internal static class VisualHelperExtensions
	{
		public static T GetVisualChild<T>(this Visual parent) where T : Visual
		{
			var child = default(T);
			var numVisuals = VisualTreeHelper.GetChildrenCount(parent);
			for (var i = 0; i < numVisuals; i++)
			{
				var v = (Visual)VisualTreeHelper.GetChild(parent, i);
				child = v as T ?? GetVisualChild<T>(v);

				if (child != null) break;
			}

			return child;
		}

		public static bool IsMouseInBounds(this FrameworkElement fe, MouseEventArgs e)
		{
			var bounds = new Rect(0, 0, fe.ActualWidth, fe.ActualHeight);
			return bounds.Contains(e.GetPosition(fe));
		}

		public static void ShiftSelections(this DataGrid grid, bool down)
		{
			int sel;
			int idx;

			var selections = grid.SelectedRowIndexes();
			var data = grid.DataList();
			if (data == null
					|| selections.Count <= 0) return;
			if (down)
			{
				sel = selections.Last();
				idx = sel + 1;
				if (idx < 0
						|| idx >= data.Count) return;
				while (idx >= 0)
				{
					idx = data.ShiftUnselectedRowUp(selections, idx) - 1;
					while (idx > 0
								 && !selections.Contains(idx - 1))
						idx--;
				}
			}
			else
			{
				sel = selections.First();

				if (sel <= 0) return;
				while (sel < data.Count)
				{
					sel = data.ShiftUnselectedRowDown(selections, sel - 1) + 1;
					while (sel < data.Count
								 && !selections.Contains(sel))
						sel++;
				}
			}
		}

		private static int ShiftUnselectedRowUp(this IList data, List<int> selections, int idx)
		{
			var prev = selections.PrevInsertionIndex(idx);
			if (prev < 0
					|| prev == idx) return prev;

			var row = data[idx];
			data.RemoveAt(idx);
			data.Insert(prev, row);

			return prev;
		}

		private static int ShiftUnselectedRowDown(this IList data, List<int> selections, int idx)
		{
			var next = selections.NextInsertionIndex(idx);

			if (next < 0
					|| next >= data.Count
					|| next == idx
					|| idx < 0
					|| idx >= data.Count) return next;
			var row = data[idx];
			data.RemoveAt(idx);
			data.Insert(next, row);

			return next;
		}

		private static int PrevInsertionIndex(this ICollection<int> selections, int idx)
		{
			var ret = -1;
			if (selections == null
					|| idx <= 0) return ret;

			idx--;
			ret = idx;

			while (idx >= 0
						 && selections.Contains(idx))
			{
				ret = idx;
				idx--;
			}

			return ret;
		}

		private static int NextInsertionIndex(this ICollection<int> selections, int idx)
		{
			if (selections == null)
				return -1;

			while (selections.Contains(idx + 1))
				idx++;

			return idx;
		}

		private static List<int> SelectedRowIndexes(this DataGrid grid)
		{
			var ret = new List<int>();

			if (grid != null)
				ret.AddRange(grid.SelectedItems
					.Cast<object>()
					.Select(grid.GetIndexOf)
					.Where(idx => idx >= 0));
			ret.Sort();

			return ret;
		}

		public static IList DataList(this DataGrid grid)
			=> grid?.ItemsSource as IList;

		public static int GetIndexOf(this DataGrid grid, object data)
		{
			var list = grid.DataList();
			return list == null || data == null ? -1 : list.IndexOf(data);
		}

		public static int GetIndexAt(this DataGrid grid, MouseEventArgs e)
			=> grid.GetIndexOf(grid.FindOverItem(e));

		public static object FindOverItem(this DataGrid grid, MouseEventArgs e)
		{
			var row = grid.FindOverRow(e);
			var idx = row?.GetIndex() ?? -1;
			return row?.IsEditing != false ? null : row.Item;
		}

		public static DataGridRow FindOverRow(this DataGrid grid, MouseEventArgs e)
			=> grid.GetAtPoint<DataGridRow>(e);

		public static bool IsOver<T>(this DataGrid grid, MouseEventArgs e) where T : DependencyObject
			=> grid.GetAtPoint<T>(e) != null;

		public static T GetAtPoint<T>(this UIElement reference, MouseEventArgs e) where T : DependencyObject
			=> reference.GetAtPoint<T>(e.GetPosition(reference));

		public static T GetAtPoint<T>(this UIElement reference, Point point) where T : DependencyObject
		{
			if (!(reference?.InputHitTest(point) is DependencyObject element))
				return null;
			if (element is T variable)
				return variable;
			return element.TryFindParent<T>();
		}

		public static T TryFindParent<T>(this DependencyObject child) where T : DependencyObject
		{
			while (true)
			{
				//get parent item
				var parentObject = child.GetParentObject();

				//we've reached the end of the tree
				if (parentObject == null) return null;

				//check if the parent matches the type we're looking for
				if (parentObject is T parent) return parent;
				child = parentObject;
			}
		}

		public static DependencyObject GetParentObject(this DependencyObject child)
		{
			switch (child)
			{
				case null:
					return null;
				case ContentElement contentElement:
					{
						var parent = ContentOperations.GetParent(contentElement);
						if (parent != null)
							return parent;

						var fce = contentElement as FrameworkContentElement;
						return fce?.Parent;
					}
				case FrameworkElement frameworkElement:
					{
						var parent = frameworkElement.Parent;
						if (parent != null)
							return parent;
						break;
					}
			}

			//handle content elements separately

			//also try searching for parent in framework elements (such as DockPanel, etc)

			//if it's not a ContentElement/FrameworkElement, rely on VisualTreeHelper
			return VisualTreeHelper.GetParent(child);
		}

		public static List<T> FindLogicalChildren<T>(this DependencyObject obj) where T : DependencyObject
		{
			var children = new List<T>();
			foreach (var child in LogicalTreeHelper.GetChildren(obj))
			{
				switch (child)
				{
					case T variable:
						children.Add(variable);
						break;
					case DependencyObject dep:
						children.AddRange((child as DependencyObject).FindLogicalChildren<T>()); // recursive
						break;
					case null:
					default:
						continue;
				}
			}

			return children;
		}

		/// <summary>
		///   Gets the list of routed event handlers subscribed to the specified routed event.
		/// </summary>
		/// <param name="element">The UI element on which the event is defined.</param>
		/// <param name="routedEvent">The routed event for which to retrieve the event handlers.</param>
		/// <returns>The list of subscribed routed event handlers.</returns>
		public static RoutedEventHandlerInfo[] GetRoutedEventHandlers(this UIElement element, RoutedEvent routedEvent)
		{
			var routedEventHandlers = default(RoutedEventHandlerInfo[]);
			// Get the EventHandlersStore instance which holds event handlers for the specified element.
			// The EventHandlersStore class is declared as internal.
			var eventHandlersStoreProperty =
				typeof(UIElement).GetProperty("EventHandlersStore", BindingFlags.Instance | BindingFlags.NonPublic);
			if (eventHandlersStoreProperty == null) return null;
			var eventHandlersStore = eventHandlersStoreProperty.GetValue(element, null);

			if (eventHandlersStore == null) return null;
			// Invoke the GetRoutedEventHandlers method on the EventHandlersStore instance
			// for getting an array of the subscribed event handlers.
			var getRoutedEventHandlers = eventHandlersStore.GetType()
				.GetMethod("GetRoutedEventHandlers", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			if (getRoutedEventHandlers != null)
				routedEventHandlers = (RoutedEventHandlerInfo[])getRoutedEventHandlers.Invoke(eventHandlersStore, new object[] { routedEvent });

			return routedEventHandlers;
		}
	}

	public class DataGridAvancedRowClickedEventArgs
		: MouseEventArgs
	{
		public DataGridAvancedRowClickedEventArgs(DataGridRow row, object rowItem, MouseDevice device, int timestamp = 0)
			: base(device, timestamp)
		{
			Row = row;
			RowItem = rowItem;
		}

		public object RowItem { get; }
		public DataGridRow Row { get; }
	}

	public class DataGridAdvanced
		: DataGrid, IDisposable
	{
		#region Constructors

		public DataGridAdvanced()
			=> PreviewMouseDoubleClick += OnMouseDoubleClick;

		#endregion

		public void Dispose()
			=> PreviewMouseDoubleClick -= OnMouseDoubleClick;

		#region Events

		private void OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (!(sender is DataGrid grid))
				throw new Exception($"Control is not of type {nameof(DataGrid)}");

			var row = grid.FindOverRow(e);
			if (row == null
					&& grid.SelectedItems.Count != 1)
				return;

			OnRowDoubleClicked(new DataGridAvancedRowClickedEventArgs(row, grid.CurrentItem, e.MouseDevice, e.Timestamp));
		}

		#endregion

		#region Custom Events

		public event EventHandler<DataGridAvancedRowClickedEventArgs> RowDoubleClicked;

		protected virtual void OnRowDoubleClicked(DataGridAvancedRowClickedEventArgs e)
			=> RowDoubleClicked?.Invoke(this, e);

		#endregion
	}
}