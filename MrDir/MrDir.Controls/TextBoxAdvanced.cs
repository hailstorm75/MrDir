﻿using System.Windows;
using System.Windows.Controls;

namespace MrDir.Controls
{
  public class TextBoxAdvanced
    : TextBox
  {
    public static readonly DependencyProperty SelectionLengthBindableProperty =
      DependencyProperty.Register(nameof(SelectionLengthBindable), typeof(int),
        typeof(TextBoxAdvanced), new PropertyMetadata(default(int)));

    public int SelectionLengthBindable
    {
      get => (int)GetValue(SelectionLengthBindableProperty);
      set => SetValue(SelectionLengthBindableProperty, value);
    }

    protected override void OnGotFocus(RoutedEventArgs e)
    {
      Select(SelectionStart, SelectionLengthBindable);
      if (IsKeyboardFocused)
        BringIntoView();
    }
  }
}
