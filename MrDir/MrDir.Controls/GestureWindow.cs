﻿using System;
using System.Windows;
using System.Windows.Input;
using MahApps.Metro.Controls;
using MrDir.Enums;

namespace MrDir.Controls
{
  public class GestureWindow
    : MetroWindow
  {
    public GestureWindow()
    {
      TouchUp += OnTouchUp;
      TouchDown += OnTouchDown;
    }

    ~GestureWindow()
    {
      TouchUp -= OnTouchUp;
      TouchDown -= OnTouchDown;
    }

    private TouchPoint m_touchStart;
    private DateTime m_touchStartTime;
    private static readonly TimeSpan MAX_SWIPE_DURATION = TimeSpan.FromSeconds(1);

    private void OnTouchDown(object sender, TouchEventArgs e)
    {
      m_touchStart = e.GetTouchPoint(this);
      m_touchStartTime = DateTime.Now;
    }

    private void OnTouchUp(object sender, TouchEventArgs e)
    {
      if (DateTime.Now - m_touchStartTime > MAX_SWIPE_DURATION)
        return;
      var touch = e.GetTouchPoint(this);

      if (m_touchStart != null && (touch.Position.X - m_touchStart.Position.X > 50
        || touch.Position.Y - m_touchStart.Position.Y > 50))
        OnSwiped(new GestureEventArgs(m_touchStart.Position, touch.Position));
    }

    public event EventHandler<GestureEventArgs> Swiped;

    protected virtual void OnSwiped(GestureEventArgs e)
      => Swiped?.Invoke(this, e);
  }

  public class GestureEventArgs
  {
    public Directions Direction { get; }
    public Point From { get; }
    public Point To { get; }

    public GestureEventArgs(Point from ,Point to)
    {
      From = from;
      To = to;
      Direction = DetermineDirection(from, to);
    }

    private const double SWIPE_THRESHOLD = 50d;

    private static Directions DetermineDirection(Point @from ,Point to)
    {
      var verticalMovement = Movement(from.Y, to.Y);
      var horizontalMovement = Movement(from.X, to.X);
      if (verticalMovement > SWIPE_THRESHOLD && verticalMovement > horizontalMovement)
      {
        var direction = from.Y - to.Y;
        if (direction > 0)
          return Directions.Up;
        if (direction < 0)
          return Directions.Down;
      }
      else if (horizontalMovement > SWIPE_THRESHOLD && horizontalMovement > verticalMovement)
      {
        var direction = from.X - to.X;
        if (direction > 0)
          return Directions.Left;
        if (direction < 0)
          return Directions.Right;
      }

      return Directions.None;
    }

    private static double Movement(double @from, double @to)
      => Math.Abs(@from - @to);
  }
}
