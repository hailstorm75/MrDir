﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace MrDir.Behaviors
{
  public class TextBoxEnterKeyUpdateBehavior
		: Behavior<TextBox>
  {
    protected override void OnAttached()
    {
      if (AssociatedObject == null) return;
      base.OnAttached();
      AssociatedObject.KeyDown += AssociatedObject_KeyDown;
    }

    protected override void OnDetaching()
    {
      if (AssociatedObject == null) return;
      AssociatedObject.KeyDown -= AssociatedObject_KeyDown;
      base.OnDetaching();
    }

    private static void AssociatedObject_KeyDown(object sender, KeyEventArgs e)
    {
      if (!(sender is TextBox textBox)) return;
      if (e.Key != Key.Return) return;
      if (e.Key == Key.Enter)
        textBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
    }
  }
}