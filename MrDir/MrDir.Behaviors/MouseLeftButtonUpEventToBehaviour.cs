﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MrDir.Behaviors
{
	public class MouseLeftButtonUpEventToBehaviour
	{
		public static DependencyProperty CommandProperty =
				DependencyProperty.RegisterAttached("Command",
				typeof(ICommand),
				typeof(MouseLeftButtonUpEventToBehaviour),
				new UIPropertyMetadata(CommandChanged));

		public static DependencyProperty CommandParameterProperty =
				DependencyProperty.RegisterAttached("CommandParameter",
				typeof(object),
				typeof(MouseLeftButtonUpEventToBehaviour),
				new UIPropertyMetadata(null));

		public static void SetCommand(DependencyObject target, ICommand value)
			=> target.SetValue(CommandProperty, value);

		public static void SetCommandParameter(DependencyObject target, object value)
			=> target.SetValue(CommandParameterProperty, value);

		public static object GetCommandParameter(DependencyObject target)
			=> target.GetValue(CommandParameterProperty);

		private static void CommandChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
		{
			if (target is Control control)
			{
				if ((e.NewValue != null) && (e.OldValue == null))
					control.MouseLeftButtonUp += OnMouseLeftButtonUp;
				else if ((e.NewValue == null) && (e.OldValue != null))
					control.MouseLeftButtonUp -= OnMouseLeftButtonUp;
			}
		}

		private static void OnMouseLeftButtonUp(object sender, RoutedEventArgs e)
		{
			var control = sender as Control;
			var command = (ICommand)control.GetValue(CommandProperty);
			var commandParameter = control.GetValue(CommandParameterProperty);

			if (sender is TreeViewItem treeViewItem)
			{
				if (!(treeViewItem).IsSelected)
					return;
			}

			if (command.CanExecute(commandParameter))
				command.Execute(commandParameter);
		}
	}
}
