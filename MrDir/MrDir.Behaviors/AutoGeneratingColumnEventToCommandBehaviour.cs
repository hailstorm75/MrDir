﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MrDir.Behaviors
{
  public class AutoGeneratingColumnEventToCommandBehaviour
  {
    public static readonly DependencyProperty CommandProperty =
      DependencyProperty.RegisterAttached(
        "Command",
        typeof(ICommand),
        typeof(AutoGeneratingColumnEventToCommandBehaviour),
        new PropertyMetadata(
          null,
          CommandPropertyChanged));

    public static void SetCommand(DependencyObject o, ICommand value)
    {
      o.SetValue(CommandProperty, value);
    }

    public static ICommand GetCommand(DependencyObject o)
    {
      return o.GetValue(CommandProperty) as ICommand;
    }

    private static void CommandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      if (!(d is DataGrid dataGrid)) return;
      if (e.OldValue != null)
        dataGrid.AutoGeneratingColumn -= OnAutoGeneratingColumn;
      if (e.NewValue != null)
        dataGrid.AutoGeneratingColumn += OnAutoGeneratingColumn;
    }

    private static void OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
    {
      if (!(sender is DependencyObject dependencyObject)) return;
      if (dependencyObject.GetValue(CommandProperty) is ICommand command
          && command.CanExecute(e))
        command.Execute(e);
    }
  }
}