﻿using MrDir.Controls;
using System.Windows;
using System.Windows.Input;

namespace MrDir.Behaviors
{
	public class SwipedEventToBehaviourCommand
	{
		public static readonly DependencyProperty CommandProperty =
			DependencyProperty.RegisterAttached(
				"Command",
				typeof(ICommand),
				typeof(SwipedEventToBehaviourCommand),
				new PropertyMetadata(
					null,
					CommandPropertyChanged));

		public static void SetCommand(DependencyObject o, ICommand value)
		{
			o.SetValue(CommandProperty, value);
		}

		public static ICommand GetCommand(DependencyObject o)
		{
			return o.GetValue(CommandProperty) as ICommand;
		}

		private static void CommandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (!(d is GestureWindow dataGrid)) return;
			if (e.OldValue != null)
				dataGrid.Swiped -= OnSwiped;
			if (e.NewValue != null)
				dataGrid.Swiped += OnSwiped;
		}

		private static void OnSwiped(object sender, GestureEventArgs e)
		{
			if (!(sender is DependencyObject dependencyObject)) return;
			if (dependencyObject.GetValue(CommandProperty) is ICommand command
					&& command.CanExecute(e))
				command.Execute(e);
		}
	}
}
