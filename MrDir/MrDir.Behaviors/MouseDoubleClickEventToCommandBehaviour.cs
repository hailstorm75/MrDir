﻿using System.Windows;
using System.Windows.Input;
using MrDir.Controls;

namespace MrDir.Behaviors
{
  public class MouseDoubleClickEventToCommandBehaviour
  {
    public static readonly DependencyProperty CommandProperty =
      DependencyProperty.RegisterAttached(
        "Command",
        typeof(ICommand),
        typeof(MouseDoubleClickEventToCommandBehaviour),
        new PropertyMetadata(
          null,
          CommandPropertyChanged));

    public static void SetCommand(DependencyObject o, ICommand value)
    {
      o.SetValue(CommandProperty, value);
    }

    public static ICommand GetCommand(DependencyObject o)
    {
      return o.GetValue(CommandProperty) as ICommand;
    }

    private static void CommandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      if (!(d is DataGridAdvanced dataGrid)) return;
      if (e.OldValue != null)
        dataGrid.RowDoubleClicked -= OnAutoGeneratingColumn;
      if (e.NewValue != null)
        dataGrid.RowDoubleClicked += OnAutoGeneratingColumn;
    }

    private static void OnAutoGeneratingColumn(object sender, MouseEventArgs e)
    {
      if (!(sender is DependencyObject dependencyObject)) return;
      if (dependencyObject.GetValue(CommandProperty) is ICommand command
          && command.CanExecute(e))
        command.Execute(e);
    }
  }
}