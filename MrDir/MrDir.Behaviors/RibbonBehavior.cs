﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Interactivity;
using Fluent;
using MrDir.Interfaces.Models;

namespace MrDir.Behaviors
{
  // Fluent.Ribbon does not support binding of ribbon tab items. As a workaround,
  // we use attached behavior to add a bindable items source, which is an observable
  // collection, as the source for ribbon tab items. The idea here is that we
  // subscribe to the 'CollectionChanged' of the observable collection. Whenever
  // the items in the observable collection has changed, we will update the ribbon
  // tab items accordingly.
  //
  // ref:
  // 01.  https://github.com/fluentribbon/Fluent.Ribbon/issues/1
  public class RibbonBehaviour
    : Behavior<Ribbon>
  {
    private static NotifyCollectionChangedEventHandler m_collectionChangedEventHandler;

    // Static Methods.
    private static void OnTabsChangeCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      // This methods is called when the value of Tabs property has changed.
      // Directions: source -> target.

      if (!(d is RibbonBehaviour behavior)) return;
      var ribbon = behavior.AssociatedObject;
      var oldItems = e.OldValue as ObservableCollection<IRibbonTabItem>;
      var newItems = e.NewValue as ObservableCollection<IRibbonTabItem>;
      // ----

      if (oldItems != null) oldItems.CollectionChanged -= m_collectionChangedEventHandler;

      // Since the tabs in source is changing, we should clear all ribbon
      // tab items in the ribbon.
      ribbon.Tabs.Clear();

      if (newItems == null) return;

      // Subscribe to the event.
      newItems.CollectionChanged += m_collectionChangedEventHandler;
      // Iterate thru the new tabs and add each of them into the ribbon.
      foreach (var item in newItems)
      {
        item.View.Uid = item.Header;
        ribbon.Tabs.Add(item.View);
      }
    }

    private static void OnTabsCollectionChanged(Ribbon ribbon, NotifyCollectionChangedEventArgs args)
    {
      switch (args.Action)
      {
        case NotifyCollectionChangedAction.Add:
          // Iterate thru the new ribbon tab items and add each of them into
          // the ribbon as the last ribbon tab item.
          foreach (IRibbonTabItem item in args.NewItems)
          {
            item.View.Uid = item.Header;
            ribbon.Tabs.Add(item.View);
          }

          break;

        case NotifyCollectionChangedAction.Remove:
          // Iterate thru the old ribbon tab items and remove them from the
          // ribbon.
          foreach (IRibbonTabItem item in args.OldItems) ribbon.Tabs.Remove(item.View);
          break;

        case NotifyCollectionChangedAction.Replace:
        case NotifyCollectionChangedAction.Reset:
        case NotifyCollectionChangedAction.Move:
        default:
          throw new NotImplementedException($"'{args.Action}' action is not supported, yet");
      }
    }

    protected override void OnAttached()
    {
      base.OnAttached();
      m_collectionChangedEventHandler = (s, e) => OnTabsCollectionChanged(AssociatedObject, e);
    }

    protected override void OnDetaching()
    {
      base.OnDetaching();
      m_collectionChangedEventHandler = null;
    }

    // Static Fields.

    #region NOTE: About the EventHandler.

    // Use to keep a reference to the anonymous method to allow unsubscription
    // later.
    //
    //      It is important to notice that you cannot easily unsubscribe from an
    //      event if you used an anonymous function to subscribe to it. To unsubscribe
    //      in this scenario, it is necessary to go back to the code where you
    //      subscribe to the event, store the anonymous method in a delegate variable,
    //      and then add the delegate to the event.
    //
    //      ref:
    //      01. https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/events/how-to-subscribe-to-and-unsubscribe-from-events

    #endregion

    #region TabsProperty

    // DP.
    public static readonly DependencyProperty TabItemsSourceProperty =
      DependencyProperty.Register(nameof(TabItemsSource), typeof(ObservableCollection<IRibbonTabItem>),
        typeof(RibbonBehaviour),
        new FrameworkPropertyMetadata(null, OnTabsChangeCallback) {BindsTwoWayByDefault = true});

    // Getter and setter for DP.
    public ObservableCollection<IRibbonTabItem> TabItemsSource
    {
      get => (ObservableCollection<IRibbonTabItem>) GetValue(TabItemsSourceProperty);
      set => SetValue(TabItemsSourceProperty, value);
    }

    #endregion
  }
}