﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace MrDir.Behaviors
{
	public class ValidationErrorEventTrigger
		: EventTriggerBase<DependencyObject>
	{
		protected override void OnAttached()
		{
			var associatedElement = base.AssociatedObject as FrameworkElement;

			if (base.AssociatedObject is Behavior behavior)
				associatedElement = ((IAttachedObject)behavior).AssociatedObject as FrameworkElement;
			if (associatedElement == null)
				throw new ArgumentException("Validation Error Event trigger can only be associated to framework elements");

			associatedElement.AddHandler(Validation.ErrorEvent, new RoutedEventHandler(this.OnValidationError));
		}

		private void OnValidationError(object sender, RoutedEventArgs args)
			=> base.OnEvent(args);

		protected override string GetEventName()
			=> Validation.ErrorEvent.Name;
	}
}
