﻿using Dragablz.Savablz;
using MrDir.Cultures;
using MrDir.Helpers;
using MrDir.Models;
using MrDir.Properties;
using MrDir.ViewModels;
using MrDir.Views.MainWindows;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace MrDir
{
	/// <summary>
	///   Interaction logic for App.xaml
	/// </summary>
	public partial class App
		: Application
	{
		private static MaterialDesignThemes.Wpf.PaletteHelper m_paletteHelper;
		public static Dispatcher MainThread { get; }
		public static ResourceManager CultureManager {get;}

		static App()
		{
			MainThread = Dispatcher.CurrentDispatcher;
			CultureManager = new ResourceManager("MrDir.Cultures.Resources", typeof(Resources).Assembly);
		}

		public App()
		{
			Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.Default.Language);
			m_paletteHelper = new MaterialDesignThemes.Wpf.PaletteHelper();

			// For MahApps
			//Accents = ThemeManager.Accents.Select(x => new Color(x.Resources["AccentColor"], x.Name))
			//                              .OrderBy(x => x)
			//                              .ToList();

			// For Material Design
			Accents = new MaterialDesignColors.SwatchesProvider().Swatches.Select(x => new Color(x.ExemplarHue.Color, x.Name))
				.OrderBy(x => x)
				.ToList();
		}

		public static Window GetActiveWindow()
			=> Current.Windows.Cast<Window>().FirstOrDefault(x => x.IsActive);

		public static Window GetMainWindow()
			=> Current.MainWindow;

		public static IList<Color> Accents { get; private set; }

		public static void SetAccent(string name)
		{
			// For MahApps
			//MahApps.Metro.ThemeManager.ChangeAppStyle(Current,
			//																									ThemeManager.Accents.FirstOrDefault(x => x.Name == name),
			//																									ThemeManager.DetectAppStyle().Item1);

			m_paletteHelper.ReplacePrimaryColor(name);
		}

		public static void SetTheme(bool light)
		{
			MahApps.Metro.ThemeManager.ChangeAppTheme(Current, light ? "BaseDark" : "BaseLight");
			Fluent.ThemeManager.ChangeAppTheme(Current, light ? "BaseDark" : "BaseLight");
			m_paletteHelper.SetLightDark(light);
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			var mainWindow = new MainWindow();
			mainWindow.Closing += MainWindow_Closing;

			SetTheme(Settings.Default.AppMode);
			SetAccent(Settings.Default.AppAccent);

			//var tabControl = mainWindow.ViewModel.WindowView as Views.WindowContent.SmartWindowView;

			//var layout = JsonConvert.DeserializeObject<IEnumerable<LayoutWindowState<TabContentModel>>>(Settings.Default.Layout);
			//WindowsStateSaver.RestoreWindowsState(tabControl.InitialTabablzControl, layout.ToArray(), x => new TabContentViewModel(x));

			mainWindow.Show();
		}

		private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			var a = ClipboardHelper.Instance.ContainsFileDropList;
			// Saves the layout and exit
			var windowsState =
				WindowsStateSaver.GetWindowsState<TabContentModel, TabContentViewModel>(vm =>
					new TabContentModel(vm.Guid));

			JsonConvert.DefaultSettings = () => new JsonSerializerSettings
			{
				PreserveReferencesHandling = PreserveReferencesHandling.Objects,
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
				Formatting = Newtonsoft.Json.Formatting.Indented
			};

			Settings.Default.Layout = windowsState.First().Child == null
				? null
				: JsonConvert.SerializeObject(windowsState);

			Settings.Default.Save();

			Shutdown();
		}
	}
}