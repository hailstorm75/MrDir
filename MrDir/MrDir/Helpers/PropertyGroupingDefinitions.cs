﻿using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using System;
using System.Collections.Generic;

namespace MrDir.Helpers
{
	public static class PropertyGroupingDefinitions
	{
		private static Dictionary<string, (bool isReadonly, Groups group)> m_definitions = new Dictionary<string, (bool isReadonly, Groups group)>
		{
			{ "System.Size", (true, Groups.File) },
			{ "System.FileAttributes", (false, Groups.File) },
			{ "System.DateModified", (true, Groups.File) },
			{ "System.DateCreated", (true, Groups.File) },
			{ "System.DateAccessed", (true, Groups.File) },
			{ "System.Document.DatePrinted", (true, Groups.File) },
			{ "System.Document.DateCreated", (true, Groups.File) },
			{ "System.Document.DateSaved", (true, Groups.File) },
			{ "System.FileOwner", (false, Groups.File) },
			{ "System.FileName", (false, Groups.File) },
			{ "System.Company", (false, Groups.Origin) },
			{ "System.Author", (false, Groups.Origin) },
			{ "System.Document.LastAuthor", (true, Groups.Origin) },
			{ "System.ComputerName", (false, Groups.Origin) },
			{ "System.Document.Scale", (false, Groups.Content) },
			{ "System.Document.LinksDirty", (true, Groups.Content) },
			{ "System.ContentType", (false, Groups.Content) },
			//{ "System.ItemNameDisplayWithoutExtension", (false, Groups.) },
			//{ "System.ItemFolderNameDisplay", (false, Groups.) },
			//{ "System.ItemTypeText", (false, Groups.) },
			//{ "System.ItemNameDisplay", (false, Groups.) },
			//{ "System.ApplicationName", (false, Groups.) },
			//{ "System.Document.Security", (false, Groups.) },
			//{ "System.NetworkLocation", (false, Groups.) },
			//{ "System.ItemPathDisplayNarrow", (false, Groups.) },
			//{ "System.PerceivedType", (false, Groups.) },
			//{ "System.ItemType", (false, Groups.) },
			//{ "System.ParsingName", (false, Groups.) },
			//{ "System.SFGAOFlags", (false, Groups.) },
			//{ "System.ParsingPath", (false, Groups.) },
			//{ "System.FileExtension", (false, Groups.) },
			//{ "System.ItemDate", (false, Groups.) },
			//{ "System.KindText", (false, Groups.) },
			//{ "System.FileAttributesDisplay", (false, Groups.) },
			//{ "System.ItemParticipants", (false, Groups.) },
			//{ "System.IsShared", (false, Groups.) },
			//{ "System.SharedWith", (false, Groups.) },
			//{ "System.SharingStatus", (false, Groups.) },
			//{ "System.ShareScope", (false, Groups.) },
			//{ "System.Security.EncryptionOwnersDisplay", (false, Groups.) },
			//{ "System.ItemName", (false, Groups.) },
			//{ "System.Shell.SFGAOFlagsStrings", (false, Groups.) },
			//{ "System.Link.TargetSFGAOFlagsStrings", (false, Groups.) },
			//{ "System.OfflineAvailability", (false, Groups.) },
			//{ "System.ZoneIdentifier", (false, Groups.) },
			//{ "System.LastWriterPackageFamilyName", (false, Groups.) },
			//{ "System.AppZoneIdentifier", (false, Groups.) },
			//{ "System.Kind", (false, Groups.) },
			//{ "System.Security.EncryptionOwners", (false, Groups.) },
			//{ "System.ItemFolderPathDisplayNarrow", (false, Groups.) },
			//{ "System.Security.AllowedEnterpriseDataProtectionIdentities", (false, Groups.) },
			//{ "System.ThumbnailCacheId", (false, Groups.) },
			//{ "System.VolumeId", (false, Groups.) },
			//{ "System.ItemAuthors", (false, Groups.) },
			//{ "System.Link.TargetParsingPath", (false, Groups.) },
			//{ "System.Link.TargetSFGAOFlags", (false, Groups.) },
			//{ "System.ItemFolderPathDisplay", (false, Groups.) },
			//{ "System.ItemPathDisplay", (false, Groups.) },
			//{ "System.AppUserModel.ID", (false, Groups.) },
			//{ "System.AppUserModel.ParentID", (false, Groups.) },
			//{ "System.Link.TargetExtension", (false, Groups.) },
			//{ "System.OfflineStatus", (false, Groups.) },
			//{ "System.IsFolder", (false, Groups.) },
			//{ "System.NotUserContent", (false, Groups.) },
			//{ "System.StorageProviderAggregatedCustomStates", (false, Groups.) },
			//{ "System.SyncTransferStatusFlags", (false, Groups.) },
			//{ "System.DateImported", (false, Groups.) },
			//{ "System.ExpandoProperties", (false, Groups.) },
			//{ "System.FilePlaceholderStatus", (false, Groups.) }
		};

		public static (bool isReadonly, string group) GetDefinition(this IShellProperty property)
		{
			if (!m_definitions.ContainsKey(property.CanonicalName))
				return (true, Groups.Other.ToString());
			var (isReadonly, group) = m_definitions[property.CanonicalName];
			return (isReadonly, group.GetGroupName());
		}

		private enum Groups
			: ushort
		{
			Other,
			File,
			Description,
			Origin,
			Content,
			Media,
			Sound
		}

		private static string GetGroupName(this Groups group)
		{
			switch (group)
			{
				case Groups.Other:
					return nameof(Groups.Other);
				case Groups.File:
					return nameof(Groups.File);
				case Groups.Description:
					return nameof(Groups.Description);
				case Groups.Origin:
					return nameof(Groups.Origin);
				case Groups.Content:
					return nameof(Groups.Content);
				case Groups.Media:
					return nameof(Groups.Media);
				case Groups.Sound:
					return nameof(Groups.Sound);
				default:
					throw new Exception();
			}
		}
	}
}
