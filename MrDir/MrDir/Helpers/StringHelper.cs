﻿namespace MrDir.Helpers
{
  public static class StringHelper
  {
    public static string Trim2Length(this string str, int length)
    {
      if (str.Length <= length)
        return str;

      //var reduced = str.RemoveKCharacters(2);
      //if (str.Length <= length)
      //  return reduced;

      return str.Substring(0, length - 3) + "...";
    }
  }
}
