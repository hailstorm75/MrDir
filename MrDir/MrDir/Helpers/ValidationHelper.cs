﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MrDir.Helpers
{
  public static class ValidationHelper
  {
    private static readonly Regex CONTAINS_A_BAD_CHARACTER = new Regex($"[{Regex.Escape(new string(Path.GetInvalidFileNameChars()))}]");

    public static async Task<string> IsNotEmpty(string value)
      => await Task.Run(() =>
           string.IsNullOrEmpty(value)
             ? "Cannot be empty"
             : null);

    public static async Task<string> HasNoWhitespace(string value)
      => await Task.Run(() =>
           value?.Any(char.IsWhiteSpace) == true
             ? "Whitespace characters not allowed"
             : null);

    public static async Task<string> IsInteger(string value)
      => await Task.Run(() =>
           value?.All(char.IsDigit) == true
             ? "Must be an integer"
             : null);

    public static async Task<string> IsValidSystemName(string value)
      => await Task.Run(() =>
           value != null && new string(Path.GetInvalidFileNameChars()).Contains(value)
             ? "Cannot contain \\ / : * ? \" < > | characters"
             : null);
  }
}
