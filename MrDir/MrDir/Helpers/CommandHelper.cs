﻿using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Models;
using MrDir.Managers;
using MrDir.Models.Processes;
using MrDir.Models.Bookmarks;
using MrDir.Views.Dialogs.PropertiesDialogs;
using MrDir.Views.Dialogs.RenameDialogs;
using MrDir.Views.Dialogs;
using MrDir.Enums;
using MrDir.Types;
using MrDir.Types.Observable;
using RenameSingleDialogView = MrDir.Views.Dialogs.RenameDialogs.RenameSingleDialogView;
using RenameDialogView = MrDir.Views.Dialogs.RenameDialogs.RenameDialogView;
using Directory = MrDir.Models.FileSystem.Directory;
using File = MrDir.Models.FileSystem.File;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Collections.Generic;
using SchwabenCode.QuickIO;
using Common.Linq;

namespace MrDir.Helpers
{
  public static class CommandHelper
  {
    #region Selectors

    private static string GetCurrentDirectory()
      => (TabManager.ActiveTab != null)
        ? (TabManager.ActiveTab.Header as ITabHeaderViewModel).ToolTip
        : null;

    private static IEnumerable<IFSEntity> GetUniqueSelection(bool onlyForSinglePanel)
    {
      IEnumerable<IFSEntity> selection;
      if (!onlyForSinglePanel
       && TabManager.IsTabSelectionEnabled
       && TabManager.SelectedTabs.Any())
      {
        var filter = TabManager.SelectedTabs
          .Select(x => x.ContentPanel)
          .OfType<IBrowserPanelViewModel>()
          .Select(x => x.Selection)
          .SelectMany(x => x)
          .DistinctBy(x => x.Path)
          .GroupBy(x => x.Name)
          .GroupBy(x => x.Count() > 1)
          .ToDictionary(x => x.Key, x => x.Select(y => y));
        var conflicts = filter[true].Select(x => x).ToList();

        if (conflicts.Any())
        {
          var dialog = new ResolveFileConflictsDialogView(conflicts) { Owner = App.GetActiveWindow() };
          dialog.ShowDialog();

          var resolved = dialog.Result.ToList();
          if (resolved?.Any() != true)
            return null;

          selection = filter.ContainsKey(false)
            ? resolved.Union(filter[false].SelectMany(x => x))
            : resolved;
        }
        else
          selection = filter[false].SelectMany(x => x);
      }
      else if (TabManager.ActiveTab != null
            && SelectionManager.Instance.AnyIFSEntitySelected)
        selection = SelectionManager.Instance.ActiveSelection.Cast<IFSEntity>();
      else return null;

      return selection;
    }

    private static IEnumerable<IFSEntity> GetSelection(bool onlyForSinglePanel)
    {
      IEnumerable<IFSEntity> selection;

      if (!onlyForSinglePanel
          && TabManager.IsTabSelectionEnabled
          && TabManager.SelectedTabs.Any())
        selection = TabManager.SelectedTabs.Select(x => x.ContentPanel)
                                           .OfType<IBrowserPanelViewModel>()
                                           .Select(x => x.Selection)
                                           .SelectMany(x => x);
      else if (TabManager.ActiveTab != null
               && SelectionManager.Instance.AnyIFSEntitySelected)
        selection = SelectionManager.Instance.ActiveSelection.Cast<IFSEntity>();
      else return null;

      return selection;
    }

    private static IEnumerable<IFSEntity> GetParameter(object parameter)
    {
      switch (parameter)
      {
        case BookmarkToDirectory dir:
          return new Directory(dir.Path).FromSingleItem();
        case BookmarkToFile doc:
          return new File(doc.Path).FromSingleItem();
        default:
          return Enumerable.Empty<IFSEntity>();
      }
    }

    #endregion

    #region Command logic

    private static async Task Paste(object parameter = null)
    {
      var selection = parameter != null
        ? GetParameter(parameter)?.ToList()
        : GetSelection(true)?.ToList();

      var path = selection?.Count == 1
        ? selection.First().Path
        : GetCurrentDirectory();

      var data = Clipboard.GetFileDropList()
                          ?.Cast<string>();

      var toOverwrite = new LinkedList<(string source, string overwrite)>(await PasteRecursive(data, path).ConfigureAwait(false));
      if (toOverwrite.Count != 0)
        await App.MainThread.InvokeAsync(async () =>
        {
          var dialog = new ResolveOverwriteConflictsDialogView(toOverwrite);
          dialog.Owner = App.GetActiveWindow();
          dialog.Show();

          foreach (var overrides in dialog.Result)
            await QuickIOFile.CopyAsync(overrides.Item1, overrides.Item2, true).ConfigureAwait(false);
        });
    }

    private static async Task<IEnumerable<(string, string overwrite)>> PasteRecursive(IEnumerable<string> source, string target)
    {
      var blackListFiles = new HashSet<string>((await QuickIODirectory.EnumerateFilePathsAsync(target).ConfigureAwait(false)).Select(x => Path.GetFileName(x)));
      var toOverwrite = new List<(string, string)>();
      var data = source.Select(x => new QuickIOPathInfo(x))
                       .ToList();

      if (data?.Count == 0)
        return Enumerable.Empty<(string, string overwrite)>();

      foreach (var item in data)
        if (item.SystemEntryType == QuickIOFileSystemEntryType.Directory)
        {
          var newTarget = Path.Combine(target, item.Name);
          if (!await QuickIODirectory.ExistsAsync(newTarget).ConfigureAwait(false))
            await QuickIODirectory.CreateAsync(newTarget).ConfigureAwait(false);

          toOverwrite.AddRange(await PasteRecursive((await QuickIODirectory.EnumerateFileSystemEntryPathsAsync(item)).Select(x => x.Key), newTarget).ConfigureAwait(false));
        }
        else
        {
          var newTarget = Path.Combine(target, item.Name);
          if (blackListFiles.Contains(item.Name) && await QuickIOFile.ExistsAsync(newTarget).ConfigureAwait(false))
          {
            toOverwrite.Add((item.FullName, newTarget));
            continue;
          }

          await QuickIOFile.CopyToDirectoryAsync(item.FullName, target).ConfigureAwait(false);
        }

      return toOverwrite;
    }

    private static void Copy(object parameter = null, bool onlyForSinglePanel = true)
    {
      var selection = GetUniqueSelection(onlyForSinglePanel);
      if (selection == null)
        return;

      Clipboard.Clear();
      Clipboard.SetData(DataFormats.FileDrop, selection.Select(x => x.Path).ToArray());
    }

    private static async Task Delete(object parameter = null, bool onlyForSinglePanel = true)
    {
      var selection = parameter != null
        ? GetParameter(parameter)
        : GetSelection(onlyForSinglePanel);
      if (selection == null)
        return;

      var result = await ProcessManager.Instance.StartProcessAsync(new DeleteProcess(selection.ToList()), true);

      if (result == ProcessResult.Finished && parameter != null)
        if (parameter is IBookmark shortcut)
          BookmarkManager.Remove(shortcut.Path);
    }

    private static async Task Rename(object parameter = null)
    {
      var selection = parameter != null
        ? GetParameter(parameter)
        : GetSelection(true);

      if (selection == null)
        return;

      var items = selection.ToList();
      var dialog = items.Count > 1
        ? new RenameDialogView(items, System.IO.Path.GetDirectoryName(items.First().Path)) { Owner = App.GetActiveWindow() } as BaseRenameDialogView
        : new RenameSingleDialogView(items, System.IO.Path.GetDirectoryName(items.First().Path)) { Owner = App.GetActiveWindow() } as BaseRenameDialogView;

      dialog.ShowDialog();

      if (dialog.Result == null)
        return;

      var result = await ProcessManager.Instance.StartProcessAsync(new RenameProcess(dialog.Result.ToList()));

      if (result == ProcessResult.Finished && parameter != null)
      {
        var entity = (Tuple<IFSEntity, string>)dialog.Result.First();
        if (parameter is IBookmark shortcut)
          shortcut.Rename(entity.Item2);
      }
    }

    private static void OpenProperties(object parameter = null, bool onlyForSinglePanel = true)
    {
      var selection = parameter != null
        ? GetParameter(parameter)
        : GetSelection(onlyForSinglePanel);

      if (selection == null)
        return;

      var dialog = new PropertiesDialogView(selection)
      {
        Owner = App.GetActiveWindow()
      };

      dialog.Show();
    }

    private static void CreateMrDirShortcut()
    {
      var selection = GetSelection(false);
      if (selection == null)
      {
        var currentDir = GetCurrentDirectory();
        if (currentDir == null) return;
        selection = (new Directory(currentDir) as IFSEntity).FromSingleItem();
      }

      foreach (var item in selection)
        BookmarkManager.AddShortcut(item);
    }

    private static void RemoveMrDirShortcut(object parameter = null)
    {
      if (parameter is IBookmark shortcut)
        BookmarkManager.Remove(shortcut.Path);
    }

    #endregion

    #region Commands

    // TODO
    public static ICommand PasteCommand
      => new RelayCommand<object>(async x => await Paste(x));
    public static ICommand CutCommand
      => new RelayCommand(() => { });
    public static ICommand CutMultipleCommand
      => new RelayCommand(() => { });
    public static ICommand OpenCommand
      => new RelayCommand<object>(x => { });

    public static ICommand CopyCommand
      => new RelayCommand<object>(x => Copy(x));
    public static ICommand CopyMultipleCommand
      => new RelayCommand(() => Copy(false));
    public static ICommand RenameCommand
      => new RelayCommand<object>(async x => await Rename(x));
    public static ICommand DeleteCommand
      => new RelayCommand<object>(async x => await Delete(x));
    public static ICommand DeleteMultipleCommand
      => new RelayCommand(async () => await Delete(onlyForSinglePanel: false));

    public static ICommand OpenPropertiesCommand
      => new RelayCommand<object>(x => OpenProperties(x, false));
    public static ICommand OpenPropertiesMultipleCommand
      => new RelayCommand(() => OpenProperties());

    public static ICommand CreateMrDirShortcutCommand
      => new RelayCommand(CreateMrDirShortcut);
    public static ICommand RemoveMrDirShortcutCommand
      => new RelayCommand<object>(RemoveMrDirShortcut);

    public static ICommand NewFolderCommand
      => new RelayCommand(() => { });

    #endregion
  }
}
