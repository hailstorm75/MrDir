﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using MrDir.Interfaces.Models;
using MrDir.Models.FileSystem.Properties;

namespace MrDir.Helpers
{
	public class TemplateDictionary
    : Dictionary<object, DataTemplate>
  {
    public TemplateDictionary()
    {
    }
  }

  [MarkupExtensionReturnType(typeof(DataTemplateSelector))]
  public class TemplateSelectorExt
    : MarkupExtension
  {
    public string Property { get; set; }
    public TemplateDictionary Dictionary { get; set; }

    public TemplateSelectorExt() { }

    public TemplateSelectorExt(TemplateDictionary dictionary)
      : this()
      => Dictionary = dictionary;

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
      if (string.IsNullOrEmpty(Property))
        throw new ArgumentException(nameof(Property));

      return new PropertyTemplateSelector(this);
    }
  }

  public class PropertyTemplateSelector
    : DataTemplateSelector
  {
    private TemplateSelectorExt m_templateSelectorExt;

    public PropertyTemplateSelector(TemplateSelectorExt templateSelectorExt)
      => m_templateSelectorExt = templateSelectorExt;

    public override DataTemplate SelectTemplate(object item, DependencyObject container)
    {
      if (item == null
      || !(container is FrameworkElement))
        return null;

      if (!(item is IProperty property))
        return null;

      Type type = null;

      switch (property)
      {
        case StringProperty stringProp:
          type = typeof(StringProperty);
          break;
        case IntProperty uintProperty:
          type = typeof(IntProperty);
          break;
        case UIntProperty uintProperty:
          type = typeof(UIntProperty);
          break;
        case ULongProperty uintProperty:
          type = typeof(ULongProperty);
          break;
				case DateTimeProperty dateTimeProperty:
          type = typeof(DateTimeProperty);
          break;
      }

      if (type == null)
        return null;

      m_templateSelectorExt.Dictionary.TryGetValue(type, out var dataTemplate);

      return dataTemplate;
    }
  }
}
