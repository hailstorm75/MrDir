﻿using Dragablz;
using MrDir.Managers;
using MrDir.Views.MainWindows;
using System.Windows;

namespace MrDir.Helpers
{
  public class InterTabClient
    : IInterTabClient
  {
    public INewTabHost<Window> GetNewHost(IInterTabClient interTabClient, object partition, TabablzControl source)
    {
      var window = new TabWindowView();
      TabManager.WindowTabs.Add(window, window.Layout);

      return new NewTabHost<Window>(window, window.InitialTabablzControl);
    }

    public TabEmptiedResponse TabEmptiedHandler(TabablzControl tabControl, Window window)
    {
      return Application.Current.Windows.Count > 1
        ? TabEmptiedResponse.CloseWindowOrLayoutBranch
        : TabEmptiedResponse.DoNothing;
    }
  }
}