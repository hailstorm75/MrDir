﻿using System.Drawing;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MrDir.Helpers
{
  public static class ConverterHelper
  {
    public static string BytesToString(ulong bytes)
    {
      if (bytes < 1000)
        return $"{bytes} B";
      if (bytes < 1000000)
        return $"{bytes / 1000} KB";
      if (bytes < 1000000000)
        return $"{bytes / 1000000} MB";
      if (bytes < 1000000000000)
        return $"{bytes / 1000000000} GB";

      return $"{bytes / 1000000000000} TB";
    }

    public static ImageSource IconToImageSource(Icon icon)
    {
      if (icon == null)
        return new BitmapImage();

      ImageSource imageSource = Imaging.CreateBitmapSourceFromHIcon(
        icon.Handle,
        Int32Rect.Empty,
        BitmapSizeOptions.FromEmptyOptions());

      return imageSource;
    }
  }
}