﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using Common.Data;

namespace MrDir.Helpers
{
  internal class Composer : Singleton<Composer>
  {
    private CompositionContainer m_containder;

    public void Compose(object importer)
    {
      var catalog = new AssemblyCatalog(Assembly.GetExecutingAssembly());
      m_containder = new CompositionContainer(catalog);
      m_containder.SatisfyImportsOnce(importer);
    }
  }
}