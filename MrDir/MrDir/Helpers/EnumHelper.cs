﻿using MrDir.Attributes;
using MrDir.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MrDir.Helpers
{
  public static class EnumHelper
  {
    public static string Description(this Enum value)
    {
      var attributes = value.GetType()
        .GetField(value.ToString())
        .GetCustomAttributes(typeof(EnumInfoAttribute), false);
      if (attributes.Any())
        return (attributes.First() as EnumInfoAttribute)?.Description;

      var ti = CultureInfo.CurrentCulture.TextInfo;
      return ti.ToTitleCase(ti.ToLower(value.ToString().Replace("_", " ")));
    }

    public static string Name(this Enum value)
    {
      var attributes = value.GetType()
        .GetField(value.ToString())
        .GetCustomAttributes(typeof(EnumInfoAttribute), false);
      if (attributes.Any())
        return (attributes.First() as EnumInfoAttribute)?.Name;

      var ti = CultureInfo.CurrentCulture.TextInfo;
      return ti.ToTitleCase(ti.ToLower(value.ToString().Replace("_", " ")));
    }

    public static IEnumerable<EnumInfo> GetAllEnumInfos(Type type)
    {
      if (!type.IsEnum)
        throw new ArgumentException($"{nameof(type)} must be an enum type");

			return Enum.GetValues(type).Cast<Enum>().Select(e => new EnumInfo(e, e.Name(), e.Description()));
    }
  }
}