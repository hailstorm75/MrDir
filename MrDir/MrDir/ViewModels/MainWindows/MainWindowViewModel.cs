﻿using Common.Linq;
using MrDir.Enums;
using MrDir.Interfaces.Models;
using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Views;
using MrDir.Managers;
using MrDir.Types;
using MrDir.Controls;
using MrDir.Views.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Input;

namespace MrDir.ViewModels.MainWindows
{
	[Export(typeof(IMainContentViewModel<ITabWindowViewModel>))]
	public sealed class MainWindowViewModel
		: BaseViewModel, IMainContentViewModel<ITabWindowViewModel>
	{
		#region Fields

		private int m_notificationsCount;
		private int m_processCount;
		private bool m_processFlyoutOpen;
		private string m_processIcon = "LocationCirlce";
		private ObservableCollection<IRibbonTabItem> m_ribbonTabItems;

		#endregion

		public MainWindowViewModel()
		{
			NotificationManager.Instance.NewTrackedNotification += OnNewTrackedNotification;
			ProcessManager.Instance.PropertyChanged += OnProcessesUpdated;
		}

		#region Events

		private void OnProcessesUpdated(object sender, PropertyChangedEventArgs eventArgs)
			=> ProcessCount = ProcessManager.Instance.RunningProcesses;

		private void OnNewTrackedNotification(object sender, EventArgs eventArgs)
			=> NotificationsCount++;

		#endregion

		#region Properties

		/// <inheritdoc cref="IWindowView{T}.MainControl"/>
		[Import(typeof(IWindowView<ITabWindowViewModel>))]
		public IWindowView<ITabWindowViewModel> WindowView { get; set; }

		[ImportMany(typeof(IRibbonTabItem))]
		public ObservableCollection<IRibbonTabItem> RibbonTabItems
		{
			get => m_ribbonTabItems;
			set
			{
				value.Sort();
				m_ribbonTabItems = value;
			}
		}

		public ObservableCollection<IProcess> Processes => ProcessManager.Instance.Processes;

		public bool ProcessFlyoutOpen
		{
			get => m_processFlyoutOpen;
			set
			{
				if (m_processFlyoutOpen == value)
					return;

				m_processFlyoutOpen = value;
				OnPropertyChanged(nameof(ProcessFlyoutOpen));
			}
		}

		public string ProcessIcon
		{
			get => m_processIcon;
			set
			{
				m_processIcon = value;
				OnPropertyChanged(nameof(ProcessIcon));
			}
		}

		public int ProcessCount
		{
			get => m_processCount;
			set
			{
				m_processCount = value;
				OnPropertyChanged(nameof(ProcessCount));
				OnPropertyChanged(nameof(Processes));

				ProcessIcon = value == 0 ? "LocationCircle" : "Progress";
			}
		}

		public int NotificationsCount
		{
			get => m_notificationsCount;
			set
			{
				m_notificationsCount = value;
				OnPropertyChanged(nameof(NotificationsCount));
			}
		}

		#endregion

		#region Command

		public ICommand SwipedCommand
			=> new RelayCommand<GestureEventArgs>(e =>
			{
				if (e.Direction == Directions.Right)
					OpenProcessesCommand.Execute(null);
			});

		public ICommand OpenSettingsCommand
			=> new RelayCommand(() =>
			{
				var dialog = new SettingsDialogView { Owner = Application.Current.MainWindow };
				dialog.ShowDialog();
			});

		public ICommand ClearFinishedProcessesCommand
			=> new RelayCommand(() => ProcessManager.Instance.ClearFinished());
		public ICommand OpenProcessesCommand
			=> new RelayCommand(() => ProcessFlyoutOpen = true);

		public ICommand OpenNotificationsCommand
			=> new RelayCommand(() =>
			{
				if (NotificationsCount != 0)
					NotificationsCount = 0;

				var dialog = new NotificationsDialogView { Owner = Application.Current.MainWindow };
				dialog.ShowDialog();
			});

		#endregion
	}
}