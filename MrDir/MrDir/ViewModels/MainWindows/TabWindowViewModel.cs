﻿using Dragablz;
using Dragablz.Dockablz;
using MrDir.Helpers;
using MrDir.Interfaces;
using MrDir.Interfaces.ViewModels;
using MrDir.Managers;
using MrDir.Types;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows.Input;

namespace MrDir.ViewModels.MainWindows
{
	[Export(typeof(ITabWindowViewModel))]
	public class TabWindowViewModel
		: BaseViewModel, ITabWindowViewModel
	{
		/// <summary>
		/// Callback to handle tab closing.
		/// </summary>
		private static void ClosingTabItemHandlerImpl(ItemActionCallbackArgs<TabablzControl> args)
		{
			var tab = args.DragablzItem.DataContext as TabContentViewModel;
			tab.RemoveTab();

			if (args.DragablzItem.Content is ITabHeaderViewModel tabHeader)
				tabHeader.Dispose();
			args.DragablzItem.DataContext = null;
		}

		/// <summary>
		/// Callback to handle floating toolbar/MDI window closing.
		/// </summary>
		private static void ClosingFloatingItemHandlerImpl(ItemActionCallbackArgs<Layout> args)
		{
			//in here you can dispose stuff or cancel the close

			//here's your view model:
			if (args.DragablzItem.DataContext is IDisposable disposable)
				disposable.Dispose();

			//here's how you can cancel stuff:
			//args.Cancel();
		}

		#region Fields

		private bool m_isFocused;

		#endregion

		#region Properties

		public static ObservableCollection<IPanelViewMetadata> AvailablePanels { get; }
		public ClosingFloatingItemCallback ClosingFloatingItemHandler => ClosingFloatingItemHandlerImpl;
		public ItemActionCallback ClosingTabItemHandler => ClosingTabItemHandlerImpl;
		[JsonIgnore]
		public ObservableCollection<ITabItem> Items { get; }

		public bool IsActivated
		{
			get => m_isFocused;
			set
			{
				m_isFocused = value;
				OnPropertyChanged(nameof(IsActivated));
			}
		}

		#endregion

		#region Constructors

		static TabWindowViewModel()
		{
			AvailablePanels = new ObservableCollection<IPanelViewMetadata>(PanelManager.Instance.GetPanels());
		}

		public TabWindowViewModel()
		{
			Items = new ObservableCollection<ITabItem>();
		}

		public TabWindowViewModel(params TabContentViewModel[] items)
		{
			Items = new ObservableCollection<ITabItem>(items);
		}

		#endregion

		#region Commands

		public ICommand GotFocusCommand
			=> new RelayCommand(() => IsActivated = true);

		public ICommand LostFocusCommand
			=> new RelayCommand(() => IsActivated = false);

		#endregion
	}
}