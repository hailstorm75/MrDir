﻿using MrDir.Enums;
using MrDir.Interfaces.Models;
using MrDir.Managers;
using MrDir.Models;
using MrDir.Models.Bookmarks;
using MrDir.Models.FileSystem;
using MrDir.Types;
using MrDir.Types.NETShares;
using MrDir.Types.Notifiers;
using Newtonsoft.Json;
using SchwabenCode.QuickIO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using Directory = MrDir.Models.FileSystem.Directory;

namespace MrDir.ViewModels.Panels
{
	public sealed class BasicFileBrowserPanelViewModel
		: BaseFileBrowserPanelViewModel
	{
		#region Fields

		[JsonIgnore]
		[IgnoreDataMember]
		private IEnumerable<ContextMenuItem> m_contextMenuItems;
		private string m_directoryPath;
		private IDrive m_selectedDrive;
		[JsonIgnore]
		[IgnoreDataMember]
		private FileSystemWatcher m_currentDirectoryWatcher;
		private bool m_isSideMenuOpen;
		private bool m_sideMenuItemSelected;

		private bool m_updateDirPathOnDriveChange = true;

		#endregion

		public BasicFileBrowserPanelViewModel()
		{
			UpdateDrive(HardDriveList.First());
			DirectorySuggestions = new TextBoxSuggestionsCollection(HardDriveList.First().Path, RetrieveSuggestions);
			m_currentDirectoryWatcher = new FileSystemWatcher
			{
				Filter = "*",
				Path = @"C:\",
				EnableRaisingEvents = true
			};

			m_currentDirectoryWatcher.Changed += CurrentDirectoryContentChanged;
			m_currentDirectoryWatcher.Created += CurrentDirectoryContentChanged;
			m_currentDirectoryWatcher.Deleted += CurrentDirectoryContentChanged;
			m_currentDirectoryWatcher.Renamed += CurrentDirectoryContentChanged;
			BookmarkManager.SideMenuItemsChanged += SideMenuItemsChanged;
		}

		#region Methods

		private void SideMenuItemsChanged(object sender, EventArgs e)
		{
			AnySideMenuItems = BookmarkManager.Shortcuts.Any();
			OnPropertyChanged(nameof(AnySideMenuItems));
		}
		private async void CurrentDirectoryContentChanged(object sender, FileSystemEventArgs e)
		{
			switch (e.ChangeType)
			{
				case WatcherChangeTypes.Changed:
					{
						var updated = FileGridItems.Result?.First(x => x.Path.Equals(e.FullPath));
						updated?.Refresh();

						break;
					}
				case WatcherChangeTypes.Renamed:
					{
						if (!(e is RenamedEventArgs er))
							return;

						var old = FileGridItems.Result?.First(x => x.Path.Equals(er.OldFullPath));
						await App.MainThread.InvokeAsync(() => FileGridItems.Result?.Remove(old));

						await App.MainThread.InvokeAsync(() =>
						{
							switch (old)
							{
								case IFile doc:
									FileGridItems.Result?.Add(new MrDir.Models.FileSystem.File(er.FullPath));
									break;
								case DirectoryUNC dirUnc:
									FileGridItems.Result?.Add(new DirectoryUNC(er.FullPath, false));
									break;
								case Directory dir:
									FileGridItems.Result?.Add(new Directory(er.FullPath));
									break;
							}
						});
						break;
					}
				//case WatcherChangeTypes.Created:
				//	m_existingNames.Add(e.Name);
				//	break;
				case WatcherChangeTypes.Deleted:
					{
						var old = FileGridItems.Result.First(x => x.Path.Equals(e.FullPath));
						await App.MainThread.InvokeAsync(() => FileGridItems.Result?.Remove(old));
						break;
					}
				default:
					FileGridItems = new NotifyTaskCompletion<ObservableCollection<IFSEntity>>(LoadItems(DirectoryPath));
					break;
			}
		}

		protected override async Task<ObservableCollection<IFSEntity>> LoadItems(string path)
		{
			DirectorySuggestions.UpdateSuggestions(path);

			return await Task.Run(() =>
				{
					IsLoading = true;
					var items = QuickIODirectory
						.EnumerateDirectories(path)
						.Where(x => !x.Attributes.HasFlag(FileAttributes.Hidden | FileAttributes.System))
						.Select(x => App.MainThread.Invoke(() => new Directory(x) as IFSEntity))
						.Union(QuickIODirectory.EnumerateFiles(path)
						.Select(x =>
						{
							return App.MainThread.Invoke(()
								=> Shortcut.IsShortcut(x.FullName)
									? new Shortcut(x)
									: new Models.FileSystem.File(x) as IFSEntity);
						}));

					var result = new ObservableCollection<IFSEntity>(items);
					IsLoading = false;

					return result;
				}).ConfigureAwait(false);
		}
		private async Task<ObservableCollection<IFSEntity>> LoadSharedItems(string server)
		{
			return await Task.Run(() =>
				{
					var shares = ShareCollection.GetShares(server);
					var result = new ObservableCollection<IFSEntity>();
					foreach (Share share in shares)
					{
						if (!share.IsFileSystem
								|| !share.ShareType.HasFlag(ShareType.Disk))
							continue;
						result.Add(new DirectoryUNC(new QuickIODirectoryInfo(share.Root), true));
					}

					IsLoading = false;

					return result;
				})
				.ConfigureAwait(false);
		}

		private static async Task<List<string>> RetrieveSuggestions(string path)
		{
			return await Task.Run(() =>
				{
					return new List<string>(QuickIODirectory
						.EnumerateDirectories(path)
						.Where(x => !x.Attributes.HasFlag(FileAttributes.Hidden | FileAttributes.System))
						.Select(x => x.FullName));
				})
				.ConfigureAwait(false);
		}

		private async void UpdateDrive(IDrive drive)
		{
			await ClearGridItems().ConfigureAwait(false);
			await Task.Run(() =>
				{
					IsLoading = true;
					if (drive is Drive)
						QuickIODirectory.Exists(drive.Path);

					if (m_updateDirPathOnDriveChange)
						DirectoryPath = drive.Path;
					else
						m_updateDirPathOnDriveChange = true;

					m_selectedDrive = drive;
					OnPropertyChanged(nameof(SelectedDrive));
				})
				.ConfigureAwait(false);
		}
		private void SetDriveFromPath(string path)
		{
			if (SelectedDrive == null) return;
			if (path?.StartsWith(SelectedDrive.Path) ?? true) return;

			m_updateDirPathOnDriveChange = false;

			var root = QuickIODirectory.GetDirectoryRoot(path);
			SelectedDrive = HardDriveList.Where(x => x is Drive)
				.FirstOrDefault(x => root == null
					? path == x.Path
					: root.FullName == x.Path);
		}
		private void SetDriveFromUNCPath(string server)
		{
			if (SelectedDrive == null) return;
			if (GetServerName(server).StartsWith(SelectedDrive?.Path)) return;

			m_updateDirPathOnDriveChange = false;

			var root = GetServerName(server);
			var drive = HardDriveList.Where(x => x is NetworkDrive).FirstOrDefault(x => root == x.Path);
			if (drive == null)
			{
				drive = new NetworkDrive(root);
				HardDriveList.Add(drive);
				SelectedDrive = HardDriveList.Last();
			}
			else SelectedDrive = drive;
		}

		private void SetDirectoryPath(string path)
		{
			if (!path.EndsWith(@"\"))
				path += "\\";

			if (QuickIOPath.IsLocalRegularPath(path))
			{
				if (!SetDirectoryPathLocal(path))
					return;

				SetDriveFromPath(path);
			}
			else if (QuickIOPath.IsShareRegularPath(path))
			{
				if (!SetDirectoryPathUNC(path))
					return;

				SetDriveFromUNCPath(path);
			}
			else return;

			m_lastDirectoryPath = m_directoryPath;
			m_directoryPath = path;

			OnPropertyChanged(nameof(DirectoryPath));
			OnPropertyChanged(nameof(CanGoToRoot));
			OnPropertyChanged(nameof(CanGoToPrevious));
			OnPropertyChanged(nameof(CanGoUp));
			OnRequestedNewTitle(QuickIOPath.GetName(path) ?? path, path);

			IsLoading = false;
		}
		private bool SetDirectoryPathLocal(string path)
		{
			if (!QuickIODirectory.Exists(path)) return false;

			if (m_currentDirectoryWatcher != null)
			{
				m_currentDirectoryWatcher.Changed -= CurrentDirectoryContentChanged;
				m_currentDirectoryWatcher.Created -= CurrentDirectoryContentChanged;
				m_currentDirectoryWatcher.Deleted -= CurrentDirectoryContentChanged;
				m_currentDirectoryWatcher.Renamed -= CurrentDirectoryContentChanged;

				m_currentDirectoryWatcher.Dispose();
			}

			m_currentDirectoryWatcher = new FileSystemWatcher
			{
				Filter = "*",
				Path = path,
				EnableRaisingEvents = true
			};

			m_currentDirectoryWatcher.Changed += CurrentDirectoryContentChanged;
			m_currentDirectoryWatcher.Created += CurrentDirectoryContentChanged;
			m_currentDirectoryWatcher.Deleted += CurrentDirectoryContentChanged;
			m_currentDirectoryWatcher.Renamed += CurrentDirectoryContentChanged;

			FileGridItems = new NotifyTaskCompletion<ObservableCollection<IFSEntity>>(LoadItems(path));

			return true;
		}
		private bool SetDirectoryPathUNC(string path)
		{
			var serverName = GetServerName(path);

			if (ShareCollection.GetShares(serverName).Count == 0) return false;
			if (!serverName.Equals(path)) return SetDirectoryPathLocal(path);

			FileGridItems = new NotifyTaskCompletion<ObservableCollection<IFSEntity>>(LoadSharedItems(serverName));

			return true;
		}

		private static string GetServerName(string path)
		{
			var index = path
				.Substring(2)
				.IndexOf(@"\", StringComparison.InvariantCultureIgnoreCase) + 2;
			if (index == 1)
				throw new ArgumentException(nameof(path));

			return path.Substring(0, index + 1);
		}

		#endregion

		#region Properties

		public bool SideMenuItemSelected
		{
			get => m_sideMenuItemSelected;
			set
			{
				m_sideMenuItemSelected = value;
				OnPropertyChanged(nameof(SideMenuItemSelected));
			}
		}
		public bool IsSideMenuOpen
		{
			get => m_isSideMenuOpen;
			set
			{
				m_isSideMenuOpen = value;
				OnPropertyChanged(nameof(IsSideMenuOpen));
			}
		}

		public bool AnySideMenuItems { get; set; } = BookmarkManager.Shortcuts.Any();

		[JsonIgnore]
		[IgnoreDataMember]
		public IEnumerable<ContextMenuItem> ContextMenuItems
		{
			get => m_contextMenuItems;
			set
			{
				m_contextMenuItems = value;
				OnPropertyChanged(nameof(ContextMenuItems));
			}
		}
		[JsonIgnore]
		public override IList<IFSEntity> Selection { get; set; }
		[JsonIgnore]
		public TextBoxSuggestionsCollection DirectorySuggestions { get; private set; }
		public override bool CanGoToRoot => DirectoryPath != SelectedDrive?.Path;
		public override bool CanGoToPrevious => m_lastDirectoryPath != null;
		public override bool CanGoUp => CanGoToRoot;

		public override string DirectoryPath
		{
			get => m_directoryPath;
			set => SetDirectoryPath(value);
		}

		public static ObservableCollection<IDrive> HardDriveList { get; set; }
			= new ObservableCollection<IDrive>(DriveInfo.GetDrives().Where(x => x.IsReady).Select(x => new Drive(x)));

		public IDrive SelectedDrive
		{
			get => m_selectedDrive;
			set => UpdateDrive(value);
		}

		#endregion

		#region Commands

		[JsonIgnore]
		public ICommand OpenSidebarCommand
			=> new RelayCommand(() => { IsSideMenuOpen = !IsSideMenuOpen; });

		[JsonIgnore]
		public override ICommand SelectedItemsCommand
			=> new RelayCommand<object>(list =>
			{
				Selection = ((IList)list).Cast<IFSEntity>().ToList();
				if (Selection.Count > 1)
					ContextMenuItems = ContextMenuManager.MULTI_CONTEXT_MENU;
				else if (Selection.Count == 1)
				{
					if (Selection[0] is IFile doc)
						ContextMenuItems = ContextMenuManager.FILE_CONTEXT_MENU;
					else
						ContextMenuItems = ContextMenuManager.DIR_CONTEXT_MENU;
				}
				OnPropertyChanged(nameof(Selection));
				OnSelectedEntitiesCountChanged();
			});

		[JsonIgnore]
		public override ICommand GoToRootCommand =>
			new RelayCommand(() =>
			{
				DirectoryPath = SelectedDrive.Path;
				OnPropertyChanged(nameof(DirectoryPath));
			});
		[JsonIgnore]
		public override ICommand GoUpCommand =>
			new RelayCommand(() =>
			{
				if (DirectoryPath == SelectedDrive.Path)
					return;

				var n = 2;
				var index = DirectoryPath.AsEnumerable().Reverse().TakeWhile(x => (n -= x == '\\' ? 1 : 0) > 0).Count();
				DirectoryPath = DirectoryPath.Substring(0, DirectoryPath.Length - index);
				OnPropertyChanged(nameof(DirectoryPath));
			});
		[JsonIgnore]
		public override ICommand GoToPreviousCommand =>
			new RelayCommand(() =>
			{
				DirectoryPath = m_lastDirectoryPath;
				OnPropertyChanged(nameof(DirectoryPath));
			});

		[JsonIgnore]
		public ICommand OpenShortcutCommand
			=> new RelayCommand<object>(e =>
			{
				switch (e)
				{
					case BookmarkToDirectory dir:
						IsSideMenuOpen = false;
						DirectoryPath = dir.Path;
						break;
					case BookmarkToFile doc:
						IsSideMenuOpen = false;
						Process.Start(doc.Path);
						break;
				}

				SideMenuItemSelected = false;
			});
		[JsonIgnore]
		public ICommand RemoveShortcutCommand
			=> new RelayCommand<string>(e =>
			{
				BookmarkManager.Remove(e);
				SideMenuItemSelected = false;
			});

		[JsonIgnore]
		public ICommand ExpandAllCommand
			=> new RelayCommand<ItemsControl>(control =>
			{
				if (control == null) return;

				foreach (var item in control.Items)
					if (control.ItemContainerGenerator.ContainerFromItem(item) is TreeViewItem container)
						container.IsExpanded = true;
			});
		[JsonIgnore]
		public ICommand CollapseAllCommand
			=> new RelayCommand<ItemsControl>(control =>
			{
				if (control == null) return;

				foreach (var item in control.Items)
					if (control.ItemContainerGenerator.ContainerFromItem(item) is TreeViewItem container)
						container.IsExpanded = false;
			});

		#endregion

		protected override void Dispose(bool disposing)
			=> m_currentDirectoryWatcher?.Dispose();
	}
}