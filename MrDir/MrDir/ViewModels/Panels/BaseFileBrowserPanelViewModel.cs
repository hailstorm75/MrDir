﻿using MrDir.Attributes;
using MrDir.Interfaces.Models;
using MrDir.Interfaces.ViewModels;
using MrDir.Models.FileSystem;
using MrDir.Types;
using MrDir.Types.Notifiers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace MrDir.ViewModels.Panels
{
	public abstract class BaseFileBrowserPanelViewModel
		: BaseViewModel, IBrowserPanelViewModel
	{
		#region Methods

		protected void OpenEntity(IFSEntity entity)
		{
			switch (entity)
			{
				case IDirectory dir:
					DirectoryPath = dir.Path;
					break;
				case IFile doc:
					Process.Start(doc.Path);
					break;
				case Shortcut shortcut:
					OpenEntity(shortcut.GetTarget());
					break;
			}
		}

		private static bool CanRead(string path)
		{
			//var readAllow = false;
			//var readDeny = false;

			//var accessControlList = Directory.GetAccessControl(path);
			//var accessRules = accessControlList?.GetAccessRules(true, true, typeof(SecurityIdentifier));

			//if (accessRules == null) return false;

			//foreach (FileSystemAccessRule rule in accessRules)
			//{
			//  if ((FileSystemRights.Read & rule.FileSystemRights) != FileSystemRights.Read) continue;

			//  switch (rule.AccessControlType)
			//  {
			//    case AccessControlType.Allow:
			//      readAllow = true;
			//      break;
			//    case AccessControlType.Deny:
			//      readDeny = true;
			//      break;
			//  }
			//}

			//return readAllow && !readDeny;

			// TODO Update with QuickIO

			return true;
		}

		protected abstract Task<ObservableCollection<IFSEntity>> LoadItems(string path);

		protected static async Task<ObservableCollection<IFSEntity>> ClearGridItems()
			=> await Task.Run(() => new ObservableCollection<IFSEntity>());

		public event EventHandler SelectedEntitiesCountChanged;
		public event EventHandler<EventArgs<Tuple<string, string>>> RequestedNewTitle;

		protected void OnSelectedEntitiesCountChanged()
			=> SelectedEntitiesCountChanged?.Invoke(this, EventArgs.Empty);

		protected void OnRequestedNewTitle(string title, string tooltip)
			=> RequestedNewTitle?.Invoke(this, new EventArgs<Tuple<string, string>>(Tuple.Create(title, tooltip)));

		#endregion

		#region Fields

		private const string DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
		private NotifyTaskCompletion<ObservableCollection<IFSEntity>> m_fileGridItems;
		protected string m_lastDirectoryPath;
		private bool m_ignoreChanges;
		private bool m_isLoading;

		#endregion

		#region Properties

		[JsonIgnore]
		[IgnoreDataMember]
		public bool IsLoading
		{
			get => m_isLoading;
			set
			{
				m_isLoading = value;
				OnPropertyChanged(nameof(IsLoading));
			}
		}

		public abstract bool CanGoToRoot { get; }
		public abstract bool CanGoToPrevious { get; }
		public abstract bool CanGoUp { get; }
		public abstract string DirectoryPath { get; set; }
		/// <inheritdoc cref="IBrowserPanelViewModel.SelectedEntitiesCount"/>
		public virtual int SelectedEntitiesCount => Selection?.Count ?? 0;

		[JsonIgnore]
		[IgnoreDataMember]
		public abstract IList<IFSEntity> Selection { get; set; }

		[JsonIgnore]
		[IgnoreDataMember]
		public virtual NotifyTaskCompletion<ObservableCollection<IFSEntity>> FileGridItems
		{
			get => m_fileGridItems;
			set
			{
				m_fileGridItems = value;
				OnPropertyChanged(nameof(FileGridItems));
			}
		}

		#endregion

		#region Commands

		[JsonIgnore]
		public virtual ICommand CreateColumnsCommand =>
			new RelayCommand<DataGridAutoGeneratingColumnEventArgs>(x =>
			{
				var column = DataGridColumnAttribute.GetDataGridColumn<IFSEntity>(x.Column.Header.ToString());
				if (column == null)
				{
					x.Cancel = true;
					return;
				}

				x.Column.Header = column.Header;
				if (x.PropertyType == typeof(DateTime))
					((DataGridBoundColumn)x.Column).Binding.StringFormat = DATE_FORMAT;
			});

		public abstract ICommand SelectedItemsCommand { get; }

		[JsonIgnore]
		public virtual ICommand NavigateToPathCommand
			=> new RelayCommand<MouseEventArgs>(e =>
			{
				if (Selection?.Count != 1)
					return;

				OpenEntity(Selection.FirstOrDefault());
			});

		public abstract ICommand GoToPreviousCommand { get; }
		public abstract ICommand GoToRootCommand { get; }
		public abstract ICommand GoUpCommand { get; }

		#endregion

		protected virtual void Dispose(bool disposing)
		{
			m_fileGridItems = null;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}