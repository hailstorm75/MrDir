﻿using MrDir.Interfaces.ViewModels;
using MrDir.Types.Notifiers;

namespace MrDir.ViewModels
{
	/// <summary>
	/// Base class for all ViewModels
	/// </summary>
	public abstract class BaseViewModel
    : NotifyValidPropertyChanged, IViewModel
  {
  }
}