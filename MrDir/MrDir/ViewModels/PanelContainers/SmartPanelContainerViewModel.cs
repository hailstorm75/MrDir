﻿using MrDir.Cultures;
using MrDir.Helpers;
using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Views;
using MrDir.Managers;
using MrDir.Types;
using System;
using System.Collections.Generic;

namespace MrDir.ViewModels.PanelContainers
{
	public class SmartPanelContainerViewModel
		: BaseViewModel, IPanelContainerViewModel
	{
		#region Fields

		private bool m_isActivated;

		#endregion

		public SmartPanelContainerViewModel(string guid)
		{
			Content = PanelManager.Instance.GetPanel(guid);
			if (Content is IBrowserPanelView browser)
				browser.SelectedEntitiesCountChanged += ContentOnSelectedEntitiesCountChanged;
			Content.RequestedNewTitle += OnRequestedNewTitle;
			Content.ContentGotFocus += ContentOnGotFocus;
		}

		~SmartPanelContainerViewModel()
		{
			Dispose(false);
		}

		#region Properties

		public IEnumerable<object> Selection => ((IBrowserPanelView)Content).Selection as IEnumerable<object> ?? new List<object>();
		/// <inheritdoc cref="IPanelContainerViewModel.Content"/>
		public IPanelView<IPanelViewModel> Content { get; }
		public string StatusBar { get; set; }

		public bool IsActivated
		{
			get => m_isActivated;
			set
			{
				m_isActivated = value;
				OnPropertyChanged(nameof(IsActivated));
			}
		}

		#endregion

		#region Methods

		private void ContentOnSelectedEntitiesCountChanged(object sender, EventArgs eventArgs)
		{
			var browser = Content as IBrowserPanelView;
			StatusBar = browser.SelectedCount == 0
				? string.Empty
				: $"{Resources.ItemsSelected}: {browser.SelectedCount}";
			OnPropertyChanged(nameof(StatusBar));
		}

		/// <inheritdoc cref="IActivatable.Activate"/>
		public void Activate()
		{
			IsActivated = true;
			Content.Activate();
		}

		/// <inheritdoc cref="IActivatable.Deactivate"/>
		public void Deactivate()
		{
			IsActivated = false;
			Content.Deactivate();
		}

		#endregion

		#region IDisposasble

		private void ReleaseUnmanagedResources()
		{
			StatusBar = null;
		}

		private void Dispose(bool disposing)
		{
			ReleaseUnmanagedResources();

			if (!disposing) return;

			if (Content is IBrowserPanelView browser)
				browser.SelectedEntitiesCountChanged -= ContentOnSelectedEntitiesCountChanged;
			Content.RequestedNewTitle -= OnRequestedNewTitle;
			Content.ContentGotFocus -= ContentOnGotFocus;
			Content?.Dispose();
		}

		/// <inheritdoc cref="IDisposable.Dispose"/>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion

		#region Events

		/// <inheritdoc cref="IPanelContainerViewModel.GotFocus"/>
		public event EventHandler GotFocus;

		/// <inheritdoc cref="IPanelContainerViewModel.RequestedNewTitle"/>
		public event EventHandler<EventArgs<Tuple<string, string>>> RequestedNewTitle;

		private void OnRequestedNewTitle(object sender, EventArgs<Tuple<string, string>> e)
			=> RequestedNewTitle?.Invoke(sender, e);

		private void ContentOnGotFocus(object sender, EventArgs eventArgs)
			=> GotFocus?.Invoke(sender, eventArgs);

		#endregion
	}
}