﻿using System;
using System.Runtime.Serialization;
using System.Windows;
using Dragablz;
using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Views;
using MrDir.Managers;
using MrDir.Models;
using Newtonsoft.Json;

namespace MrDir.ViewModels
{
  public class TabContentViewModel
    : BaseViewModel, ITabItem, IActivatable, IDisposable
  {
    #region Fields

    [JsonIgnore]
    [IgnoreDataMember]
    private ITabHeaderViewModel m_header;
    [JsonIgnore]
    [IgnoreDataMember]
    private IPanelContainerView m_content;

    #endregion

    public TabContentViewModel(TabContentModel model)
    {
      m_content = model.Content;
      m_header = m_content.Header;
      m_header.CheckBoxVisiblility = TabManager.IsTabSelectionEnabled
        ? Visibility.Visible
        : Visibility.Collapsed;
      m_header.SelectionChanged += OnSelectionChanged;
      m_content.ViewModel.GotFocus += OnGotFocus;
      Guid = model.Guid;
    }

    #region Properties

    public string Guid { get; }

    public object Header
      => m_header;

    public object Content
      => m_content;

    public IPanelViewModel ContentPanel
      => m_content.ViewModel.Content.ViewModel;

    #endregion

    #region Events

    private void OnGotFocus(object sender, EventArgs e)
      => TabManager.Activate(this);

    private void OnSelectionChanged(object sender, EventArgs e)
    {
      if (m_header.IsSelected)
        this.SelectTab();
      else
        this.UnselectTab();
    }

    #endregion

    #region IDisposable

    public void Dispose()
    {
      if (Content != null)
      {
        if (Header != null)
          m_header.SelectionChanged -= OnSelectionChanged;
        if (m_content.ViewModel != null)
          m_content.ViewModel.GotFocus -= OnGotFocus;
      }

      m_content?.Dispose();
      m_content = null;
    }

    #endregion   

    #region IActivatable

    public void Activate()
      => m_content?.Activate();

    public void Deactivate()
      => m_content?.Deactivate();

    #endregion
  }
}