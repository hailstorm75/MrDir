﻿using System;
using System.Windows;
using MrDir.Interfaces.ViewModels;

namespace MrDir.ViewModels.TabHeaders
{
  public sealed class SelectableTabHeaderViewModel
    : BaseViewModel, ITabHeaderViewModel
  {
    #region Fields

    private Visibility m_checkBoxVisiblility;
    private string m_header;
    private bool m_isSelected;
    private string m_toolTip;

    #endregion

    #region Properties

    public string Header
    {
      get => m_header;
      set
      {
        if (value == m_header)
          return;
        m_header = value;
        OnPropertyChanged(nameof(Header));
      }
    }

    public string ToolTip
    {
      get => m_toolTip;
      set
      {
        m_toolTip = value;
        OnPropertyChanged(nameof(ToolTip));
      }
    }

    public bool IsSelected
    {
      get => m_isSelected;
      set
      {
        if (value.Equals(m_isSelected))
          return;
        m_isSelected = value;
        OnPropertyChanged(nameof(IsSelected));
        OnSelectionChanged();
      }
    }

    public Visibility CheckBoxVisiblility
    {
      get => m_checkBoxVisiblility;
      set
      {
        m_checkBoxVisiblility = value;
        OnPropertyChanged(nameof(CheckBoxVisiblility));
      }
    }

    #endregion

    public void Dispose()
    {
      m_header = null;
    }

    public event EventHandler SelectionChanged;

    private void OnSelectionChanged()
      => SelectionChanged?.Invoke(this, EventArgs.Empty);
  }
}