﻿using MrDir.Types;
using MrDir.Types.Observable;
using SchwabenCode.QuickIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace MrDir.ViewModels.Dialogs
{
  public class ResolveOverwriteConflictsViewModel
    : BaseDialogViewModel
  {
    #region Fields

    private bool m_selectionConfirmed;
    private ObservableCollection<ObservablePair<Tuple<string,string>, bool>> m_conflicts;

    #endregion

    #region Properties

    public ObservableCollection<ObservablePair<Tuple<string,string>, bool>> Conflicts
    {
      get => m_conflicts;
      set
      {
        m_conflicts = value;
        OnPropertyChanged();
      }
    }

    public IEnumerable<Tuple<string, string>> Result
      => m_selectionConfirmed ? Conflicts.Where(x => x.Right).Select(x => x.Left) : null;

    #endregion

    #region Commands

    public override ICommand ApplyCommand
  => new RelayCommand<Window>(window =>
  {
    m_selectionConfirmed = true;
    window.Close();
  });

    public ICommand OverwriteAllCommand
      => new RelayCommand(() =>
      {
        foreach (var conflict in Conflicts)
          conflict.Right = true;
      });

    public ICommand SkipAllCommand
      => new RelayCommand(() =>
      {
        foreach (var conflict in Conflicts)
          conflict.Right = false;
      });

    #endregion

    public ResolveOverwriteConflictsViewModel(IEnumerable<(string source, string target)> conflicts)
    {
      var processed = conflicts.Select(x => new ObservablePair<Tuple<string,string>, bool>(Tuple.Create(x.source, x.target), false));
      m_conflicts = new ObservableCollection<ObservablePair<Tuple<string,string>, bool>>(processed);
    }
  }
}
