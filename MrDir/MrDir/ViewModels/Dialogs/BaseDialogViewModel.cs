﻿using System.Windows;
using System.Windows.Input;
using MrDir.Types;

namespace MrDir.ViewModels.Dialogs
{
  public abstract class BaseDialogViewModel
    : BaseViewModel
  {
    public abstract ICommand ApplyCommand { get; }

    public ICommand CancelCommand
      => new RelayCommand<Window>(window => window.Close());
  }
}
