﻿using MrDir.Interfaces.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MrDir.Types;
using MrDir.Types.Observable;

namespace MrDir.ViewModels.Dialogs
{
  public class ResolveFileConflictsDialogViewModel
    : BaseDialogViewModel
  {
    public ResolveFileConflictsDialogViewModel(IEnumerable<IGrouping<string, IFSEntity>> conflicts)
    {
      Conflicts = conflicts.ToDictionary(x => x.Key,
        x => new ObservableCollection<ObservablePair<bool, IFSEntity>>(x.Select(y => new ObservablePair<bool, IFSEntity>(false, y))
          .OrderBy(y => y.Right.Path)));
    }

    #region Properties

    public IEnumerable<IFSEntity> Result { get; private set; } = new List<IFSEntity>();
    public Dictionary<string, ObservableCollection<ObservablePair<bool, IFSEntity>>> Conflicts { get; set; }

    #endregion

    #region Commands

    public ICommand ExpandAllCommand
      => new RelayCommand<ItemsControl>(control =>
      {
        foreach (var item in control.Items)
        {
          var container = control.ItemContainerGenerator.ContainerFromItem(item) as TreeViewItem;
          container.IsExpanded = true;
        }
      });

    public ICommand CollapseAllCommand
      => new RelayCommand<ItemsControl>(control =>
      {
        foreach (var item in control.Items)
        {
          var container = control.ItemContainerGenerator.ContainerFromItem(item) as TreeViewItem;
          container.IsExpanded = false;
        }
      });

    public override ICommand ApplyCommand
      => new RelayCommand<Window>(window =>
      {
        Result = Conflicts.Select(x => x.Value.FirstOrDefault(e => e.Left)).Where(x => x != null).Select(x => x.Right);
        window.Close();
      });

    #endregion
  }
}