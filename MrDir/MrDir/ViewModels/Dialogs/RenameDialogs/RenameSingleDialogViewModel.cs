﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Common.Linq;
using MrDir.Interfaces.Models;
using MrDir.Types;
using MrDir.Types.Observable;

namespace MrDir.ViewModels.Dialogs.RenameDialogs
{
  public class RenameSingleDialogViewModel
    : BaseRenameDialogViewModel
  {
    #region Fields

    private ObservablePair<IFSEntity, string> m_name;
    private int m_selectionlength;

    #endregion

    #region Properties

    public ObservablePair<IFSEntity, string> Item
    {
      get => m_name;
      set
      {
        m_name = value;
        OnPropertyChanged(nameof(Item));
      }
    }

    public int SelectionLength
    {
      get => m_selectionlength;
      set
      {
        m_selectionlength = value;
        OnPropertyChanged(nameof(SelectionLength));
      }
    }

    #endregion

    public RenameSingleDialogViewModel(ICollection<IFSEntity> entities, string path)
			: base(path, entities.Select(x => x.Name))
    {
      var first = entities.First();
      Item = new ObservablePair<IFSEntity, string>(first, first.Name);
      SelectionLength = Item.Left is IFile
        ? Path.GetFileNameWithoutExtension(Item?.Right ?? string.Empty).Length
        : Item.Right.Length;
    }

    #region Commands

    public override ICommand RenameCommand
      => new RelayCommand<Window>(x =>
      {
        Result = new List<Tuple<IFSEntity, string>> { new Tuple<IFSEntity, string>(Item.Left, Item.Right) };

        x.Close();
      });

		#endregion
	}
}
