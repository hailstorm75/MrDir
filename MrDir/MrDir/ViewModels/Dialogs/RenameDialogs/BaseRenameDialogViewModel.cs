﻿using ConcurrentCollections;
using MrDir.Interfaces.Models;
using MrDir.Types;
using SchwabenCode.QuickIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace MrDir.ViewModels.Dialogs.RenameDialogs
{
	public abstract class BaseRenameDialogViewModel
		: BaseViewModel
	{
		#region Fields

		protected ObservableCollection<string> m_blackList;
		private readonly string m_path;

		private readonly FileSystemWatcher m_watcher;

		#endregion

		public List<Tuple<IFSEntity, string>> Result { get; protected set; }
		public ObservableCollection<string> BlackList
		{
			get => m_blackList;
			set
			{
				m_blackList = value;
				OnPropertyChanged();
			}
		}

		public BaseRenameDialogViewModel(string path, IEnumerable<string> whiteList)
		{
			m_path = path;
			m_watcher = new FileSystemWatcher()
			{
				Filter = "*",
				Path = m_path,
				EnableRaisingEvents = true
			};

			BlackList = new ObservableCollection<string>(QuickIODirectory.EnumerateDirectoryPaths(path)
				.Concat(QuickIODirectory.EnumerateFilePaths(path))
				.Select(x => x.Substring(m_path.Length))
				.Except(whiteList));

			m_watcher.Created += OnPathContentChanged;
			m_watcher.Deleted += OnPathContentChanged;
			m_watcher.Renamed += OnPathContentChanged;
		}

		private void OnPathContentChanged(object sender, FileSystemEventArgs e)
		{
			switch (e.ChangeType)
			{
				case WatcherChangeTypes.Renamed:
					var er = e as RenamedEventArgs;
					BlackList.Remove(er.OldName);
					BlackList.Add(er.Name);
					break;
				case WatcherChangeTypes.Created:
					BlackList.Add(e.Name);
					break;
				case WatcherChangeTypes.Deleted:
					BlackList.Remove(e.Name);
					break;
			}
		}

		~BaseRenameDialogViewModel()
		{
			m_watcher?.Dispose();
		}

		#region Commands

		public abstract ICommand RenameCommand { get; }

		public ICommand CancelCommand
			=> new RelayCommand<Window>(x => { x.Close(); });

		public ICommand ValidationErrorCommand
			=> new RelayCommand<object>(x =>
			{
				var a = x;
			});

		public ICommand ValidationNoErrorCommand
			=> new RelayCommand<object>(x =>
			{
				var a = x;
			});

		#endregion
	}
}
