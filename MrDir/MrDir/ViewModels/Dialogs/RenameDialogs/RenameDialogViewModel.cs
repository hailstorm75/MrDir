﻿using MrDir.Cultures;
using MrDir.Enums;
using MrDir.Helpers;
using MrDir.Interfaces.Models;
using MrDir.Types;
using MrDir.Types.Notifiers;
using MrDir.Types.Observable;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MrDir.ViewModels.Dialogs.RenameDialogs
{
  public class RenameDialogViewModel
    : BaseRenameDialogViewModel
  {
    #region Fields

    private int m_selectedMode;
    private ObservableCollection<ObservablePair<IFSEntity, string>> m_entities;
    private string m_mainText;
    private string m_additionalText;
    private string m_beforeSequenceText;
    private string m_afterSequenceText;
    private bool m_skipSequenceFirst;
    private List<string> m_tabs;
    private string m_addBefore;
    private string m_addAfter;
    private SearchMode m_searchMode;
    private string m_find;
    private string replaceWith;
    private IList<ObservablePair<IFSEntity, string>> m_selection;
    private const string CARD_MAIN_TEXT = "CardMainText";
    private const string CARD_ADDITIONAL_TEXT = "CardAdditionalText";
    private const string CARD_SEQUENCE = "CardSequence";

    #endregion

    #region Properties

    public int SelectedMode
    {
      get => m_selectedMode;
      set
      {
        m_selectedMode = value;
        OnPropertyChanged(nameof(RegexFromSelectionEnabled));
        ClearErrors();
        SendValidationRequired();
      }
    }
    public ObservableCollection<int> Indecies { get; set; }

    public List<string> Tabs
    {
      private get { return m_tabs; }
      set
      {
        m_tabs = value;
        UpdateTabIndecies();
        OnPropertyChanged(nameof(Tabs));
      }
    }

    public string MainText
    {
      get => m_mainText;
      set
      {
        m_mainText = value;
        OnPropertyChanged(nameof(MainText), m_propertyCheckers[nameof(MainText)]);
        OnPropertyChanged(nameof(AdditionalText), m_propertyCheckers[nameof(AdditionalText)]);
      }
    }
    public string AdditionalText
    {
      get => m_additionalText;
      set
      {
        m_additionalText = value;
        OnPropertyChanged(nameof(MainText), m_propertyCheckers[nameof(MainText)]);
        OnPropertyChanged(nameof(AdditionalText), m_propertyCheckers[nameof(AdditionalText)]);
      }
    }
    public string BeforeSequenceText
    {
      get => m_beforeSequenceText;
      set
      {
        m_beforeSequenceText = value;
        OnPropertyChanged(nameof(BeforeSequenceText));
      }
    }
    public string AfterSequenceText
    {
      get => m_afterSequenceText;
      set
      {
        m_afterSequenceText = value;
        OnPropertyChanged(nameof(AfterSequenceText));
      }
    }
    public bool SkipSequenceFirst
    {
      get => m_skipSequenceFirst;
      set
      {
        m_skipSequenceFirst = value;
        OnPropertyChanged(nameof(SkipSequenceFirst), m_propertyCheckers[nameof(MainText)]);
        OnPropertyChanged(nameof(SkipSequenceFirst), m_propertyCheckers[nameof(AdditionalText)]);
      }
    }

    public string AddBefore
    {
      get => m_addBefore;
      set
      {
        m_addBefore = value;
        OnPropertyChanged(nameof(AddBefore), m_propertyCheckers[nameof(AddBefore)]);
      }
    }
    public string AddAfter
    {
      get => m_addAfter;
      set
      {
        m_addAfter = value;
        OnPropertyChanged(nameof(AddAfter), m_propertyCheckers[nameof(AddAfter)]);
      }
    }
    public SearchMode SearchMode
    {
      get => m_searchMode;
      set
      {
        m_searchMode = value;
        OnPropertyChanged();
      }
    }
    public string Find
    {
      get => m_find;
      set
      {
        m_find = value;
        OnPropertyChanged(nameof(Find), m_propertyCheckers[nameof(Find)]);
      }
    }
    public string ReplaceWith
    {
      get => replaceWith;
      set
      {
        replaceWith = value;
        OnPropertyChanged(nameof(ReplaceWith), m_propertyCheckers[nameof(ReplaceWith)]);
      }
    }

		public ObservableCollection<ObservablePair<IFSEntity, string>> Entities
    {
      get => m_entities;
      set
      {
        m_entities = value;
        OnPropertyChanged(nameof(Entities));
      }
    }

    [JsonIgnore]
    public IList<ObservablePair<IFSEntity, string>> Selection
    {
      get => m_selection;
      set
      {
        m_selection = value;
        OnPropertyChanged();
        OnPropertyChanged(nameof(RegexFromSelectionEnabled));
      }
    }
    public bool RegexFromSelectionEnabled
      => SelectedMode == 1 && Selection?.Any() == true;

    #endregion

    #region Events

    protected override async void ReceiveValidationRequired(object sender, EventArgs eventArgs)
    {
      if (SelectedMode == 0)
      {
        await m_propertyCheckers[nameof(MainText)]().ConfigureAwait(false);
        await m_propertyCheckers[nameof(AdditionalText)]().ConfigureAwait(false);
      }
      else if (SelectedMode == 0)
      {

      }
    }

    #endregion

    public RenameDialogViewModel(ICollection<IFSEntity> entities, string path)
			: base(path, entities.Select(x => x.Name))
    {
      Entities = new ObservableCollection<ObservablePair<IFSEntity, string>>(entities
        .Select(entity => new ObservablePair<IFSEntity, string>(entity, entity.Name)));
      Indecies = new ObservableCollection<int> { 0, 1, 2, 3, 4, 5 };

      m_propertyCheckers = new Dictionary<string, Func<Task<bool>>>
      {
        {
          nameof(MainText),
          () => Validate(nameof(MainText),
                         false,
                         () => IsAtLeastOneNotEmpty(MainText, AdditionalText, Resources.ValidationAtLeastOneFieldFilled),
                         () => ValidationHelper.IsValidSystemName(MainText))
        },
        {
          nameof(AdditionalText),
          () => Validate(nameof(AdditionalText),
                         false,
                         () => IsAtLeastOneNotEmpty(MainText, AdditionalText, Resources.ValidationAtLeastOneFieldFilled),
                         () => ValidationHelper.IsValidSystemName(AdditionalText))
        },
        {
          nameof(AddBefore),
          () => Validate(nameof(AddBefore),
                         false,
                         () => ValidationHelper.IsValidSystemName(AddBefore))
        },
        {
          nameof(AddAfter),
          () => Validate(nameof(AddAfter),
                         false,
                         () => ValidationHelper.IsValidSystemName(AddAfter))
        },
        {
          nameof(Find),
          () => Validate(nameof(Find),
                         false,
                         () => Task.Run(() => default(string)))
        },
        {
          nameof(ReplaceWith),
          () => Validate(nameof(ReplaceWith),
                         false,
                         () => ValidationHelper.IsValidSystemName(ReplaceWith))
        },
      };
    }

    #region Methods

    private void UpdateTabIndecies()
    {
      int nextIndex = 0;
      foreach (var tab in Tabs)
      {
        switch (tab)
        {
          case CARD_MAIN_TEXT:
            Indecies[0] = nextIndex++;
            break;
          case CARD_ADDITIONAL_TEXT:
            Indecies[1] = nextIndex++;
            break;
          case CARD_SEQUENCE:
            // Before
            Indecies[2] = nextIndex++;
            // After
            Indecies[3] = nextIndex++;
            // Skip first
            Indecies[4] = nextIndex++;
            // Type
            Indecies[5] = nextIndex++;
            break;
          default:
            throw new Exception();
        }
      }
    }

    private void BuildNewName(ref StringBuilder sb, int index, string tab)
    {
      switch (tab)
      {
        case CARD_MAIN_TEXT:
          sb.Append(MainText);
          break;
        case CARD_ADDITIONAL_TEXT:
          sb.Append(AdditionalText);
          break;
        case CARD_SEQUENCE:
          if (index != 0
              || !SkipSequenceFirst && index == 0)
            sb.Append(BeforeSequenceText).Append(index + (SkipSequenceFirst ? 0 : 1)).Append(AfterSequenceText);
          break;
        default:
          throw new Exception();
      }
    }
    private string BuildModifiedName(string currentName)
    {
      string replaced = currentName;
      if (SearchMode == SearchMode.RegEx)
      {
        var regex = new Regex(Find);
        replaced = regex.Replace(currentName, ReplaceWith);
      }
      else if (SearchMode == SearchMode.Glob)
      {
        var regex = new Regex(Regex.Escape(Find).Replace("\\?", ".").Replace("\\*", ".*"));
        replaced = regex.Replace(currentName, ReplaceWith);
      }

      return string.Concat(AddBefore ?? string.Empty, replaced, AddAfter ?? string.Empty);
    }

    private async Task<string> IsAtLeastOneNotEmpty(string valueA, string valueB, string error)
    {
      return await Task.Run(() =>
      {
        if (SkipSequenceFirst && string.IsNullOrEmpty(valueA) && string.IsNullOrEmpty(valueB))
          return error;

        return null;
      });
    }

    private void PreviewNewName()
    {
      for (var i = 0; i < Entities.Count; i++)
      {
        var sb = new StringBuilder();
        sb.Append(BuildModifiedName(Path.GetFileNameWithoutExtension(Entities[i].Left.Name)));

        if (Entities[i].Left is IFile doc)
          sb.Append(doc.Extension);

        Entities[i].Right = sb.ToString();
      }
    }
    private void PreviewModifiedName()
    {
      for (var i = 0; i < Entities.Count; i++)
      {
        var sb = new StringBuilder();
        foreach (var tab in Tabs)
          BuildNewName(ref sb, i, tab);

        if (Entities[i].Left is IFile doc)
          sb.Append(doc.Extension);

        Entities[i].Right = sb.ToString();
      }
    }

    private void RenameNewName()
    {
      Result = new List<Tuple<IFSEntity, string>>();
      for (var i = 0; i < Entities.Count; i++)
      {
        var sb = new StringBuilder();
        foreach (var tab in Tabs)
          BuildNewName(ref sb, i, tab);

        if (Entities[i].Left is IFile doc)
          sb.Append(doc.Extension);

        Result.Add(new Tuple<IFSEntity, string>(Entities[i].Left, sb.ToString()));
      }
    }

    #endregion

    #region Commands

    [JsonIgnore]
    public ICommand SelectedItemsCommand
      => new RelayCommand<object>(list =>
      {
        Selection = ((IList)list).Cast<ObservablePair<IFSEntity, string>>().ToList();
        OnPropertyChanged(nameof(Selection));
      });

    public ICommand RegexFromSelectionCommand
      => new RelayCommand(() =>
      {
        var generator = new Common.Data.PatternGenerator();
        generator.LoadStrings(Selection.Select(x => Path.GetFileNameWithoutExtension(x.Left.Name)));
        var result = generator.FindPattern().ToString();
        Clipboard.SetText(result);
      });

    public ICommand LoadedCommand
      => new RelayCommand<ItemCollection>(x => { });

    public ICommand PreviewCommand
      => new RelayCommand<ItemCollection>(x =>
      {
        Task.Run(() =>
        {
          if (SelectedMode == 0)
            PreviewModifiedName();
          else
            PreviewNewName();

          OnPropertyChanged(nameof(Entities));
        });
      });

    public override ICommand RenameCommand
      => new RelayCommand<Window>(x =>
      {
        if (SelectedMode == 0)
          RenameNewName();

        x.Close();
      });

    #endregion
  }
}