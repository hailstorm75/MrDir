﻿using System.Collections.ObjectModel;
using MrDir.Interfaces.Models;
using MrDir.Managers;

namespace MrDir.ViewModels.Dialogs
{
  public class NotificationsWindowViewModel
    : BaseViewModel
  {
    public NotificationsWindowViewModel()
      => Notifications = new ObservableCollection<INotification>(NotificationManager.Instance.Notifications);

    public ObservableCollection<INotification> Notifications { get; set; }
  }
}