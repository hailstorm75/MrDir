﻿using MahApps.Metro.Controls.Dialogs;
using MrDir.Helpers;
using MrDir.Types;
using System;
using System.Windows.Input;

namespace MrDir.ViewModels.Dialogs.CustomMessageDialogs
{
  public class MessageDialogViewModel
    : BaseViewModel
  {
    #region Fields

    private readonly Action<MessageDialogViewModel> m_closeHandler;
    private string m_messageText;

    #endregion

    #region Properties

    public MessageDialogResult Result { get; private set; }
    public string MessageText
    {
      get => m_messageText;
      set
      {
        m_messageText = value;
        OnPropertyChanged();
      }
    }

    #endregion

    #region Commands

    public ICommand OkCommand
      => new RelayCommand(() =>
      {
        Result = MessageDialogResult.Affirmative;
        m_closeHandler(this);
      });

    public ICommand CancelCommand
      => new RelayCommand(() =>
      {
        Result = MessageDialogResult.Canceled;
        m_closeHandler(this);
      });

    #endregion

    public MessageDialogViewModel(Action<MessageDialogViewModel> closeHandler, string message)
    {
      m_closeHandler = closeHandler;
      MessageText = message;
    }
  }
}
