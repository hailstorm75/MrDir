﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using MrDir.Interfaces.Models;
using MrDir.Types;
using MrDir.Views.Dialogs.PropertiesDialogs;

namespace MrDir.ViewModels.Dialogs.PropertiesDialogs
{
  public class PropertiesDialogViewModel
    : BaseDialogViewModel
  {
    #region Fields

    private readonly List<IFSEntity> m_items;

    #endregion

    #region Properties

    public BasePropertiesEditorView Editor { get; }

    #endregion

    public PropertiesDialogViewModel(IEnumerable<IFSEntity> items)
    {
      m_items = items.ToList();
      if (m_items.Count == 1)
        Editor = new PropertiesSingleEditorView(m_items.First());
    }

    #region Commands

    public override ICommand ApplyCommand
      => new RelayCommand(() =>
      {

      });

    #endregion
  }
}
