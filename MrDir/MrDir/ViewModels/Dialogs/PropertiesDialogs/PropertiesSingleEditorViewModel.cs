﻿using System.Collections.ObjectModel;
using System.Linq;
using MrDir.Interfaces.Models;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using MrDir.Models.FileSystem.Properties;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MrDir.Enums;

namespace MrDir.ViewModels.Dialogs.PropertiesDialogs
{
  public class PropertiesSingleEditorViewModel
    : BasePropertiesDialogViewModel
  {
    #region Fields

    private readonly EntityType m_type;
    private readonly IFSEntity m_item;
    private string m_itemName;
    private ObservableCollection<IProperty> m_details;

    #endregion

    #region Properties

    public string ItemName
    {
      get => m_itemName;
      set
      {
        m_itemName = value;
        OnPropertyChanged(nameof(ItemName));
      }
    }
    public Visibility DetailsTabVisible { get; set; } = Visibility.Visible;
    public Visibility GeneralTabVisible { get; set; } = Visibility.Visible;
    public ImageSource Itemicon { get; set; } = new BitmapImage();

    public ObservableCollection<IProperty> Details
    {
      get => m_details;
      set
      {
        m_details = value;
        OnPropertyChanged(nameof(Details));
      }
    }

    #endregion

    public PropertiesSingleEditorViewModel(IFSEntity item)
    {
      m_item = item;
      ItemName = item.Name;
      switch (item)
      {
        case IFile _:
          m_type = EntityType.File;
          LoadDetails();
          break;
        case IDirectory _:
          m_type = EntityType.Directory;
          break;
      }
    }

    private void LoadDetails()
    {
      var filePath = m_item.Path;
      var file = ShellFile.FromFilePath(filePath);
			var details = file.Properties.DefaultPropertyCollection
				.Cast<IShellProperty>()
				.Where(x => !string.IsNullOrEmpty(x.Description.DisplayName))
				.Select(x =>
				{
					if (x.ValueType == typeof(DateTime) || x.ValueType == typeof(DateTime?))
						return new DateTimeProperty(x) as IProperty;
					else if (x.ValueType == typeof(string))
						return new StringProperty(x) as IProperty;
					else if (x.ValueType == typeof(int) || x.ValueType == typeof(int?))
						return new UIntProperty(x) as IProperty;
					else if (x.ValueType == typeof(uint) || x.ValueType == typeof(uint?))
						return new UIntProperty(x) as IProperty;
					else if (x.ValueType == typeof(ulong) || x.ValueType == typeof(ulong?))
						return new ULongProperty(x) as IProperty;
					return null;
				})
				.Where(x => x != null);
			Details = new ObservableCollection<IProperty>(details);

      // Read and Write:

      //var oldAuthors = file.Properties.System.Author.Value;
      //var oldTitle = file.Properties.System.Title.Value;

      //file.Properties.System.Author.Value = new string[] { "Author #1", "Author #2" };
      //file.Properties.System.Title.Value = "Example Title";

      // Alternate way to Write:

      //ShellPropertyWriter propertyWriter = file.Properties.GetPropertyWriter();
      //propertyWriter.WriteProperty(SystemProperties.System.Author, new string[] { "Author" });
      //propertyWriter.Close();
    }
  }
}