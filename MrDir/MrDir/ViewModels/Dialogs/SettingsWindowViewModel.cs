﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using System.Windows.Input;
using Fluent;
using MrDir.Cultures;
using MrDir.Helpers;
using MrDir.Models;
using MrDir.Properties;
using MrDir.Types;

namespace MrDir.ViewModels.Dialogs
{
  public class SettingsWindowViewModel
    : BaseDialogViewModel
  {
    #region Methods

    private static async Task<IEnumerable<CultureInfo>> GetAvailableCultures()
    {
      return await Task.Run(() =>
      {
        var result = new List<CultureInfo>();

        var rm = new ResourceManager(typeof(Resources));

        var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
        foreach (var culture in cultures)
          try
          {
            if (culture.Equals(CultureInfo.InvariantCulture)) continue; //do not use "==", won't work

            var rs = rm.GetResourceSet(culture, true, false);
            if (rs != null)
              result.Add(culture);
          }
          catch (CultureNotFoundException)
          {
            //NOP
          }

        return result;
      });
    }

    private static async Task<ObservableCollection<Language>> GetAvailableLanguages()
    {
      var languages = new ObservableCollection<Language>();
      var cultures = await GetAvailableCultures().ConfigureAwait(true);

      foreach (var culture in cultures) languages.Add(new Language(culture));

      return languages;
    }

    private async Task LoadSettings()
    {
      Colors = new ObservableCollection<Color>(App.Accents.AsEnumerable());
      AvailableLanguages = await Task.Run(async () => await GetAvailableLanguages());
      SelectedLanguage = Settings.Default.Language.Equals(string.Empty)
        ? AvailableLanguages.FirstOrDefault(x => x.Info.Name == ENGLISH_ISO_NAME)
        : AvailableLanguages.FirstOrDefault(x => x.Info.Name == Settings.Default.Language);

      m_selectedAppMode = Settings.Default.AppMode;
      m_selectedColor = Colors.FirstOrDefault(x => x.Name == Settings.Default.AppAccent) ?? Colors.First();
      OnPropertyChanged(nameof(SelectedAppMode));
      OnPropertyChanged(nameof(SelectedColor));

      IsLoading = false;
    }

    private void SaveSettings()
    {
      Settings.Default.Language = m_selectedLanguage.Info.Name;
      Settings.Default.AppMode = m_selectedAppMode;
      Settings.Default.AppAccent = m_selectedColor.Name;

      Settings.Default.Save();

      CultureResources.ChangeCulture(m_selectedLanguage.Info);
      RibbonLocalization.Current.Culture = m_selectedLanguage.Info;
      App.SetTheme(m_selectedAppMode);
      App.SetAccent(m_selectedColor.Name);
    }

    #endregion

    #region Fields

    private const string ENGLISH_ISO_NAME = "en";
    private ObservableCollection<Language> m_availableLanguages;
    private Language m_selectedLanguage;
    private bool m_isLoading = true;
    private bool m_selectedAppMode;
    private ObservableCollection<Color> m_colors;
    private Color m_selectedColor;

    #endregion

    #region Properties

    public ObservableCollection<Language> AvailableLanguages
    {
      get => m_availableLanguages;
      set
      {
        m_availableLanguages = value;
        OnPropertyChanged(nameof(AvailableLanguages));
      }
    }

    public Language SelectedLanguage
    {
      get => m_selectedLanguage;
      set
      {
        m_selectedLanguage = value;
        OnPropertyChanged(nameof(SelectedLanguage));
      }
    }

    public bool SelectedAppMode
    {
      get => m_selectedAppMode;
      set
      {
        m_selectedAppMode = value;
        App.SetTheme(m_selectedAppMode);
        OnPropertyChanged(nameof(SelectedAppMode));
      }
    }

    public ObservableCollection<Color> Colors
    {
      get => m_colors;
      set
      {
        m_colors = value;
        OnPropertyChanged(nameof(Colors));
      }
    }

    public Color SelectedColor
    {
      get => m_selectedColor;
      set
      {
        m_selectedColor = value;
        App.SetAccent(m_selectedColor.Name);
        OnPropertyChanged(nameof(SelectedColor));
      }
    }

    public bool IsLoading
    {
      get => m_isLoading;
      set
      {
        m_isLoading = value;
        OnPropertyChanged(nameof(IsLoading));
      }
    }

    #endregion

    #region Commands

    public ICommand LoadedCommand =>
      new RelayCommand(async () => await LoadSettings());

    public ICommand ClosingCommand =>
      new RelayCommand(SaveSettings);

    public override ICommand ApplyCommand
      => new RelayCommand(SaveSettings);

    #endregion
  }
}