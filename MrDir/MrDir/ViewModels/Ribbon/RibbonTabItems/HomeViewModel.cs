﻿using Fluent;
using MrDir.Interfaces.Models;
using MrDir.Managers;
using MrDir.Views.Ribbon.RibbonTabItems;
using System.ComponentModel.Composition;
using System.Windows.Input;
using MrDir.Cultures;
using MrDir.Types;

namespace MrDir.ViewModels.Ribbon.RibbonTabItems
{
  [Export(typeof(IRibbonTabItem))]
  public class HomeViewModel
    : BaseViewModel, IRibbonTabItem
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public HomeViewModel()
    {
      View = new HomeView(this);
    }

    #region Properties

    public bool IsMultiTabSelectionEnabled
    {
      get => TabManager.IsTabSelectionEnabled;
      set
      {
        TabManager.IsTabSelectionEnabled = value;
        OnPropertyChanged(nameof(IsMultiTabSelectionEnabled));
      }
    }

    #endregion

    #region Commands

    public ICommand SelectAllTabsCommand
      => new RelayCommand(TabManager.SelectAllTabs);

    public ICommand DeselectAllTabsCommand
      => new RelayCommand(TabManager.UnselectAllTabs);

    #endregion

    #region IRibbonTabItem Members

    public int CompareTo(IRibbonTabItem other)
      => Order.CompareTo(other.Order);

    public bool Equals(IRibbonTabItem other)
      => Order == other?.Order;

    /// <inheritdoc />
    public int Order => 0;

    /// <inheritdoc />
    public string Header => Resources.Home;

    /// <inheritdoc />
    public RibbonTabItem View { get; }

    #endregion
  }
}