﻿using MrDir.Cultures;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace MrDir.Validators
{
	public class StringNotInBlackListValidationRule
		: ValidationRule
	{
		#region Fields

		private BlackListBinding m_blackListSource;
		private IEnumerable<string> m_blackList;
		private Lazy<string> m_errorMessage;

		#endregion

		#region Properties

		public BlackListBinding BlackListSource
		{
			get => m_blackListSource;
			set
			{
				m_blackListSource = value;
				m_blackList = m_blackListSource.BlackList;
			}
		}

		public string ErrorStringKey { get; set; }

		#endregion

		public StringNotInBlackListValidationRule()
		{
			m_errorMessage = new Lazy<string>(() =>
			{
				return string.IsNullOrEmpty(ErrorStringKey)
					? Resources.Error
					: App.CultureManager.GetString(ErrorStringKey);
			});
		}

		public override ValidationResult Validate(object value, CultureInfo cultureInfo)
		{
			var toValidate = (string)value;
			if (m_blackList.Contains(toValidate))
				return new ValidationResult(false, m_errorMessage.Value);

			return ValidationResult.ValidResult;
		}
	}

	public class BlackListBinding
		: DependencyObject
	{
		public static readonly DependencyProperty m_blackList
			= DependencyProperty.Register(nameof(BlackList),
																		typeof(IEnumerable<string>),
																		typeof(BlackListBinding),
																		new PropertyMetadata(default(IEnumerable<string>)));

		public IEnumerable<string> BlackList
		{
			get => (IEnumerable<string>)GetValue(m_blackList);
			set => SetValue(m_blackList, value);
		}
	}
}
