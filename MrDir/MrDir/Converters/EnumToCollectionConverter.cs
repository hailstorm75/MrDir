﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using MrDir.Helpers;

namespace MrDir.Converters
{
  [ValueConversion(typeof(Enum), typeof(IEnumerable<Tuple<object, object>>))]
  public class EnumToCollectionConverter
    : BaseConverter, IValueConverter
  {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
			=> EnumHelper.GetAllEnumInfos(value?.GetType());

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
			=> null;
	}
}