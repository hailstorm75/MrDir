﻿using System;
using System.Globalization;
using System.Windows.Data;
using MrDir.Helpers;

namespace MrDir.Converters
{
  [ValueConversion(typeof(long), typeof(string))]
  public class BytesToStringConverter
    : BaseConverter, IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value != null
        ? ConverterHelper.BytesToString((ulong) value)
        : "? B";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return null;
    }
  }
}