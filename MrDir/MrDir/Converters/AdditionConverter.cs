﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MrDir.Converters
{
  [ValueConversion(typeof(double), typeof(double), ParameterType = typeof(double))]
  public class AdditionConverter
    : BaseConverter, IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      => (dynamic) value + (dynamic) parameter;

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      => (dynamic) value - (dynamic) parameter;
  }
}