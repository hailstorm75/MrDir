﻿using MrDir.Cultures;
using System;
using System.Globalization;
using System.Resources;
using System.Windows.Data;

namespace MrDir.Converters
{
	[ValueConversion(typeof(string), typeof(string))]
	public class KeyToCultureConverter
    : BaseConverter, IValueConverter
	{
		private static ResourceManager m_manager = new ResourceManager(typeof(Resources));

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
			=> value == null ? string.Empty : m_manager.GetString((string)value);

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
			=> null;
	}
}
