﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Data;
using System.Globalization;

namespace MrDir.Converters
{
	public class ValueConverterGroup
		: List<IValueConverter>, IValueConverter
	{
		private string[] m_parameters;
		private bool m_shouldReverse = false;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			ExtractParameters(parameter);

			if (m_shouldReverse)
			{
				Reverse();
				m_shouldReverse = false;
			}

			return this.Aggregate(value, (current, converter) => converter.Convert(current, targetType, GetParameter(converter), culture));
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			ExtractParameters(parameter);

			Reverse();
			m_shouldReverse = true;

			return this.Aggregate(value, (current, converter) => converter.ConvertBack(current, targetType, GetParameter(converter), culture));
		}

		private void ExtractParameters(object parameter)
		{
			if (parameter != null)
				m_parameters = Regex.Split(parameter.ToString(), @"(?<!\\),");
		}

		private string GetParameter(IValueConverter converter)
		{
			if (m_parameters == null)
				return null;

			var index = IndexOf(converter as IValueConverter);
			string parameter;

			try
			{
				parameter = m_parameters[index];
			}
			catch (IndexOutOfRangeException ex)
			{
				parameter = null;
			}

			if (parameter != null)
				parameter = Regex.Unescape(parameter);

			return parameter;
		}
	}
}
