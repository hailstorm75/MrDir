﻿using MrDir.Helpers;
using System;
using System.Globalization;
using System.Windows.Data;

namespace MrDir.Converters
{
	[ValueConversion(typeof(Enum), typeof(string))]
	public class EnumToStringConverter
		: BaseConverter, IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if ((string)parameter == "description")
				return (value as Enum)?.Description();
			else if ((string)parameter == "name")
				return (value as Enum)?.Name();
			return "Enum conv error";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
			=> null;
	}
}