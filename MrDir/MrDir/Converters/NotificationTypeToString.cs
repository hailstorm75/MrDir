﻿using MrDir.Attributes;
using MrDir.Enums;
using System;
using System.Globalization;
using System.Windows.Data;

namespace MrDir.Converters
{
  [ValueConversion(typeof(NotificationType), typeof(string))]
	public class NotificationTypeToString
		: BaseConverter, IValueConverter
	{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      => NotificationMetadataAttribute.Metadata(value as Enum).Item1;

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      => null;
  }
}
