﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Common.Data;
using MrDir.Enums;
using MrDir.Interfaces.Models;

namespace MrDir.Managers
{
  public class NotificationManager
    : Singleton<NotificationManager>
  {
    private NotificationManager()
    {
      Notifications = new ObservableCollection<INotification>();
      TrackedNotifications = new List<NotificationType>
      {
        NotificationType.Error,
        NotificationType.Warning
      };
    }

    #region Properties

    public ObservableCollection<INotification> Notifications { get; }
    private List<NotificationType> TrackedNotifications { get; }

    #endregion

    #region Methods

    public void Notify(INotification notification)
    {
      Notifications.Add(notification);
      if (TrackedNotifications.Contains(notification.Type))
        OnNewTrackedNotification();
    }

    #endregion

    #region Events

    public event EventHandler NewTrackedNotification;

    private void OnNewTrackedNotification()
    {
      NewTrackedNotification?.Invoke(this, EventArgs.Empty);
    }

    #endregion
  }
}