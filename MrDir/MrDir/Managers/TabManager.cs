﻿using Dragablz;
using Dragablz.Dockablz;
using MrDir.Helpers;
using MrDir.Interfaces.ViewModels;
using MrDir.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using MrDir.ViewModels;
using MrDir.Types;

namespace MrDir.Managers
{
  public static class TabManager
  {
    #region Fields

    private static bool m_isTabSelectionEnabled;

    #endregion

    static TabManager()
    {
      InterTabClient = new InterTabClient();
      AllTabs = new HashSet<TabContentViewModel>();
      SelectedTabs = new HashSet<TabContentViewModel>();
      WindowTabs = new Dictionary<Window, Layout>();
      DragablzManager.TabSelectionChanged += DragablzManagerOnTabSelectionChanged;
    }

    #region Properties

    public static Func<object, TabContentViewModel> NewItemFactory
    {
      get
      {
        return p =>
        {
          var tab = new TabContentViewModel(new TabContentModel(p as string));
          AllTabs.Add(tab);
					OnTabAdded(null, tab);

          return tab;
        };
      }
    }

    public static bool IsTabSelectionEnabled
    {
      get => m_isTabSelectionEnabled;
      set
      {
        ToggleTabSelection(value ? Visibility.Visible : Visibility.Collapsed);
        m_isTabSelectionEnabled = value;
      }
    }

    public static IInterTabClient InterTabClient { get; }
    public static HashSet<TabContentViewModel> AllTabs { get; }
    public static HashSet<TabContentViewModel> SelectedTabs { get; }
    public static TabContentViewModel ActiveTab { get; set; }
    public static Dictionary<Window, Layout> WindowTabs { get; }

    #endregion

    #region Methods

    private static void DragablzManagerOnTabSelectionChanged(object sender, TabSelectionEventArgs e)
    {
      if (e.Content is TabContentViewModel tabContent)
        Activate(tabContent);
    }

    public static void Activate(TabContentViewModel tabContentViewModel)
    {
      if (ActiveTab == tabContentViewModel)
        return;
      ActiveTab?.Deactivate();
      ActiveTab = tabContentViewModel;
      ActiveTab.Activate();
			OnTabActivated(null, ActiveTab);
    }

    private static void ToggleTabSelection(Visibility visibility)
    {
      foreach (var header in AllTabs.Where(x => x.Header is ITabHeaderViewModel)
        .Select(x => x.Header)
        .Cast<ITabHeaderViewModel>())
        header.CheckBoxVisiblility = visibility;
    }

    public static void RemoveTab(this TabContentViewModel tab)
    {
      AllTabs.Remove(tab);

      if (SelectedTabs.Any())
        SelectedTabs.Remove(tab);

      tab.Dispose();
    }

    public static void SelectTab(this TabContentViewModel tab)
      => SelectedTabs.Add(tab);

    public static void UnselectTab(this TabContentViewModel tab)
      => SelectedTabs.Remove(tab);

    public static IEnumerable<TabContentViewModel> GetTabs(Window window)
      => WindowTabs[window]
        .FindVisualChildren<TabablzControl>()
        .Select(x => x.Items.SourceCollection.Cast<TabContentViewModel>())
        .SelectMany(x => x);

    public static void SelectAllTabs()
    {
      foreach (var header in AllTabs.Where(x => x.Header is ITabHeaderViewModel)
        .Select(x => x.Header)
        .Cast<ITabHeaderViewModel>())
        header.IsSelected = true;
    }

    public static void UnselectAllTabs()
    {
      foreach (var header in AllTabs.Where(x => x.Header is ITabHeaderViewModel)
        .Select(x => x.Header)
        .Cast<ITabHeaderViewModel>())
        header.IsSelected = false;
    }

		#endregion

		#region Events

		public static event EventHandler<EventArgs<TabContentViewModel>> TabAdded;
		public static event EventHandler<EventArgs<TabContentViewModel>> TabRemoved;
		public static event EventHandler<EventArgs<TabContentViewModel>> TabActivated;

		private static void OnTabAdded(object sender, TabContentViewModel tab)
			=> TabAdded?.Invoke(sender, new EventArgs<TabContentViewModel>(tab));

		private static void OnTabRemoved(object sender, TabContentViewModel tab)
			=> TabRemoved?.Invoke(sender, new EventArgs<TabContentViewModel>(tab));

		private static void OnTabActivated(object sender, TabContentViewModel tab)
			=> TabActivated?.Invoke(sender, new EventArgs<TabContentViewModel>(tab));

		#endregion
	}
}