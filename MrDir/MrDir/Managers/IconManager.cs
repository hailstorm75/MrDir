﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace MrDir.Managers
{
  public static class IconManager
  {
    #region Fields

    private const uint SHGFI_ICON = 0x100;

    //private const uint SHGFI_LARGEICON = 0x0;
    private const uint SHGFI_SMALLICON = 0x000000001;
    private static readonly Lazy<Icon> LAZY_FOLDER_ICON = new Lazy<Icon>(FetchFolderIcon, true);

    #endregion

    #region Properties

    public static Icon FolderSmall
      => LAZY_FOLDER_ICON.Value;

    #endregion

    #region Methods

    public static Icon ExtractIconFromPath(string path)
    {
      var shinfo = new SHFILEINFO();
      SHGetFileInfo(
        path,
        0, ref shinfo, (uint)Marshal.SizeOf(shinfo),
        SHGFI_ICON | SHGFI_SMALLICON);
      return Icon.FromHandle(shinfo.hIcon);
    }

    private static Icon FetchFolderIcon()
    {
      var tmpDir = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString())).FullName;
      var icon = ExtractIconFromPath(tmpDir);
      Directory.Delete(tmpDir);
      return icon;
    }

    [DllImport("shell32.dll")]
    private static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi,
      uint cbSizeFileInfo, uint uFlags);

    #endregion

    //Struct used by SHGetFileInfo function
    [StructLayout(LayoutKind.Sequential)]
    private struct SHFILEINFO
    {
      public readonly IntPtr hIcon;
      public readonly int iIcon;
      public readonly uint dwAttributes;

      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
      public readonly string szDisplayName;

      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
      public readonly string szTypeName;
    }
  }
}