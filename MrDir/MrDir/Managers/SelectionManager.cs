﻿using Common.Data;
using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Views;
using MrDir.Types;
using MrDir.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace MrDir.Managers
{
  public class SelectionManager
    : Singleton<SelectionManager>, INotifyPropertyChanged
  {
    #region Fields

    private readonly bool m_selectionProvidersExist;
    private IBrowserPanelViewModel m_selectionProvider;

    #endregion

    #region Properties

    public bool AnyIFSEntitySelected
      => SelectionProvider?.Selection?.Any() ?? false;

    public IEnumerable<object> ActiveSelection
      => SelectionProvider?.Selection ?? Enumerable.Empty<object>();

    private IBrowserPanelViewModel SelectionProvider
    {
      get => m_selectionProvider;
      set
      {
        if (m_selectionProvider != null)
          m_selectionProvider.SelectedEntitiesCountChanged -= OnPanelSelectedEntitiesCountChanged;

        m_selectionProvider = value;

        m_selectionProvider.SelectedEntitiesCountChanged += OnPanelSelectedEntitiesCountChanged;
      }
    }

    #endregion

    protected SelectionManager()
    {
      m_selectionProvidersExist = PanelManager.Instance.AvailablePanels.Any(x => x.Metadata.Type == typeof(IBrowserPanelView));
      if (m_selectionProvidersExist)
        TabManager.TabActivated += OnTabActivated;
    }

    private void OnTabActivated(object sender, EventArgs<TabContentViewModel> e)
    {
      if (e.Value.ContentPanel is IBrowserPanelViewModel panel)
      {
        SelectionProvider = panel;
        OnPropertyChanged(nameof(AnyIFSEntitySelected));
        OnPropertyChanged(nameof(ActiveSelection));
      }
    }

    private void OnPanelSelectedEntitiesCountChanged(object sender, EventArgs e)
    {
      OnPropertyChanged(nameof(AnyIFSEntitySelected));
      OnPropertyChanged(nameof(ActiveSelection));
    }

    protected void OnPropertyChanged(string propertyName = "")
      => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

    [field: NonSerialized]
    public event PropertyChangedEventHandler PropertyChanged;
  }
}
