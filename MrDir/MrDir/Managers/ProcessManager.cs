﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Common.Data;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MrDir.Cultures;
using MrDir.Enums;
using MrDir.Interfaces.Models;
using MrDir.Properties;
using MrDir.Views.Dialogs.CustomMessageDialogs;

namespace MrDir.Managers
{
  public class ProcessManager
    : Singleton<ProcessManager>, INotifyPropertyChanged
  {
    #region Fields

    private int m_runningProcesses;

    #endregion

    private ProcessManager()
    {
      Processes = new ObservableCollection<IProcess>();
    }

    #region Properties

    public ObservableCollection<IProcess> Processes { get; }

    public int RunningProcesses
    {
      get => m_runningProcesses;
      set
      {
        m_runningProcesses = value;
        OnPropertyChanged(nameof(RunningProcesses));
      }
    }

    #endregion

    #region Methods

    public async Task<ProcessResult> StartProcessAsync(IProcess process, bool requireConfirm = false)
    {
      if (requireConfirm)
      {
        var activeWindow = App.GetActiveWindow() as MetroWindow;
        var res = await MessageDialogView.Initialize(activeWindow, process.Name, Resources.ConfirmAction).ConfigureAwait(true);

        if (res != MessageDialogResult.Affirmative) return ProcessResult.NotRun;
      }

      Processes.Add(process);
      RunningProcesses++;
      OnPropertyChanged(nameof(Processes));

      return await process.Run().ConfigureAwait(false);
    }

    public void ProcessFinished(IProcess process)
    {
      RunningProcesses--;
    }

    public void ClearFinished()
    {
      foreach (var item in Processes.Where(x => x.IsComplete).ToList())
        Processes.Remove(item);
    }

    public void Clear(IProcess process)
    {
      Processes.Remove(process);
    }

    #endregion

    #region INotifyPropertyChanged

    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    private void OnPropertyChanged([CallerMemberName] string propertyName = "")
      => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

    #endregion
  }
}