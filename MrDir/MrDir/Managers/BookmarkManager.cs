﻿using SchwabenCode.QuickIO;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Common.Data;
using Common.Linq;
using MrDir.Types.Observable;
using MrDir.Interfaces.Models;
using MrDir.Models.Bookmarks;

namespace MrDir.Managers
{
  public static class BookmarkManager
  {
    #region Fields

    private static readonly string APPDATA = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MrDir");
    private static readonly string APPDATA_SHORTCUTS = Path.Combine(APPDATA, "bookmarks.bin");

    #endregion

    #region Properties

    public static ObservableCollection<ObservablePair<string, ObservableCollection<IBookmark>>> Shortcuts { get; private set; }

    #endregion

    static BookmarkManager()
    {
      if (!QuickIODirectory.Exists(APPDATA))
        QuickIODirectory.Create(APPDATA);

      Load();
    }

    #region Methods

    public static void AddShortcut(IFSEntity item)
    {
      IBookmark newItem;
      switch (item)
      {
        case Models.FileSystem.Directory dir:
          newItem = new BookmarkToDirectory(dir);
          break;
        case Models.FileSystem.File doc:
          newItem = new BookmarkToFile(doc);
          break;
        default:
          return;
      }

      var key = GetRoot(item.Path);
      var index = new Func<int>(() =>
      {
        var indexes = Shortcuts.Select((x, i) => new
        {
          x,
          i
        })
          .Where(x => x.x.Left.Equals(key))
          .Select(x => x.i)
          .ToArray();

        return indexes.Length == 0
          ? -1
          : indexes[0];
      })();

      if (index != -1)
      {
        if (!Shortcuts[index].Right.Contains(newItem))
        {
          Shortcuts[index].Right.Add(newItem);
          Shortcuts[index].Right.Sort((x, y) => string.Compare(x.Label, y.Label, StringComparison.InvariantCultureIgnoreCase));
        }
      }
      else
        Shortcuts.Add(new ObservablePair<string, ObservableCollection<IBookmark>>(key, new ObservableCollection<IBookmark> { newItem }));

      Shortcuts.Sort((x, y) => string.Compare(x.Left, y.Left, StringComparison.InvariantCultureIgnoreCase));

      Save();
      OnSideMenuItemChanged();
    }

    public static void Remove(string path)
    {
      var key = GetRoot(path);
      var index = new Func<int>(() =>
      {
        var indexes = Shortcuts.Select((x, i) => new
        {
          x,
          i
        })
          .Where(x => x.x.Left.Equals(key))
          .Select(x => x.i)
          .ToArray();

        return indexes.Length == 0
          ? -1
          : indexes[0];
      })();

      if (index == -1) return;

      var item = Shortcuts[index].Right.FirstOrDefault(x => x.Path.Equals(path));
      if (item == null)
        return;

      Shortcuts[index].Right.Remove(item);
      if (Shortcuts[index].Right.Count == 0)
        Shortcuts.RemoveAt(index);

      Save();
      OnSideMenuItemChanged();
    }

    private static string GetRoot(string path)
    {
      string root = null;
      if (QuickIOPath.IsLocalRegularPath(path))
        root = QuickIODirectory.GetDirectoryRoot(path).FullName;
      else if (QuickIOPath.IsShareRegularPath(path))
      {
        var index = path.Substring(2).IndexOf(@"\", StringComparison.InvariantCultureIgnoreCase) + 2;
        if (index == 1)
          throw new ArgumentException(nameof(path));

        root = path.Substring(0, index + 1);
      }

      return root ?? throw new NotSupportedException("Path type not supported");
    }

    private static void Save()
    {
      using (var fs = new FileStream(APPDATA_SHORTCUTS, FileMode.Create))
        Shortcuts.SerializeBinary(fs);
    }

    private static void Load()
    {
      if (!QuickIOFile.Exists(APPDATA_SHORTCUTS))
        Shortcuts = new ObservableCollection<ObservablePair<string, ObservableCollection<IBookmark>>>();
      else
        using (var fs = new FileStream(APPDATA_SHORTCUTS, FileMode.OpenOrCreate))
          Shortcuts = Serialization.DeserializeBinary<ObservableCollection<ObservablePair<string, ObservableCollection<IBookmark>>>>(fs);

      OnSideMenuItemChanged();
    }

    #endregion

    #region Events

    public static event EventHandler SideMenuItemsChanged;
    private static void OnSideMenuItemChanged()
      => SideMenuItemsChanged?.Invoke(null, EventArgs.Empty);

    #endregion
  }
}
