﻿using MrDir.Helpers;
using MrDir.Models;
using System.Collections.Generic;
using System.Linq;

namespace MrDir.Managers
{
	public static class ContextMenuManager
	{
    #region Fields

    private static readonly List<ContextMenuItem> ENTITIES_TOP;
    private static readonly List<ContextMenuItem> ENTITIES_BOTTOM;

    public static readonly List<ContextMenuItem> FILE_CONTEXT_MENU;
    public static readonly List<ContextMenuItem> DIR_CONTEXT_MENU;
    public static readonly List<ContextMenuItem> MULTI_CONTEXT_MENU;
    public static readonly List<ContextMenuItem> SHORTCUT_CONTEXT_MENU;
    public static readonly List<ContextMenuItem> NO_SELECTION_CONTEXT_MENU;

    #endregion

    static ContextMenuManager()
		{
			ENTITIES_TOP = new List<ContextMenuItem>
			{
				new ContextMenuItem
				{
					Name = "Open",
					ToolTip = "Open",
					Command = CommandHelper.OpenCommand
				}
			};
			ENTITIES_BOTTOM = new List<ContextMenuItem>
			{
				new ContextMenuItem
				{
					Name = "Copy",
					ToolTip = "Copy selected items",
					Command = CommandHelper.CopyCommand,
				},

				new ContextMenuItem
				{
					Name = "Rename",
					ToolTip = "Rename selected items",
					Command = CommandHelper.RenameCommand
				},
				new ContextMenuItem
				{
					Name = "Delete",
					ToolTip = "Delete",
					Command = CommandHelper.DeleteCommand,
				},
				new ContextMenuItem
				{
					Name = "Properties",
					ToolTip = "Properties",
					Command = CommandHelper.OpenPropertiesCommand,
				}
			};

			MULTI_CONTEXT_MENU = ENTITIES_TOP.Concat(ENTITIES_BOTTOM).ToList();
			FILE_CONTEXT_MENU = ENTITIES_TOP.Concat(new List<ContextMenuItem>
			{
				new ContextMenuItem
				{
					Name = "Add to shortcuts",
					ToolTip = "Add to shortcuts in MrDir for quick access",
					Command = CommandHelper.CreateMrDirShortcutCommand
				}
			}).Concat(ENTITIES_BOTTOM).ToList();
			DIR_CONTEXT_MENU = ENTITIES_TOP.Concat(new List<ContextMenuItem>
			{
				new ContextMenuItem
				{
					Name = "Add to bookmarks",
					ToolTip = "Add to bookmarks in MrDir for quick access",
					Command = CommandHelper.CreateMrDirShortcutCommand
				},
				new ContextMenuItem
				{
					Name = "Paste",
					ToolTip = "Paste items from clipboard",
					Command = CommandHelper.PasteCommand,
				}
			}).Concat(ENTITIES_BOTTOM).ToList();
			SHORTCUT_CONTEXT_MENU = ENTITIES_TOP.Concat(new List<ContextMenuItem>
			{
				new ContextMenuItem
				{
					Name = "Remove from shortcuts",
					ToolTip = "Remove from shortcuts in MrDir for quick access",
					Command = CommandHelper.RemoveMrDirShortcutCommand
				}
			}).Concat(ENTITIES_BOTTOM).ToList();
			NO_SELECTION_CONTEXT_MENU = new List<ContextMenuItem>
			{
				new ContextMenuItem
				{
					Name = "New folder",
					ToolTip = "Create a new folder",
					Command = CommandHelper.NewFolderCommand
				},
        new ContextMenuItem
        {
					Name = "Add to bookmarks",
					ToolTip = "Add to bookmarks in MrDir for quick access",
					Command = CommandHelper.CreateMrDirShortcutCommand
        },
				new ContextMenuItem
				{
					Name = "Paste",
					ToolTip = "Paste items from clipboard",
					Command = CommandHelper.PasteCommand,
				}
			};
		}
	}
}
