﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Data;
using MrDir.Attributes;
using MrDir.Interfaces;
using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Views;

namespace MrDir.Managers
{
  public class PanelManager
    : Singleton<PanelManager>
  {
    #region Properties

    [ImportMany(typeof(IPanelProvider))]
    public IEnumerable<Lazy<IPanelProvider, IPanelViewMetadata>> AvailablePanels { get; set; }

    public IEnumerable<IPanelViewMetadata> GetPanels()
      => AvailablePanels.Select(x => x.Metadata);

    #endregion

    public IPanelView<IPanelViewModel> GetPanel(string guid)
    {
      var panel = AvailablePanels.FirstOrDefault(x => x.Metadata.Guid.Equals(guid))?.Value
                  ?? AvailablePanels.First().Value;
      return panel.Provide();
    }

    protected PanelManager()
      => ComposerManager.Compose(this);
  }
}