﻿using MrDir.Helpers;

namespace MrDir.Managers
{
  internal static class ComposerManager
  {
    private static readonly Composer COMPOSER;

    static ComposerManager()
      => COMPOSER = new Composer();

    public static void Compose(object importer)
      => COMPOSER.Compose(importer);
  }
}