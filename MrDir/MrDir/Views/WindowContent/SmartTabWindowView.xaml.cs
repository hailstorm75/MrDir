﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Views;

namespace MrDir.Views.WindowContent
{
  /// <summary>
  ///   Interaction logic for SmartWindowView.xaml
  /// </summary>
  [Export(typeof(IWindowView<ITabWindowViewModel>))]
  public partial class SmartWindowView
    : UserControl, IWindowView<ITabWindowViewModel>
  {
    public SmartWindowView()
    {
      InitializeComponent();
    }

    [Import(typeof(ITabWindowViewModel))]
    public ITabWindowViewModel ViewModel
    {
      get => DataContext as ITabWindowViewModel;
      set => DataContext = value;
    }

    public Control MainControl => RootLayout;
  }
}