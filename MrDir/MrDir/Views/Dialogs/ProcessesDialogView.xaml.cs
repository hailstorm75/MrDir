﻿using MahApps.Metro.Controls;

namespace MrDir.Views.Dialogs
{
  /// <summary>
  ///   Interaction logic for ProcessesWindowView.xaml
  /// </summary>
  public partial class ProcessesDialogView
    : MetroWindow
  {
    public ProcessesDialogView()
    {
      InitializeComponent();
    }
  }
}