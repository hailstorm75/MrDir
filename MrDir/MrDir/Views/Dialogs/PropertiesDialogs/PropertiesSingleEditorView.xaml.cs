﻿using MrDir.Interfaces.Models;
using MrDir.Models.FileSystem.Properties;
using MrDir.ViewModels.Dialogs.PropertiesDialogs;
using System.Windows.Data;

namespace MrDir.Views.Dialogs.PropertiesDialogs
{
	/// <summary>
	/// Interaction logic for PropertiesSingleView.xaml
	/// </summary>
	public partial class PropertiesSingleEditorView
		: BasePropertiesEditorView
	{
		public PropertiesSingleEditorViewModel ViewModel
		{
			get => (PropertiesSingleEditorViewModel)DataContext;
			set => DataContext = value;
		}

		public PropertiesSingleEditorView(IFSEntity item)
		{
			ViewModel = new PropertiesSingleEditorViewModel(item);
			InitializeComponent();
		}
	}
}
