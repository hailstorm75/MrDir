﻿using System.Collections.Generic;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using MrDir.Interfaces.Models;
using MrDir.ViewModels.Dialogs.PropertiesDialogs;

namespace MrDir.Views.Dialogs.PropertiesDialogs
{
  /// <summary>
  /// Interaction logic for PropertiesDialogView.xaml
  /// </summary>
  public partial class PropertiesDialogView
    : MetroWindow
  {
    public PropertiesDialogViewModel ViewModel
    {
      get => (PropertiesDialogViewModel)DataContext;
      set => DataContext = value;
    }

    public PropertiesDialogView(IEnumerable<IFSEntity> items)
    {
      ViewModel = new PropertiesDialogViewModel(items);

      InitializeComponent();
    }
  }
}
