﻿using MahApps.Metro.Controls;
using MrDir.Interfaces.Views;
using MrDir.ViewModels.Dialogs;
using SchwabenCode.QuickIO;
using System;
using System.Collections.Generic;

namespace MrDir.Views.Dialogs
{
  /// <summary>
  /// Interaction logic for ResolveOverrideConflictsDialogView.xaml
  /// </summary>
  public partial class ResolveOverwriteConflictsDialogView
    : MetroWindow, IView<ResolveOverwriteConflictsViewModel>
  {
    public ResolveOverwriteConflictsDialogView(IEnumerable<(string, string)> conflicts)
    {
      ViewModel = new ResolveOverwriteConflictsViewModel(conflicts);
      InitializeComponent();
    }

    public ResolveOverwriteConflictsViewModel ViewModel
    {
      get => (ResolveOverwriteConflictsViewModel)DataContext;
      set => DataContext = value;
    }

    public IEnumerable<Tuple<string, string>> Result
      => ViewModel.Result;
  }
}
