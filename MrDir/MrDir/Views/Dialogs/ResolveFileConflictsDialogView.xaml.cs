﻿using MahApps.Metro.Controls;
using MrDir.Interfaces.Models;
using MrDir.Interfaces.Views;
using MrDir.ViewModels.Dialogs;
using System.Collections.Generic;
using System.Linq;

namespace MrDir.Views.Dialogs
{
  /// <summary>
  /// Interaction logic for ResolveFileConflictsDialogView.xaml
  /// </summary>
	/// <inheritdoc cref="IView{T}"/>
  public partial class ResolveFileConflictsDialogView
    : MetroWindow, IView<ResolveFileConflictsDialogViewModel>
  {
		/// <inheritdoc cref="IView{T}.ViewModel"/>
    public ResolveFileConflictsDialogViewModel ViewModel
    {
      get => DataContext as ResolveFileConflictsDialogViewModel;
      set => DataContext = value;
    }

    public IEnumerable<IFSEntity> Result => ViewModel.Result;

    public ResolveFileConflictsDialogView(IEnumerable<IGrouping<string, IFSEntity>> conflicts)
    {
      InitializeComponent();
      ViewModel = new ResolveFileConflictsDialogViewModel(conflicts);
    }
  }
}