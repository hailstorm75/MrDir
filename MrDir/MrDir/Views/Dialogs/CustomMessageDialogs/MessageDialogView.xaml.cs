﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MrDir.Interfaces.Views;
using MrDir.ViewModels.Dialogs.CustomMessageDialogs;
using System.Threading.Tasks;

namespace MrDir.Views.Dialogs.CustomMessageDialogs
{
	/// <summary>
	/// Interaction logic for MessageDialog.xaml
	/// </summary>
	/// <inheritdoc cref="IView{T}"/>
	public partial class MessageDialogView
		: CustomDialog, IView<MessageDialogViewModel>
	{
		/// <inheritdoc cref="IView{T}.ViewModel"/>
		public MessageDialogViewModel ViewModel
		{
			get => (MessageDialogViewModel)DataContext;
			set => DataContext = value;
		}

		public MessageDialogView()
			=> InitializeComponent();

		public static async Task<MessageDialogResult> Initialize(MetroWindow window, string title, string message)
		{
			var dialog = new MessageDialogView();
			dialog.Title = title;
			dialog.ViewModel = new MessageDialogViewModel(async _ => await window.HideMetroDialogAsync(dialog).ConfigureAwait(true), message);

			await window.ShowMetroDialogAsync(dialog).ConfigureAwait(true);
			await dialog.WaitUntilUnloadedAsync().ConfigureAwait(true);

			return dialog.ViewModel.Result;
		}
	}
}
