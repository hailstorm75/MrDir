﻿using MahApps.Metro.Controls;
using MrDir.Interfaces.Views;
using MrDir.ViewModels.Dialogs;

namespace MrDir.Views.Dialogs
{
	/// <summary>
	/// Interaction logic for NotificationsWindowView.xaml
	/// </summary>
	/// <inheritdoc cref="IView{T}"/>
	public partial class NotificationsDialogView
		: MetroWindow, IView<NotificationsWindowViewModel>
	{
		public NotificationsDialogView()
		{
			ViewModel = new NotificationsWindowViewModel();
			InitializeComponent();
		}

		/// <inheritdoc cref="IView{T}.ViewModel"/>
		public NotificationsWindowViewModel ViewModel
		{
			get => DataContext as NotificationsWindowViewModel;
			set => DataContext = value;
		}
	}
}