﻿using MahApps.Metro.Controls;
using MrDir.Interfaces.Views;
using MrDir.ViewModels.Dialogs.RenameDialogs;
using System.Collections.Generic;

namespace MrDir.Views.Dialogs.RenameDialogs
{
	/// <inheritdoc cref="IView{T}"/>
	public class BaseRenameDialogView
		: MetroWindow, IView<BaseRenameDialogViewModel>
	{
		/// <inheritdoc cref="IView{T}.ViewModel"/>
		public BaseRenameDialogViewModel ViewModel
		{
			get => DataContext as BaseRenameDialogViewModel;
			set => DataContext = value;
		}

		public IEnumerable<object> Result => ViewModel.Result;
	}
}
