﻿using System.Collections.Generic;
using MrDir.Interfaces.Models;
using MrDir.ViewModels.Dialogs.RenameDialogs;

namespace MrDir.Views.Dialogs.RenameDialogs
{
	/// <summary>
	/// Interaction logic for RenameSingleDialogView.xaml
	/// </summary>
	public partial class RenameSingleDialogView
    : BaseRenameDialogView
  {
    public RenameSingleDialogView(ICollection<IFSEntity> entities, string path)
    {
      // ViewModel initialization must be before component
      // Otherwise MainText doesn't get selection length
      ViewModel = new RenameSingleDialogViewModel(entities, path);
      InitializeComponent();
      MainText.Focus();
    }
  }
}
