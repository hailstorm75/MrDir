﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Dragablz;
using MahApps.Metro.Controls;
using MrDir.Interfaces.Models;
using MrDir.ViewModels.Dialogs.RenameDialogs;

namespace MrDir.Views.Dialogs.RenameDialogs
{
  /// <summary>
  /// Interaction logic for RenameDialogView.xaml
  /// </summary>
  public partial class RenameDialogView
    : BaseRenameDialogView
  {
    public RenameDialogView(ICollection<IFSEntity> entities, string path)
    {
      ViewModel = new RenameDialogViewModel(entities, path);
      InitializeComponent();
    }

    private void StackPositionMonitor_OnOrderChanged(object sender, OrderChangedEventArgs e)
    {
      ((RenameDialogViewModel) ViewModel).Tabs =
        e.NewOrder.Cast<UIElement>().Select((x, i) => x.TryFindParent<DragablzItem>().Uid).ToList();
    }
  }
}