﻿using MahApps.Metro.Controls;
using MrDir.Interfaces.Views;
using MrDir.ViewModels.Dialogs;

namespace MrDir.Views.Dialogs
{
	/// <summary>
	/// Interaction logic for SettingsWindowView.xaml
	/// </summary>
	/// <inheritdoc cref="IView{T}"/>
	public partial class SettingsDialogView
		: MetroWindow, IView<SettingsWindowViewModel>
	{
		/// <inheritdoc cref="IView{T}.ViewModel"/>
		public SettingsWindowViewModel ViewModel
		{
			get => (SettingsWindowViewModel)DataContext;
			set => DataContext = value;
		}

		public SettingsDialogView()
		{
			InitializeComponent();
			ViewModel = new SettingsWindowViewModel();
		}
	}
}