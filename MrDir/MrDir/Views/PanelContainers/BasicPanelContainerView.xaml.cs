﻿using System;
using System.Windows;
using System.Windows.Controls;
using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Views;
using MrDir.ViewModels.TabHeaders;

namespace MrDir.Views.PanelContainers
{
  /// <summary>
  ///   Interaction logic for BasicTabPage.xaml
  /// </summary>
  public partial class BasicPanelContainer
    : UserControl, IPanelContainerView
  {
    public BasicPanelContainer()
    {
      InitializeComponent();
      Header = new SelectableTabHeaderViewModel
      {
        Header = "Tab",
        CheckBoxVisiblility = Visibility.Collapsed
      };
    }

    public ITabHeaderViewModel Header { get; set; }

    public IPanelContainerViewModel ViewModel
    {
      get => throw new NotImplementedException();
      set => throw new NotImplementedException();
    }

    public bool IsActivated { get; set; }

    public void Activate()
    {
      ViewModel.Activate();
    }

    public void Deactivate()
    {
      ViewModel.Deactivate();
    }

    public void Dispose()
    {
      ViewModel?.Dispose();
      Header?.Dispose();

      ViewModel = null;
      Header = null;
    }
  }
}