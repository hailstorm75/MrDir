﻿using System;
using System.Windows;
using System.Windows.Controls;
using MrDir.Helpers;
using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Views;
using MrDir.Types;
using MrDir.ViewModels.PanelContainers;
using MrDir.ViewModels.TabHeaders;
using Newtonsoft.Json;

namespace MrDir.Views.PanelContainers
{
  /// <summary>
  /// Interaction logic for SmartTabPage.xaml
  /// </summary>
  [JsonConverter(typeof(PanelContainerViewConverter))]
  public partial class SmartPanelContainerView
    : UserControl, IPanelContainerView
  {
    public SmartPanelContainerView(string guid)
    {
      InitializeComponent();
      ViewModel = new SmartPanelContainerViewModel(guid);
      ViewModel.RequestedNewTitle += OnRequestedNewTitle;
      Header = new SelectableTabHeaderViewModel
      {
        Header = "New Tab",
        CheckBoxVisiblility = Visibility.Collapsed
      };
    }

    private void OnRequestedNewTitle(object sender, EventArgs<Tuple<string, string>> e)
    {
      Header.Header = e.Value.Item1.Trim2Length(15);
      Header.ToolTip = e.Value.Item2;
    }

    public IPanelContainerViewModel ViewModel
    {
      get => DataContext as IPanelContainerViewModel;
      set => DataContext = value;
    }

    public ITabHeaderViewModel Header { get; set; }
    public bool IsActivated { get; set; }

    public void Deactivate()
    {
      ViewModel.Deactivate();
    }

    public void Activate()
    {
      ViewModel.Activate();
    }

    public void Dispose()
    {
      ViewModel?.Dispose();
      Header?.Dispose();

      ViewModel = null;
      Header = null;
    }

    private void UserControl_GotFocus(object sender, RoutedEventArgs e)
    {
      Activate();
    }
  }

  public class PanelContainerViewConverter
    : JsonConverter
  {
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
      var panel = value as IPanelContainerView;

      writer.WriteStartObject();

      writer.WritePropertyName(nameof(panel.Header));
      serializer.Serialize(writer, panel.Header);

      writer.WritePropertyName(nameof(panel.ViewModel));
      serializer.Serialize(writer, panel.ViewModel);

      writer.WriteEndObject();
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
      var a = 5;
      return new object();
    }

    public override bool CanConvert(Type objectType)
    {
      return true;
    }
  }
}