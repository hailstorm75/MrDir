﻿using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Dragablz;
using MahApps.Metro.Controls;
using MrDir.Helpers;
using MrDir.Interfaces.ViewModels;
using MrDir.Managers;
using MrDir.Models;

namespace MrDir.Views.MainWindows
{
  /// <summary>
  /// Interaction logic for TabWindowView.xaml
  /// </summary>
  public partial class TabWindowView
    : MetroWindow
  {
    public TabWindowView()
    {
      InitializeComponent();
      ComposerManager.Compose(this);
    }

    [Import(typeof(ITabWindowViewModel))]
    public ITabWindowViewModel ViewModel
    {
      get => DataContext as ITabWindowViewModel;
      set => DataContext = value;
    }

    private void TabWindowView_OnLoaded(object sender, RoutedEventArgs e)
    {
      var close = this.FindChild<Button>("PART_Close");
      close.Click += CloseOnClick;
    }

    private void CloseOnClick(object sender, RoutedEventArgs routedEventArgs)
    {
      var items = TabManager.GetTabs(this);
      TabManager.WindowTabs.Remove(this);

      foreach (var item in items)
        item.RemoveTab();

      Close();
    }
  }
}