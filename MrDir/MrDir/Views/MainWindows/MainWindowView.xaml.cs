﻿using Dragablz.Dockablz;
using Fluent;
using MahApps.Metro.Controls;
using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Views;
using MrDir.Managers;
using MrDir.Controls;
using System;
using System.ComponentModel.Composition;
using System.Windows;

namespace MrDir.Views.MainWindows
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	/// <inheritdoc cref="IView{T}"/>
	public partial class MainWindow
		: GestureWindow, IRibbonWindow, IView<IMainContentViewModel<ITabWindowViewModel>>
	{
		public MainWindow()
		{
			InitializeComponent();
			Loaded += MahMetroWindow_Loaded;
			Closed += MahMetroWindow_Closed;
			ComposerManager.Compose(this);
			TabManager.WindowTabs.Add(this, ViewModel.WindowView.MainControl as Layout);
		}

		private void SyncThemeManagers(object sender, OnThemeChangedEventArgs args)
		{
			var fluentRibbonTheme = args?.AppTheme ?? ThemeManager.DetectAppStyle()?.Item1;
			if (fluentRibbonTheme == null) return;
			MahApps.Metro.ThemeManager.ChangeAppTheme(this, fluentRibbonTheme.Name);
		}

		#region Properties

		/// <inheritdoc cref="IView{T}.ViewModel"/>
		[Import(typeof(IMainContentViewModel<ITabWindowViewModel>))]
		public IMainContentViewModel<ITabWindowViewModel> ViewModel
		{
			get => DataContext as IMainContentViewModel<ITabWindowViewModel>;
			set => DataContext = value;
		}

		/// <inheritdoc cref="IRibbonWindow.TitleBar"/>
		public RibbonTitleBar TitleBar
		{
			get => (RibbonTitleBar)GetValue(TitleBarProperty);
			private set => SetValue(TITLE_BAR_PROPERTY_KEY, value);
		}

		private static readonly DependencyPropertyKey TITLE_BAR_PROPERTY_KEY =
			DependencyProperty.RegisterReadOnly(nameof(TitleBar), typeof(RibbonTitleBar), typeof(MainWindow),
				new PropertyMetadata());

		/// <summary>
		/// <see cref="DependencyProperty" /> for <see cref="TitleBar" />.
		/// </summary>
		public static readonly DependencyProperty TitleBarProperty = TITLE_BAR_PROPERTY_KEY.DependencyProperty;

		#endregion

		#region Events

		private void MahMetroWindow_Loaded(object sender, RoutedEventArgs e)
		{
			TitleBar = this.FindChild<RibbonTitleBar>("RibbonTitleBar");
			TitleBar.InvalidateArrange();
			TitleBar.UpdateLayout();

			SyncThemeManagers(null, null);

			ThemeManager.IsThemeChanged += SyncThemeManagers;
		}

		private void MahMetroWindow_Closed(object sender, EventArgs e)
			=> ThemeManager.IsThemeChanged -= SyncThemeManagers;

		#endregion
	}
}