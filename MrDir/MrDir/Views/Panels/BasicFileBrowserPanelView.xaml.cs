﻿using MrDir.Interfaces;
using MrDir.Interfaces.Models;
using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Views;
using MrDir.Types;
using MrDir.ViewModels.Panels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;

namespace MrDir.Views.Panels
{
	/// <inheritdoc cref="IPanelProvider"/>
	[Export(typeof(IPanelProvider))]
	[ExportMetadata(nameof(IPanelViewMetadata.Name), NAME)]
	[ExportMetadata(nameof(IPanelViewMetadata.Guid), GUID)]
	[ExportMetadata(nameof(IPanelViewMetadata.Type), typeof(IBrowserPanelView))]
	public class BasicFileBrowserPanelProvider
		: IPanelProvider
	{
		private const string NAME = "Browser";
		private const string GUID = "83A05438-E171-412B-AD92-E4728A6F8204";

		/// <inheritdoc cref="IPanelProvider.Provide"/>
		public IPanelView<IPanelViewModel> Provide()
			=> new BasicFileBrowserPanelView() as IPanelView<IPanelViewModel>;
	}

	/// <summary>
	/// Interaction logic for BasicFileBrowserPanelView.xaml
	/// </summary>
	/// <inheritdoc cref="IBrowserPanelView"/>
	[JsonConverter(typeof(PanelViewConverter))]
	public sealed partial class BasicFileBrowserPanelView
		: UserControl, IBrowserPanelView
	{
		public BasicFileBrowserPanelView()
		{
			InitializeComponent();
			ViewModel = new BasicFileBrowserPanelViewModel();
			ViewModel.SelectedEntitiesCountChanged += ViewModelOnSelectedEntitiesCountChanged;
			ViewModel.RequestedNewTitle += ViewModelOnRequestedNewTitle;
		}

		#region Properties

		/// <inheritdoc />
		public IBrowserPanelViewModel ViewModel
		{
			get => DataContext as IBrowserPanelViewModel;
			set => DataContext = value;
		}

		[JsonIgnore]
		public int SelectedCount
			=> ViewModel.SelectedEntitiesCount;

		[JsonIgnore]
		public IEnumerable<IFSEntity> Selection
			=> ViewModel.Selection;

		#endregion

		#region Methods

		public void Dispose()
			=> ViewModel.Dispose();

		public void Activate()
			=> Focus();

		public void Deactivate()
		{
		}

		#endregion

		#region Event handlers

		public event EventHandler SelectedEntitiesCountChanged;
		public event EventHandler ContentGotFocus;
		public event EventHandler<EventArgs<Tuple<string, string>>> RequestedNewTitle;

		#endregion

		#region Events

		private void ViewModelOnSelectedEntitiesCountChanged(object sender, EventArgs eventArgs)
			=> OnSelectedEntitiesCountChanged();

		private void ViewModelOnRequestedNewTitle(object sender, EventArgs<Tuple<string, string>> eventArgs)
			=> OnRequestNewTitle(sender, eventArgs);

		private void OnSelectedEntitiesCountChanged()
			=> SelectedEntitiesCountChanged?.Invoke(this, EventArgs.Empty);

		private void OnGotFocus(object sender, RoutedEventArgs e)
			=> ContentGotFocus?.Invoke(sender, e);

		private void OnRequestNewTitle(object sender, EventArgs<Tuple<string, string>> e)
			=> RequestedNewTitle?.Invoke(sender, e);

		#endregion
	}

	/// <summary>
	/// Convets a panel to and from a JSON
	/// </summary>
	/// <seealso cref="Newtonsoft.Json.JsonConverter" />
	public class PanelViewConverter
		: JsonConverter
	{
		/// <inheritdoc cref="JsonConverter.WriteJson(JsonWriter, object, JsonSerializer)"/>
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			// TODO Generalize
			var panel = value as IBrowserPanelView;
			var viewModel = JToken.FromObject(panel.ViewModel);
			viewModel.WriteTo(writer);
		}

		/// <inheritdoc cref="JsonConverter.ReadJson(JsonReader, Type, object, JsonSerializer)"/>
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			return new object();
		}

		/// <inheritdoc cref="JsonConverter.CanConvert(Type)"/>
		public override bool CanConvert(Type objectType)
		{
			return true;
		}
	}
}