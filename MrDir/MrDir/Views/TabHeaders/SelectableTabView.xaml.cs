﻿using System.Windows.Controls;

namespace MrDir.Views.TabHeaders
{
  /// <summary>
  ///   Interaction logic for SelectableTabView.xaml
  /// </summary>
  public partial class SelectableTabView
    : UserControl
  {
    public SelectableTabView()
      => InitializeComponent();
  }
}