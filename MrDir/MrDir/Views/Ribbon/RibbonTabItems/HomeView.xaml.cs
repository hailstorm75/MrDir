﻿namespace MrDir.Views.Ribbon.RibbonTabItems
{
  public partial class HomeView
  {
    public HomeView(object dataContext)
    {
      InitializeComponent();
      DataContext = dataContext;
    }
  }
}