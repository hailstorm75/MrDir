﻿using System;
using System.Windows.Media;

namespace MrDir.Models
{
  public class Color
    : IEquatable<Color>, IComparable<Color>
  {
    public Color(object value, string name)
    {
      Value = new SolidColorBrush((System.Windows.Media.Color)value);
      Name = name; //Resources.ResourceManager.GetString(name);

      ColorToHSV(Value.Color, out m_hue, out m_saturation, out m_value);
    }

    #region Properties

    public SolidColorBrush Value { get; set; }
    public string Name { get; set; }

    #endregion

    #region Fields

    private readonly double m_hue;
    private readonly double m_saturation;
    private readonly double m_value;

    #endregion

    bool IEquatable<Color>.Equals(Color other)
      => Value.Color.Equals(other?.Value.Color);

    int IComparable<Color>.CompareTo(Color other)
    {
      if (ReferenceEquals(this, other)) return 0;
      if (ReferenceEquals(null, other)) return 1;

      var hueComp = m_hue.CompareTo(other.m_hue);
      if (hueComp != 0) return hueComp;

      var satComp = m_saturation.CompareTo(other.m_saturation);
      return satComp == 0
        ? m_value.CompareTo(other.m_value)
        : satComp;
    }

    private static void ColorToHSV(System.Windows.Media.Color color, out double hue, out double saturation, out double value)
    {
      int max = Math.Max(color.R, Math.Max(color.G, color.B));
      int min = Math.Min(color.R, Math.Min(color.G, color.B));

      hue = GetHue(color.R, color.G, color.B);
      saturation = max == 0
        ? 0
        : 1d - (1d * min / max);
      value = max / 255d;
    }

    private static double GetHue(int red, int green, int blue)
    {
      double min = Math.Min(Math.Min(red, green), blue);
      double max = Math.Max(Math.Max(red, green), blue);

      if (min == max)
        return 0;

      var hue = 0.0;
      if (max == red)
        hue = (green - blue) / (max - min);
      else if (max == green)
        hue = 2d + (blue - red) / (max - min);
      else
        hue = 4d + (red - green) / (max - min);

      hue = hue * 60;
      if (hue < 0) hue = hue + 360;

      return Math.Round(hue);
    }
  }
}