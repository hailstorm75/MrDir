﻿using System;
using System.Windows.Media;
using MrDir.Interfaces.Models;
using MrDir.Types.Notifiers;
using SchwabenCode.QuickIO;

namespace MrDir.Models.Bookmarks
{
	/// <summary>
	/// Base class for all Bookmarks
	/// </summary>
	[Serializable]
  public abstract class BaseBookmark
    : NotifyPropertyChanged, IBookmark
  {
    public string Label { get; protected set; }
    public string Path { get; protected set; }
    public abstract ImageSource Icon { get; }

    protected BaseBookmark(IFSEntity item)
    {
      Label = item.Name;
      Path = item.Path;
    }

    public virtual void Rename(string newName)
    {
      Label = newName;
      Path = System.IO.Path.Combine(new QuickIODirectoryInfo(Path).ParentFullName, newName);

      OnPropertyChanged(nameof(Label));
      OnPropertyChanged(nameof(Path));
    }
  }
}
