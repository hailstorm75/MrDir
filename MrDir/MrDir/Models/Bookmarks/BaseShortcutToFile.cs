﻿using System;
using System.Windows.Media;
using MrDir.Interfaces.Models;

namespace MrDir.Models.Bookmarks
{
  [Serializable]
  public class BookmarkToFile
    : BaseBookmark
  {
    [field:NonSerialized]
    private readonly ImageSource m_icon;
    public override ImageSource Icon => m_icon;

    public BookmarkToFile(IFile item)
      : base(item)
    {
      m_icon = item.Icon;
    }
  }
}
