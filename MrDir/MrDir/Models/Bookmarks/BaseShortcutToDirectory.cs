﻿using System;
using System.Windows.Media;
using MrDir.Helpers;
using MrDir.Managers;
using Directory = MrDir.Models.FileSystem.Directory;

namespace MrDir.Models.Bookmarks
{
  [Serializable]
  public class BookmarkToDirectory
    : BaseBookmark
  {
    [field:NonSerialized]
    private static readonly ImageSource m_icon = ConverterHelper.IconToImageSource(IconManager.FolderSmall);
    public override ImageSource Icon => m_icon;

    public BookmarkToDirectory(Directory directory)
      : base(directory)
    {
    }
  }
}
