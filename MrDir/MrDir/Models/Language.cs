﻿using System.Globalization;

namespace MrDir.Models
{
  public class Language
  {
    public CultureInfo Info { get; }

    public Language(CultureInfo cultureInfo)
    {
      Info = cultureInfo;
    }

    public override string ToString()
      => $"{char.ToUpper(Info.NativeName[0])}{Info.NativeName.Substring(1)} - {Info.EnglishName}";
  }
}