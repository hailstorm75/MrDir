﻿using MrDir.Interfaces.Views;
using MrDir.Views.PanelContainers;

namespace MrDir.Models
{
  public class TabContentModel
  {
    #region Properties

    public IPanelContainerView Content { get; }

    public string Guid { get; }

    #endregion

    public TabContentModel(string guid)
    {
      Guid = guid;
      Content = new SmartPanelContainerView(guid);
    }
  }
}
