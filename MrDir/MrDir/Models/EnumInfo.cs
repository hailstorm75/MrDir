﻿using System;

namespace MrDir.Models
{
	public class EnumInfo
	{
		public Enum Instance { get; }
		public string Name { get; }
		public string Description { get; }

		public EnumInfo(Enum instance, string name, string tooltip)
		{
			Instance = instance;
			Name = name;
			Description = tooltip;
		}
	}
}
