﻿using System;
using System.IO;
using System.Windows.Media;
using MrDir.Helpers;
using MrDir.Interfaces.Models;
using MrDir.Managers;
using MrDir.Types.Notifiers;

namespace MrDir.Models.FileSystem
{
  [Serializable]
  public class Drive
    : NotifyPropertyChanged, IDrive
  {
    public Drive(DriveInfo info)
    {
      Info = info;
      Path = Info.Name;
      Label = Info.VolumeLabel;
      Name = Path.Substring(0, Path.IndexOf("\\", StringComparison.InvariantCulture));
      Icon = ConverterHelper.IconToImageSource(IconManager.ExtractIconFromPath(Info.Name));
    }

    public ImageSource Icon { get; }
    public DriveInfo Info { get; }
    public string Path { get; }
    public string Label { get; }
    public string Name { get; }
  }
}