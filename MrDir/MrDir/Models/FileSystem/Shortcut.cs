﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MrDir.Cultures;
using MrDir.Interfaces.Models;
using MrDir.Types.Notifiers;
using SchwabenCode.QuickIO;
using Shell32;

namespace MrDir.Models.FileSystem
{
  public class Shortcut
    : NotifyPropertyChanged, IFSEntity
  {
    #region Fields

    private ImageSource m_icon;
    private string m_name;
    private string m_path;
    private string m_targetPath;
    private DateTime m_creationDate;
    private DateTime m_modifiedDate;
    private DateTime m_accessDate;
    private string m_access;

    #endregion

    #region Properties

    public ImageSource Icon
    {
      get => m_icon;
      protected set
      {
        m_icon = value;
        OnPropertyChanged(nameof(Icon));
      }
    }
    public string Name
    {
      get => m_name;
      protected set
      {
        m_name = value;
        OnPropertyChanged(nameof(Name));
      }
    }
    public string Path
    {
      get => m_path;
      protected set
      {
        m_path = value;
        OnPropertyChanged(nameof(Path));
      }
    }
    public string TargetPath
    {
      get => m_targetPath;
      protected set
      {
        m_targetPath = value;
        OnPropertyChanged(nameof(TargetPath));
      }
    }
    public string ToolTip => $"{Resources.Path}: {Path}\n{Resources.TargetPath}: {TargetPath}";
    public string Size => string.Empty;
    public DateTime CreationDate
    {
      get => m_creationDate;
      protected set
      {
        m_creationDate = value;
        OnPropertyChanged(nameof(CreationDate));
      }
    }
    public DateTime ModifiedDate
    {
      get => m_modifiedDate;
      protected set
      {
        m_modifiedDate = value;
        OnPropertyChanged(nameof(ModifiedDate));
      }
    }
    public DateTime AccessDate
    {
      get => m_accessDate;
      protected set
      {
        m_accessDate = value;
        OnPropertyChanged(nameof(AccessDate));
      }
    }
    public string Access
    {
      get => m_access;
      protected set
      {
        m_access = value;
        OnPropertyChanged(nameof(Access));
      }
    }

    #endregion

    public Shortcut(QuickIOFileSystemEntryBase entity)
    {
      Initialize(entity);
    }

    protected virtual void Initialize(QuickIOFileSystemEntryBase entity)
    {
      Name = entity.Name;
      Path = entity.FullName;
      TargetPath = GetShortcutTarget(Path);
      CreationDate = entity.CreationTime;
      ModifiedDate = entity.LastWriteTime;
      AccessDate = entity.LastAccessTime;
      Access = string.Empty;
      Icon = new BitmapImage();
    }

    /// <summary>
    /// Returns whether the given path/file is a link
    /// </summary>
    /// <param name="shortcutFilename"></param>
    /// <returns></returns>
    public static bool IsShortcut(string shortcutFilename)
    {
      var pathOnly = System.IO.Path.GetDirectoryName(shortcutFilename);
      var filenameOnly = System.IO.Path.GetFileName(shortcutFilename);

      var shell = new Shell();
      var folder = shell.NameSpace(pathOnly);
      var folderItem = folder.ParseName(filenameOnly);

      return folderItem != null && folderItem.IsLink;
    }

    /// <summary>
    /// If path/file is a link returns the full pathname of the target,
    /// Else return the original pathnameo "" if the file/path can't be found
    /// </summary>
    /// <param name="shortcutFilename"></param>
    /// <returns></returns>
    private static string GetShortcutTarget(string shortcutFilename)
    {
      var pathOnly = System.IO.Path.GetDirectoryName(shortcutFilename);
      var filenameOnly = System.IO.Path.GetFileName(shortcutFilename);

      var shell = new Shell();
      var folder = shell.NameSpace(pathOnly);
      var folderItem = folder.ParseName(filenameOnly);

      if (folderItem == null) return string.Empty;
      if (!folderItem.IsLink) return shortcutFilename;

      var link = (Shell32.ShellLinkObject)folderItem.GetLink;
      return link.Path;
    }

    public IFSEntity GetTarget()
    {
      var attributes = System.IO.File.GetAttributes(TargetPath);

      return ((attributes & FileAttributes.Directory) == FileAttributes.Directory)
        ? (QuickIOPath.IsLocalRegularPath(TargetPath)
          ? new Directory(TargetPath) as IFSEntity
          : QuickIOPath.IsShareRegularPath(TargetPath)
            ? new DirectoryUNC(TargetPath, true) as IFSEntity
            : null)
        : new File(TargetPath) as IFSEntity;
    }

    public void Dispose()
    {
      Name = null;
      Path = null;
      TargetPath = null;
      Access = null;
      Icon = null;
    }

		public async Task Refresh()
			=> Initialize(new QuickIOFileInfo(Path));
	}
}