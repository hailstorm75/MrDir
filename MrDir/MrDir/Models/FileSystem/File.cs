﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Media;
using MrDir.Cultures;
using MrDir.Helpers;
using MrDir.Interfaces.Models;
using MrDir.Types.Notifiers;
using SchwabenCode.QuickIO;

namespace MrDir.Models.FileSystem
{
  public class File
    : NotifyPropertyChanged, IFile
  {
    #region Fields

    private Icon m_icon;

    private readonly Lazy<ImageSource> m_lazyIcon;
    private string m_name;
    private string m_size;
    private string m_path;
    private DateTime m_creationDate;
    private DateTime m_modifiedDate;
    private DateTime m_accessDate;
    private string m_access;

    private static ImageSource GetIcon(Icon icon)
    {
      return ConverterHelper.IconToImageSource(icon);
    }

    #endregion

    #region Properties

    public string Name
    {
      get => m_name;
      private set
      {
        m_name = value;
        OnPropertyChanged(nameof(Name));
      }
    }

    public ImageSource Icon => m_lazyIcon.Value;

    public string Size
    {
      get => m_size;
      private set
      {
        m_size = value;
        OnPropertyChanged(nameof(Size));
      }
    }

    public string Path
    {
      get => m_path;
      private set
      {
        m_path = value;
        OnPropertyChanged(nameof(Path));
      }
    }

    public string Extension { get; protected set; }

    public DateTime CreationDate
    {
      get => m_creationDate;
      private set
      {
        m_creationDate = value;
        OnPropertyChanged(nameof(CreationDate));
      }
    }

    public DateTime ModifiedDate
    {
      get => m_modifiedDate;
      private set
      {
        m_modifiedDate = value;
        OnPropertyChanged(nameof(ModifiedDate));
      }
    }

    public DateTime AccessDate
    {
      get => m_accessDate;
      private set
      {
        m_accessDate = value;
        OnPropertyChanged(nameof(AccessDate));
      }
    }

    public string Access
    {
      get => m_access;
      private set
      {
        m_access = value;
        OnPropertyChanged(nameof(Access));
      }
    }

    public string ToolTip => $"{Resources.Path}: {Path}\n{Resources.Size}: {Size}";

    #endregion

    #region Constructors

    public File(string name)
    {
      m_lazyIcon = new Lazy<ImageSource>(() => GetIcon(m_icon));

      var fileInfo = new QuickIOFileInfo(name);
      Initialize(fileInfo);
    }

    public File(QuickIOFileInfo fileInfo)
    {
      m_lazyIcon = new Lazy<ImageSource>(() => GetIcon(m_icon));

      Initialize(fileInfo);
    }

    #endregion

    #region Methods

    private async Task Initialize(QuickIOFileInfo fileInfo)
    {
      await Task.Run(() =>
        {
          Name = fileInfo.Name;
          Path = fileInfo.FullName;
          Extension = System.IO.Path.GetExtension(Path);
          CreationDate = fileInfo.CreationTime;
          ModifiedDate = fileInfo.LastWriteTime;
          AccessDate = fileInfo.LastAccessTime;
          Size = ConverterHelper.BytesToString(fileInfo.Bytes);
          Access = string.Empty; //GetPermissions(fileInfo);
          m_icon = fileInfo.Exists && fileInfo.PathType != QuickIOPathType.UNC
            ? System.Drawing.Icon.ExtractAssociatedIcon(Path)
            : null;
        })
        .ConfigureAwait(false);
    }

    private static string GetPermissions(QuickIOFileInfo fileInfo)
    {
      char[] permissions =
      {
        '-',
        '-',
        '-',
        '-'
      };

      var rules = fileInfo.GetFileSystemSecurity();

      if (rules.CanModify)
        permissions[0] = 'm';
      if (rules.CanRead)
        permissions[1] = 'r';
      if (rules.CanWrite)
        permissions[2] = 'w';
      if (rules.CanExecuteFile)
        permissions[3] = 'e';

      return new string(permissions);
    }

    public void Dispose()
    {
      m_icon?.Dispose();
      m_name = null;
      m_path = null;
      m_size = null;
      m_access = null;
    }

		public async Task Refresh()
			=> await Initialize(new QuickIOFileInfo(Path)).ConfigureAwait(false);

		#endregion
	}
}