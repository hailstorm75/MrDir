﻿using System;
using System.Threading.Tasks;
using System.Windows.Media;
using MrDir.Cultures;
using MrDir.Helpers;
using MrDir.Interfaces.Models;
using MrDir.Managers;
using MrDir.Types.Notifiers;
using SchwabenCode.QuickIO;

namespace MrDir.Models.FileSystem
{
  public class Directory
    : NotifyPropertyChanged, IDirectory
  {
    #region Fields

    private static readonly ImageSource ICON = ConverterHelper.IconToImageSource(IconManager.FolderSmall);
    private string m_name;
    private string m_path;
    private DateTime m_creationDate;
    private DateTime m_modifiedDate;
    private DateTime m_accessDate;
    private string m_access;

    #endregion

    #region Properties

    public string Name
    {
      get => m_name;
      protected set
      {
        m_name = value;
        OnPropertyChanged(nameof(Name));
      }
    }

    public ImageSource Icon => ICON;

    public string Path
    {
      get => m_path;
      protected set
      {
        m_path = value;
        OnPropertyChanged(nameof(Path));
      }
    }

    public DateTime CreationDate
    {
      get => m_creationDate;
      protected set
      {
        m_creationDate = value;
        OnPropertyChanged(nameof(CreationDate));
      }
    }

    public DateTime ModifiedDate
    {
      get => m_modifiedDate;
      protected set
      {
        m_modifiedDate = value;
        OnPropertyChanged(nameof(ModifiedDate));
      }
    }

    public DateTime AccessDate
    {
      get => m_accessDate;
      protected set
      {
        m_accessDate = value;
        OnPropertyChanged(nameof(AccessDate));
      }
    }

    public string Access
    {
      get => m_access;
      protected set
      {
        m_access = value;
        OnPropertyChanged(nameof(Access));
      }
    }

    public string ToolTip => $"{Resources.Path}: {Path}";
    public string Size => string.Empty;

    #endregion

    #region Constructors

    public Directory(string name)
    {
      var directoryInfo = new QuickIODirectoryInfo(name);
      Initialize(directoryInfo);
    }

    protected Directory(string name, bool skipInit)
    {
      if (skipInit) return;
      var directoryInfo = new QuickIODirectoryInfo(name);
      Initialize(directoryInfo);
    }

    public Directory(QuickIODirectoryInfo directoryInfo)
    {
      Initialize(directoryInfo);
    }

    protected Directory(QuickIODirectoryInfo directoryInfo, bool skipInit)
    {
      if (!skipInit) Initialize(directoryInfo);
    }

    #endregion

    #region Methods

    protected virtual async Task Initialize(QuickIODirectoryInfo directoryInfo)
    {
      await Task.Run(() =>
			{
				Name = directoryInfo.Name;
				Path = directoryInfo.FullName;
				Access = string.Empty; //GetPermissions(directoryInfo);
				CreationDate = directoryInfo.CreationTime;
				ModifiedDate = directoryInfo.LastWriteTime;
				AccessDate = directoryInfo.LastAccessTime;
			})
			.ConfigureAwait(false);
    }

    protected static string GetPermissions(QuickIODirectoryInfo dirInfo)
    {
      char[] permissions =
      {
        'd',
        '-',
        '-',
        '-'
      };

      var rules = dirInfo.GetFileSystemSecurity();

      if (rules.CanRead)
        permissions[1] = 'r';
      if (rules.CanWrite)
        permissions[2] = 'w';
      if (rules.CanExecuteFile)
        permissions[3] = 'e';

      return new string(permissions);
    }

    public void Dispose()
    {
      m_name = null;
      m_path = null;
      m_access = null;
    }

		public async virtual Task Refresh()
			=> await Initialize(new QuickIODirectoryInfo(m_path)).ConfigureAwait(false);

		#endregion
	}
}