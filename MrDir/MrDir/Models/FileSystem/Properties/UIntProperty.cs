﻿using Microsoft.WindowsAPICodePack.Shell.PropertySystem;

namespace MrDir.Models.FileSystem.Properties
{
	public class UIntProperty
		: BaseProperty
	{
		private uint? m_value;

		public override object Value
		{
			get => m_value;
			set
			{
				if (value is uint valtype)
					m_value = valtype;
				else if (value?.GetType() == typeof(uint?))
					m_value = value as uint?;
			}
		}

		public UIntProperty(IShellProperty property)
			: base(property) { }
	}
}
