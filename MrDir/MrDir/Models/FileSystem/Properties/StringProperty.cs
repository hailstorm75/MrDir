﻿using Microsoft.WindowsAPICodePack.Shell.PropertySystem;

namespace MrDir.Models.FileSystem.Properties
{
  public class StringProperty
    : BaseProperty
  {
		private string m_value;

		public override object Value
		{
			get => m_value;
			set
			{
				m_value = value as string;
			}
		}

		public StringProperty(IShellProperty property)
			: base(property) { }
  }
}
