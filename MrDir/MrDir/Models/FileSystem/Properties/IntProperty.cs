﻿using Microsoft.WindowsAPICodePack.Shell.PropertySystem;

namespace MrDir.Models.FileSystem.Properties
{
	public class IntProperty
		: BaseProperty
	{
		private int? m_value;

		public override object Value
		{
			get => m_value ?? (int)m_value;
			set
			{
				if (value is int valtype)
					m_value = valtype;
				else if (value.GetType() == typeof(int?))
					m_value = value as int?;
			}
		}

		public IntProperty(IShellProperty property)
			: base(property) { }
	}
}
