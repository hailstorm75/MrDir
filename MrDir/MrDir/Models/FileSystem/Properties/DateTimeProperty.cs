﻿using Microsoft.WindowsAPICodePack.Shell.PropertySystem;

namespace MrDir.Models.FileSystem.Properties
{
	public class DateTimeProperty
    : BaseProperty
  {
    public override object Value { get; set; }

		public DateTimeProperty(IShellProperty property)
			: base(property) { }
  }
}
