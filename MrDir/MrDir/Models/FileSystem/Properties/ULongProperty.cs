﻿using Microsoft.WindowsAPICodePack.Shell.PropertySystem;

namespace MrDir.Models.FileSystem.Properties
{
	public class ULongProperty
		: BaseProperty
	{
		private ulong? m_value;

		public override object Value
		{
			get => m_value ?? (ulong)m_value;
			set
			{
				if (value is ulong valtype)
					m_value = valtype;
				else if (value.GetType() == typeof(ulong?))
					m_value = value as ulong?;
			}
		}

		public ULongProperty(IShellProperty property)
			: base(property) { }
	}
}
