﻿using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using MrDir.Helpers;
using MrDir.Interfaces.Models;

namespace MrDir.Models.FileSystem.Properties
{
	public abstract class BaseProperty
		: IProperty
	{
		protected readonly IShellProperty m_propertySource;

		public abstract object Value { get; set; }

    public string Name => m_propertySource.Description.DisplayName;

		public string Grouping { get; private set; }

		public bool IsReadOnly { get; private set; }

		public BaseProperty(IShellProperty property)
		{
			m_propertySource = property;
			Value = m_propertySource.ValueAsObject;

			var (isReadOnly, grouping) = property.GetDefinition();
			IsReadOnly = isReadOnly;
			Grouping = grouping;
		}
	}
}
