﻿using System.Windows.Media;
using MrDir.Helpers;
using MrDir.Interfaces.Models;
using MrDir.Managers;

namespace MrDir.Models.FileSystem
{
  public class NetworkDrive
    : IDrive
  {
    public NetworkDrive(string serverName)
    {
      Icon = ConverterHelper.IconToImageSource(IconManager.FolderSmall);
      Path = serverName;
      Label = string.Empty;
      Name = serverName.Replace("\\", "");
    }

    public ImageSource Icon { get; }

    public string Path { get; }

    public string Label { get; }

    public string Name { get; }
  }
}