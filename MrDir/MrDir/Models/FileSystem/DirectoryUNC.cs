﻿using System.Threading.Tasks;
using SchwabenCode.QuickIO;

namespace MrDir.Models.FileSystem
{
  public class DirectoryUNC
    : Directory
  {
    private bool m_isRoot;

    public DirectoryUNC(string name, bool isRoot)
      : base(name, true)
    {
      m_isRoot = isRoot;
      var directoryInfo = new QuickIODirectoryInfo(name);
      Initialize(directoryInfo);
    }

    public DirectoryUNC(QuickIODirectoryInfo directoryInfo, bool isRoot)
      : base(directoryInfo, true)
    {
      m_isRoot = isRoot;
      Initialize(directoryInfo);
    }

    protected override async Task Initialize(QuickIODirectoryInfo directoryInfo)
    {
      await Task.Run(() =>
        {
          Name = directoryInfo.Name;
          Path = directoryInfo.FullName;
          Access = string.Empty; //GetPermissions(directoryInfo);

          if (m_isRoot)
            return;

          CreationDate = directoryInfo.CreationTime;
          ModifiedDate = directoryInfo.LastWriteTime;
          AccessDate = directoryInfo.LastAccessTime;
        })
        .ConfigureAwait(false);
    }

		public async override Task Refresh()
			=> await Initialize(new QuickIODirectoryInfo(Path)).ConfigureAwait(false);
  }
}