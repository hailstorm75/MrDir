﻿using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Media;

namespace MrDir.Models
{
  public class ContextMenuItem
  {
    #region Properties

    public string Name { get; set; }
    public string ToolTip { get; set; }
    public ImageSource Icon { get; set; }
    public ICommand Command { get; set; }
    public List<ContextMenuItem> SubMenus { get; set; }

    #endregion
  }
}