﻿using MrDir.Enums;
using MrDir.Interfaces.Models;

namespace MrDir.Models
{
  public class Notification
    : INotification
  {
    public Notification(NotificationType type, string message, string details)
    {
      Type = type;
      Message = message;
      Details = details;
    }

    #region Properties

    public NotificationType Type { get; }
    public string Message { get; }
    public string Details { get; }

    #endregion
  }
}