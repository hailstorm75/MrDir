﻿using MaterialDesignExtensions.Model;
using MrDir.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace MrDir.Models
{
  public class TextBoxSuggestionsCollection
    : TextBoxSuggestionsSource, INotifyPropertyChanged
  {
    public List<string> Suggestions { get; } = new List<string>();

    public TextBoxSuggestionsCollection(string initialPath, Func<string, Task<List<string>>> suggestionsProvider)
    {
      //SuggestionsProvider = suggestionsProvider;
      //LoadSuggestions(initialPath);
    }

    public void UpdateSuggestions(string path)
      => LoadSuggestions(path);

    public Func<string, Task<List<string>>> SuggestionsProvider { get; }

    private async void LoadSuggestions(string path)
    {
      //m_suggestions = await SuggestionsProvider(path).ConfigureAwait(false);
      OnPropertyChanged(nameof(Suggestions));
    }

    public override IEnumerable<string> Search(string searchTerm)
    {
      searchTerm = searchTerm ?? string.Empty;
      searchTerm = searchTerm.ToLower();

      var result = Suggestions.Where(item => item.ToLower().Contains(searchTerm)).ToList();
      if (result.Count == 1
          && result[0].ToLower().Equals(searchTerm))
      {
        LoadSuggestions(result[0]);
        var newSearch = Search(searchTerm);
      }

      return result;
    }

    #region INotifyPropertyChanged

    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion
  }
}