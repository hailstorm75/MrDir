﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using MrDir.Enums;
using MrDir.Interfaces.Models;
using MrDir.Managers;
using MrDir.Types;
using MrDir.Types.Notifiers;

namespace MrDir.Models.Processes
{
	/// <summary>
	/// Base class for all Processes
	/// </summary>
  public abstract class BaseProcess
    : NotifyPropertyChanged, IProcess
  {
    protected BaseProcess(IReadOnlyCollection<object> objects)
    {
      CreationTime = DateTime.Now;
      m_objects = objects;
      m_cancellationTokenSource = new CancellationTokenSource();
    }

    public ICommand CancelCommand
      => new RelayCommand(Cancel);

    #region Fields

    private double m_progress;
    protected readonly IReadOnlyCollection<object> m_objects;
    protected bool m_isComplete = false;
    protected readonly CancellationTokenSource m_cancellationTokenSource;

    #endregion

    #region Properties

    public double Progress
    {
      get => m_progress;
      set
      {
        m_progress = value;
        OnPropertyChanged(nameof(Progress));
      }
    }

    public abstract string Name { get; }
    public bool IsComplete => m_isComplete;
    public DateTime CreationTime { get; }

    #endregion

    #region Methods

    public void Cancel()
      => m_cancellationTokenSource.Cancel();

    public virtual async Task<ProcessResult> Run()
    {
      return await Task.Run(async () =>
      {
        ProcessResult result;
        var successfullCount = 0;

        var currentCount = 0;
        foreach (var item in m_objects)
        {
          if (m_cancellationTokenSource.IsCancellationRequested)
          {
            result = ProcessResult.Cancelled;
            break;
          }

          try
          {
            await ProcessItem(item);
            ++successfullCount;
          }
          catch (Exception e) { LogError(item, e); }
          finally { Progress = (currentCount++ + 1) / (double)m_objects.Count * 100; }
        }

        LogSummary(successfullCount, out result);

        m_isComplete = true;
        OnPropertyChanged(nameof(IsComplete));

        ProcessManager.Instance.ProcessFinished(this);

        return result;
      }, m_cancellationTokenSource.Token).ConfigureAwait(false);
    }

    protected abstract Task ProcessItem(object item);

    protected virtual void LogError(object item, Exception e)
      => NotificationManager.Instance.Notify(new Notification(NotificationType.Error, $"Error processing '{item}'.", e.ToString()));

    protected virtual void LogSummary(int successfullCount, out ProcessResult result)
    {
      NotificationManager.Instance.Notify(new Notification(NotificationType.Information,
        "Process finished.",
        $"Processed {successfullCount}/{m_objects.Count} items. Failed items count: {m_objects.Count - successfullCount}."));

      SetResult(successfullCount, out result);
    }

    protected void SetResult(int successfullCount, out ProcessResult result)
    {
      result = successfullCount == m_objects.Count
        ? ProcessResult.Finished
        : ProcessResult.FinishedWithErrors;
    }

    #endregion
  }
}