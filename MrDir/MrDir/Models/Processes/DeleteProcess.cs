﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MrDir.Cultures;
using MrDir.Enums;
using MrDir.Interfaces.Models;
using MrDir.Managers;
using SchwabenCode.QuickIO;

namespace MrDir.Models.Processes
{
  public class DeleteProcess
    : BaseProcess
  {
    public DeleteProcess(IReadOnlyCollection<object> objects)
      : base(objects) { }

    public override string Name
      => string.Format(Resources.DeletingItemsProgress, m_objects.Count);

    protected override async Task ProcessItem(object item)
    {
      switch (item)
      {
        case IFile doc:
          await QuickIOFile.DeleteAsync(doc.Path).ConfigureAwait(false);
          break;
        case IDirectory dir:
          await QuickIODirectory.DeleteAsync(dir.Path, true).ConfigureAwait(false);
          break;
      }
    }

    protected override void LogError(object item, Exception e)
      => NotificationManager.Instance.Notify(new Notification(NotificationType.Error,
        $"Error deleting '{(item as IFSEntity)?.Path}'.", e.ToString()));

    protected override void LogSummary(int successfullCount, out ProcessResult result)
    {
      NotificationManager.Instance.Notify(new Notification(NotificationType.Information,
        "Delete operation finished.",
        $"Deleted {successfullCount}/{m_objects.Count} items. Failed items count: {m_objects.Count - successfullCount}."));

      SetResult(successfullCount, out result);
    }
  }
}