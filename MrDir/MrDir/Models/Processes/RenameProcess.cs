﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MrDir.Cultures;
using MrDir.Enums;
using MrDir.Interfaces.Models;
using MrDir.Managers;
using SchwabenCode.QuickIO;

namespace MrDir.Models.Processes
{
  public class RenameProcess
    : BaseProcess
  {
    public RenameProcess(IReadOnlyCollection<object> objects)
      : base(objects) { }

    public override string Name
      => string.Format(Resources.Rename, m_objects.Count);

    protected override async Task ProcessItem(object item)
    {
      var entity = (Tuple<IFSEntity, string>)item;
      switch (entity.Item1)
      {
        case IDirectory directory:
          var pathToDir = Path.GetDirectoryName(directory.Path);
          if (pathToDir == null)
            // TODO Log error
            throw new Exception();

          var newDirName = Path.Combine(pathToDir, entity.Item2);
          if (await QuickIODirectory.ExistsAsync(newDirName).ConfigureAwait(false))
            throw new Exception($"Cannot rename directory '{directory.Path}' to '{newDirName}' because it already exists.");

          await QuickIODirectory.MoveAsync(directory.Path, newDirName).ConfigureAwait(false);

          break;
        case IFile document:
          var pathToFile = Path.GetDirectoryName(entity.Item1.Path);
          if (pathToFile == null)
            // TODO Log error
            throw new Exception();

          var newFileName = Path.Combine(pathToFile, entity.Item2);
          if (await QuickIOFile.ExistsAsync(newFileName).ConfigureAwait(false))
            throw new Exception($"Cannot rename file '{document.Path}' to '{newFileName}' because it already exists.");

          await QuickIOFile.MoveAsync(document.Path, newFileName).ConfigureAwait(false);
          break;
      }
    }

    protected override void LogError(object item, Exception e)
    {
      var entity = (Tuple<IFSEntity, string>)item;
      NotificationManager.Instance.Notify(new Notification(NotificationType.Error,
        $"Error renaming '{entity.Item1?.Path}'.", e.ToString()));
    }

    protected override void LogSummary(int successfullCount, out ProcessResult result)
    {
      NotificationManager.Instance.Notify(new Notification(NotificationType.Information,
        "Renaming operation finished.",
        $"Renamed {successfullCount}/{m_objects.Count} items. Failed items count: {m_objects.Count - successfullCount}."));

      SetResult(successfullCount, out result);
    }
  }
}