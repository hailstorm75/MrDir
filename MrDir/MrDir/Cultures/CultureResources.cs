﻿using System.Globalization;
using System.Windows.Data;

namespace MrDir.Cultures
{
  /// <summary>
  /// Wraps up XAML access to instance of WPFLocalize.Properties.Resources, list of available cultures, and method to change culture
  /// </summary>
  public class CultureResources
  {
    /// <summary>
    /// The Resources ObjectDataProvider uses this method to get an instance of the WPFLocalize.Properties.Resources class
    /// </summary>
    /// <returns></returns>
    public Resources GetResourceInstance()
      => new Resources();

    private static ObjectDataProvider m_provider;

    public static ObjectDataProvider ResourceProvider
      => m_provider ?? (m_provider = (ObjectDataProvider) System.Windows.Application.Current.FindResource("Resources"));

    /// <summary>
    /// Change the current culture used in the application.
    /// If the desired culture is available all localized elements are updated.
    /// </summary>
    /// <param name="culture">Culture to change to</param>
    public static void ChangeCulture(CultureInfo culture)
    {
      Resources.Culture = culture;
      ResourceProvider.Refresh();
    }
  }
}