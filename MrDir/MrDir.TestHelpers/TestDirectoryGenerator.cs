﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MrDir.TestHelpers
{
  public class TestDirectoryGenerator
  {
    private const string TEST_FOLDER = "MrDir_Tests";

    public static string PathToTestRoot { get; set; }

    static TestDirectoryGenerator()
    {
      PathToTestRoot = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + TEST_FOLDER;
      CreateIfDoesntExist(PathToTestRoot);
    }

    public static void ClearRoot()
    {
      var directories = Directory.EnumerateDirectories(PathToTestRoot);
      foreach (var directory in directories)
        Directory.Delete(directory, true);

      var files = Directory.EnumerateFiles(PathToTestRoot);
      foreach (var file in files)
        File.Delete(file);
    }

    private static void CreateIfDoesntExist(string path)
    {
      if (!Directory.Exists(path))
        Directory.CreateDirectory(path);
    }
  }
}
