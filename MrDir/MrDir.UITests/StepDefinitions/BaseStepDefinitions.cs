﻿using System.Diagnostics;
using System.IO;
using TechTalk.SpecFlow;
using TestStack.White;

namespace MrDir.UITests.StepDefinitions
{
  public class BaseStepDefinitions
  {
    private Application m_application;
    private static readonly string PATH_TO_APPLICATION = Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\MrDir.exe");

    private void NewApp()
    {
      m_application = Application.AttachOrLaunch(new ProcessStartInfo(PATH_TO_APPLICATION));
      Windows.Init(m_application);
    }

    private void CloseApp()
    {
      m_application?.Close();
      m_application?.Dispose();
    }

    [BeforeScenario]
    public void BeforeScenario()
      => NewApp();

    [AfterScenario]
    public void AfterScenario()
      => CloseApp();
  }
}
