﻿using TechTalk.SpecFlow;

namespace MrDir.UITests.StepDefinitions
{
  [Binding]
  public class TabStepDefinitions
    : BaseStepDefinitions
  {
    [Given(@"I select all tabs")]
		public void GivenISelectAllTabs()
			=> Windows.Main.Ribbon.SelectAllPanels();

		[Given(@"I have '(.*)' open tab\(s\)")]
		public void GivenIHaveOpenTabS(int count)
		{
			for (var i = 0; i < count; i++)
				Windows.Main.AddNewTab();
		}
  }
}
