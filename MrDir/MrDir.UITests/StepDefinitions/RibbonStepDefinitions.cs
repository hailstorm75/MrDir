﻿using System.Threading;
using MrDir.UITests.Workflows;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Xunit;

namespace MrDir.UITests.StepDefinitions
{
  [Binding]
  public sealed class RibbonStepDefinitions
    : BaseStepDefinitions
  {
    [When(@"I press rename")]
    public void WhenIPressRename()
      => Windows.Main.Ribbon.ExecuteRename();

		[When(@"I press copy")]
		public void WhenIPressCopy()
			=> Windows.Main.Ribbon.ExecuteCopy();

		[Given(@"I enable the multipanel mode")]
		public void GivenIEnableTheMultipanelMode()
			=> Windows.Main.Ribbon.ToggleMultipanel();

    [Then(@"the rename dialog appears")]
    public void ThenTheRenameDialogAppears()
      => Assert.True(Windows.Rename.Exists());

		[Then(@"the resolve conflicts dialog appears")]
		public void ThenTheResolveConflictsDialogAppearsW()
			=> Assert.True(Windows.ResolveConflicts.Exists());

		[Then(@"the resolve conflicts dialog will not appear")]
		public void ThenTheResolveConflictsWillNotAppear()
		{
			Thread.Sleep(1000);

			Assert.False(Windows.ResolveConflicts.Exists(), "The dialog must not open.");
		}

    [When(@"I rename everything to the following:")]
    public void WhenIRenameEverythingToTheFollowing(Table table)
    {
      var (main, additional, skipFirst, before, after, order)
        = table.CreateInstance<(string main,
                                string additional,
                                bool skipFirst,
                                string before,
                                string after,
                                string[] order)>();

      var data = new RenameData
      {
        Main = main,
        Additional = additional,
        SkipFirst = skipFirst,
        BeforeSequence = before,
        AfterSequence = after,
        CardOrder = order
      };

      Windows.Main.FileBrowser.SelectAll();

      FileRenamer.Rename(FileRenamer.Mode.Multiple, data);
    }
	}
}
