﻿using System.Linq;
using TechTalk.SpecFlow;
using Xunit;

namespace MrDir.UITests.StepDefinitions
{
  [Binding]
  public class FileBrowserStepDefinitions
    : BaseStepDefinitions
  {
    [Then(@"its content is displayed in the browser list")]
    public void ThenItsContentIsDisplayInTheBrowserList()
      => Assert.True(Windows.Main.FileBrowser.BrowserItemNames.Any());

    [Given(@"I navigate to the '(.*)' directory")]
    public void GivenINavigateToTheDirectory(string path)
      => Windows.Main.FileBrowser.NavigateTo(path);

    [Given(@"I navigate to the '(.*)' directory in tab '(.*)'")]
    public void GivenINavigateToTheDirectory(string path, int tabNum)
    {
      Windows.Main.SelectTab(tabNum);
      Windows.Main.FileBrowser.NavigateTo(path);
    }

    [Given(@"I am browsing the '(.*)' directory")]
    public void GivenIAmBrowsingTheDirectory(string path)
    {
      Windows.Main.AddNewTab();
      Windows.Main.FileBrowser.NavigateTo(path);
    }

    [Given(@"I select everything in the browser")]
    public void GivenISelectEverythingInTheBrowser()
      => Windows.Main.FileBrowser.SelectAll();
  }
}