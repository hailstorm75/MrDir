﻿using TestStack.White.UIItems;

namespace MrDir.UITests.WindowObjects.Controls
{
  public class RibbonObject
    : BaseObject
  {
    #region Properties

    public CheckBox MultipanelEnable => GetCheckBox(nameof(MultipanelEnable));
    public Button MultipanelSelectAll => GetButton(nameof(MultipanelSelectAll));
    public Button MultipanelSelectNone => GetButton(nameof(MultipanelSelectNone));

    public Button OrganizeDelete => GetButton(nameof(OrganizeDelete));
    public Button OrganizeRename => GetButton(nameof(OrganizeRename));

    public Button ClipboardCopy => GetButton(nameof(ClipboardCopy));
    public Button ClipboardPaste => GetButton(nameof(ClipboardPaste));
    public Button ClipboardCut => GetButton(nameof(ClipboardCut));

    #endregion

    public RibbonObject(IUIItem container)
      : base(container) { }

    #region Multipanel

    public RibbonObject ToggleMultipanel()
    {
      MultipanelEnable.Click();
      return this;
    }

    public RibbonObject SelectAllPanels()
    {
      MultipanelSelectAll.Click();
      return this;
    }

    public RibbonObject SelectNonePanels()
    {
      MultipanelSelectNone.Click();
      return this;
    }

    #endregion

    #region Organizer

    public RibbonObject ExecuteDelete()
    {
      OrganizeDelete.Click();
      return this;
    }

    public RibbonObject ExecuteRename()
    {
      OrganizeRename.Click();
      return this;
    }

    #endregion

    #region Clipboard

    public RibbonObject ExecuteCopy()
    {
      ClipboardCopy.Click();
      return this;
    }

    public RibbonObject ExecutePaste()
    {
      ClipboardPaste.Click();
      return this;
    }

    public RibbonObject ExecuteCut()
    {
      ClipboardCut.Click();
      return this;
    }

    #endregion
  }
}
