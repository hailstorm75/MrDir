﻿using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace MrDir.UITests.WindowObjects.Dialogs
{
  public class SettingsDialog
    : BaseWindow
  {
    public UIItem Languages => GetUIItem("Languages");
    public Button AppMode => GetButton("AppMode");

    public SettingsDialog(Window container)
      : base(container)
    {
    }

		public SettingsDialog ToggleAppMode()
		{
			AppMode.Click();
			return this;
		}
	}
}
