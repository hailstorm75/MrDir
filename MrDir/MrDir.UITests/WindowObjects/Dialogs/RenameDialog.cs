﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Automation;
using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.WindowsAPI;

namespace MrDir.UITests.WindowObjects.Dialogs
{
  public class RenameDialog
    : BaseWindow
  {
    #region Properties

    private ListView PreviewTable => GetListView(nameof(PreviewTable));

    private Button PreviewButton => GetButton(nameof(PreviewButton));
    private Button RenameButton => GetButton(nameof(RenameButton));

    private TextBox MainTextInput => GetTextBox(nameof(MainTextInput));
    private TextBox AdditionalTextInput => GetTextBox(nameof(AdditionalTextInput));
    private TextBox BeforeSequenceTextInput => GetTextBox(nameof(BeforeSequenceTextInput));
    private TextBox AfterSequenceTextInput => GetTextBox(nameof(AfterSequenceTextInput));
    private Button SkipFirstToggle => GetButton(nameof(SkipFirstToggle));

    private List<Thumb> Cards { get; set; }
    // TODO
    public bool IsMultiple { get; set; } = true;

    #endregion

    #region Enums

    private enum Position
    {
      Top,
      Middle,
      Bottom
    }

    public enum Card
    {
      Main,
      Additional,
      Sequence
    }

    public static string CardToString(Card card)
    {
      switch (card)
      {
        case Card.Main:
          return nameof(Card.Main);
        case Card.Additional:
          return nameof(Card.Additional);
        case Card.Sequence:
          return nameof(Card.Sequence);
        default:
          return string.Empty;
      }
    }

    public static Card StringToCard(string card)
    {
      switch (card)
      {
        case nameof(Card.Main):
          return Card.Main;
        case nameof(Card.Additional):
          return Card.Additional;
        case nameof(Card.Sequence):
          return Card.Sequence;
        default:
          throw new ArgumentException($"No card called '{card}' exists. Please use one of the following: {nameof(Card.Main)}, {nameof(Card.Additional)}, {nameof(Card.Sequence)}");
      }
    }

    #endregion

    private static readonly Dictionary<Card, string> CARD_PAIRS = new Dictionary<Card, string>
    {
      {Card.Main, "CardMainText"},
      {Card.Additional, "CardAdditionalText"},
      {Card.Sequence, "CardSequence"}
    };

    public RenameDialog(UIItemContainer container)
      : base(container)
    {
      Cards = container.GetMultiple(SearchCriteria.ByControlType(typeof(Thumb), WindowsFramework.Wpf))
                       .Where(x => x.PrimaryIdentification.StartsWith("Card"))
                       .Cast<Thumb>()
                       .OrderBy(x => x.Location.Y).ToList();
    }

    #region Methods

    public RenameDialog SetMainText(string text)
    {
      MainTextInput.SetValue(text);
      Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);

      return this;
    }

    public RenameDialog SetAdditionalText(string text)
    {
      AdditionalTextInput.SetValue(text);
      Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);

      return this;
    }

    public RenameDialog SetSequence(string before = "", string after = "", bool? skipFirst = null)
    {
      if (!string.IsNullOrEmpty(before))
        BeforeSequenceTextInput.SetValue(before);
      if (!string.IsNullOrEmpty(after))
        AfterSequenceTextInput.SetValue(after);
      if (skipFirst != null)
        SkipFirstToggle.State = (bool)skipFirst
          ? ToggleState.On
          : ToggleState.Off;

      return this;
    }

    public RenameDialog Preview()
    {
      PreviewButton.Click();
      return this;
    }

    public RenameDialog Rename()
    {
      RenameButton.Click();
      return this;
    }

    public List<Tuple<string, string>> GetPreviewResult()
      => PreviewTable.Rows.Select(x => new Tuple<string, string>(x.Cells.First().Text, x.Cells.Last().Text))
                          .ToList();

    public RenameDialog SetCardOrder(Card first, Card second, Card third)
    {
      var checkUnique = new HashSet<RenameDialog.Card> { first, second, third };
      if (checkUnique.Count != 3)
        throw new ArgumentException("All provided card types must be unique");

      MoveCard(Cards.First(x => x.PrimaryIdentification.Equals(CARD_PAIRS[first])), Position.Top);
      MoveCard(Cards.First(x => x.PrimaryIdentification.Equals(CARD_PAIRS[second])), Position.Middle);
      MoveCard(Cards.First(x => x.PrimaryIdentification.Equals(CARD_PAIRS[third])), Position.Bottom);

      return this;
    }

    private void MoveCard(Thumb card, Position position)
    {
      var index = Cards.IndexOf(card);
      switch (position)
      {
        case Position.Top:
          if (index != 0)
            DragCardUp(card);
          break;
        case Position.Middle:
          if (index == 2)
          {
            DragCardUp(card);
            DragCardUp(Cards[1]);
          }
          else if (index == 0)
            DragCardUp(Cards[1]);
          break;
        case Position.Bottom:
          if (index != 2)
          {
            DragCardUp(Cards[2]);
            DragCardUp(Cards[2]);
          }
          break;
      }
    }

    private void DragCardUp(IUIItem card)
    {
      card.Click();
      Mouse.LeftDown();
      const int stepCount = 10;
      var stepAmount = (float)(card.ClickablePoint.Y - Cards[0].ClickablePoint.Y + 10) / stepCount;
      for (var i = 0; i < stepCount; i++)
      {
        Mouse.Instance.Location = new System.Windows.Point(Mouse.Instance.Location.X, Mouse.Instance.Location.Y - stepAmount);
        Thread.Sleep(75);
      }

      Mouse.LeftUp();

      Thread.Sleep(100);

      Cards = Cards.OrderBy(x => x.Location.Y).ToList();
    }

    #endregion
  }
}
