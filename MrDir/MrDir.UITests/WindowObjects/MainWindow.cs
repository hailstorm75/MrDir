﻿using MrDir.UITests.WindowObjects.Controls;
using MrDir.UITests.WindowObjects.Dialogs;
using MrDir.UITests.WindowObjects.Panels;
using System;
using System.Linq;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Custom;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WPFUIItems;

namespace MrDir.UITests.WindowObjects
{
	public class MainWindow
		: BaseWindow
	{
		#region Properties

		protected Button AddTab => GetButton("DefaultAddButton");
		protected Button CloseTab => GetButton("PART_CloseButton");
		protected Button Settings => GetButton("Settings");
		protected Button Notifications => GetButton("Notifications");
		protected Button Processes => GetButton("Processes");
		public RibbonObject Ribbon => new RibbonObject(m_container);
		public BasicFileBrowserPanel FileBrowser => new BasicFileBrowserPanel((m_container as UIItemContainer)
			.GetMultiple(SearchCriteria.ByAutomationId("BasicFileBrowser")
																 .AndControlType(typeof(UIItem), WindowsFramework.Wpf))
			.FirstOrDefault(x => !x.IsOffScreen));

		#endregion

		public MainWindow(Window container)
			: base(container) { }

		#region Methods

		public MainWindow OpenSettings()
		{
			Settings.Click();
			return this;
		}

		public MainWindow OpenNotifications()
		{
			Notifications.Click();
			return this;
		}

		public MainWindow OpenProcesses()
		{
			Processes.Click();
			return this;
		}

		public MainWindow AddNewTab()
		{
			AddTab.Click();
			return this;
		}

		public MainWindow CloseATab()
		{
			CloseTab.Click();
			return this;
		}

		public MainWindow SelectTab(int tabIndex)
		{
			if (tabIndex < 1)
				throw new ArgumentException("Tab index cannot be less than 1");

			var tabs = (m_container as Window)?.GetMultiple(SearchCriteria.ByAutomationId("TabHeader"))
				.OrderBy(x => x.Location.X)
				.ThenBy(x => x.Location.Y)
				.ToList();

			if (tabIndex > tabs.Count)
				throw new ArgumentException("Either provided tab number is invalid or tab creation failed.");

			tabs[tabIndex - 1]
				.Get<Label>("Header")
				.Click();

			return this;
		}

		public bool HasATab()
			=> HasUiItem<IUIItem>("TabHeader");

		#endregion
	}
}
