﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.WindowsAPI;

namespace MrDir.UITests.WindowObjects.Panels
{
	public class BasicFileBrowserPanel
		: BaseObject
	{
		public ListView FileBrowserTable => GetListView(nameof(FileBrowserTable));
		public TextBox AddressBar => GetTextBox(nameof(AddressBar));
		public Button GoToRoot => GetButton(nameof(GoToRoot));
		public Button GoBack => GetButton(nameof(GoBack));
		public Button GoUp => GetButton(nameof(GoUp));
		public ComboBox DriveList => GetComboBox(nameof(DriveList));
    public IEnumerable<string> BrowserItemNames
      => FileBrowserTable.Rows.Select(x => x.Cells.First().Text).OrderBy(x => x);

		public BasicFileBrowserPanel(IUIItem item)
			: base(item) { }

		public BasicFileBrowserPanel SelectRow(int row)
		{
			FileBrowserTable.Rows[row].Select();
			return this;
		}

		public BasicFileBrowserPanel SelectAll()
		{
			if (FileBrowserTable.ScrollBars.CanScroll)
				FileBrowserTable.ScrollBars.Vertical.SetToMinimum();

			SelectRow(0);
			Keyboard.Instance.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
			Keyboard.Instance.Send("A", FileBrowserTable);
			Keyboard.Instance.LeaveAllKeys();

			var timeout = DateTime.Now;
			while (true)
			{
				if (FileBrowserTable.SelectedRows.Count == FileBrowserTable.Rows.Count)
					break;
				if (timeout - DateTime.Now > TimeSpan.FromSeconds(10))
					throw new TimeoutException(nameof(SelectAll));
				Thread.Sleep(100);
			}

			return this;
		}

		public BasicFileBrowserPanel NavigateTo(string path)
		{
			AddressBar.SetValue(path);
			Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);
			return this;
		}

		public BasicFileBrowserPanel NavigateToRoot()
		{
			GoToRoot.Click();
			return this;
		}

		public BasicFileBrowserPanel NavigateUp()
		{
			GoUp.Click();
			return this;
		}

		public BasicFileBrowserPanel NavigateBack()
		{
			GoBack.Click();
			return this;
		}
	}
}
