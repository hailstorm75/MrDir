﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.UIItems;

namespace MrDir.UITests.WindowObjects.Dialogs
{
 public class BaseWindow
    : BaseObject
  {
    public Button Close => GetButton("PART_Close");

    public void CloseWindow()
      => Close.Click();

    public BaseWindow(UIItemContainer container)
      : base(container) { }
  }
}
