﻿using System;
using System.Diagnostics;
using System.Windows.Automation;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Custom;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.TableItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WPFUIItems;
using TestStack.White.Utility;

namespace MrDir.UITests.WindowObjects
{
  public class BaseObject
  {
    #region Properties

    protected Table GetTable(string title) => TryGetUiItem<Table>(title);
    protected Button GetButton(string title) => TryGetUiItem<Button>(title);
    protected CheckBox GetCheckBox(string title) => TryGetUiItem<CheckBox>(title);
    protected TextBox GetTextBox(string title) => TryGetUiItem<TextBox>(title);
    protected ListView GetListView(string title) => TryGetUiItem<ListView>(title);
    protected ListBox GetListBox(string title) => TryGetUiItem<ListBox>(title);
		protected ComboBox GetComboBox(string title) => TryGetUiItem<ComboBox>(title);
    protected CustomUIItem GetCustomUIItem(string title) => TryGetUiItem<CustomUIItem>(title);
    protected UIItem GetUIItem(string title) => TryGetUiItem<UIItem>(title);

    #endregion

    protected readonly IUIItem m_container;

    private T TryGetUiItem<T>(string title) where T : IUIItem
    {
      return Retry.For(() =>
      {
        try
        {
          return (T)m_container.Get(SearchCriteria.ByAutomationId(title).AndControlType(typeof(T), WindowsFramework.Wpf));
        }
        catch (Exception e)
        {
          Debug.WriteLine(e);
          return default(T);
        }
      }, TimeSpan.FromSeconds(5));
    }

		public bool HasUiItem<T>(string title) where T : IUIItem
			=> !TryGetUiItem<T>(title).Equals(default(T));

		public bool Exists()
			=> m_container != null;

		protected BaseObject(IUIItem container)
      => m_container = container;
  }
}