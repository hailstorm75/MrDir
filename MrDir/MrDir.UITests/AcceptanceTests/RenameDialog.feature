﻿Feature: RenameDialog
	In order to rename files and directories
	As a user
	I want to use the rename dialog

@MultipleItems
Scenario: Rename all items in directory
	Given I am browsing the 'C:\' directory
	When I rename everything to the following:
  | Main | Additional | Skip first | Befor sequence | After sequence | Order                      |
  | Name | File       | false      | (              | )              | Additional, Sequence, Main |
	Then the rename dialog appears
