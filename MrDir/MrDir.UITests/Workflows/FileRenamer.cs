﻿using System;
using MrDir.UITests.WindowObjects.Dialogs;
using TechTalk.SpecFlow.Assist.Attributes;

namespace MrDir.UITests.Workflows
{
  public static class FileRenamer
  {
    public enum Mode
    {
      Single,
      Multiple
    }

    public static void Rename(Mode mode, RenameData data)
    {
      Windows.Main.Ribbon.ExecuteRename();
      var renameDialog = Windows.Rename;
      if (renameDialog.IsMultiple && mode == Mode.Single || !renameDialog.IsMultiple && mode == Mode.Multiple)
        throw new Exception("Wrong rename dialog variant opened");

      RenameMultiple(renameDialog, data);
    }

    private static void RenameMultiple(RenameDialog renameDialog, RenameData data)
    {
      renameDialog.SetMainText(data.Main);
      renameDialog.SetAdditionalText(data.Additional);
      renameDialog.SetSequence(data.BeforeSequence, data.AfterSequence, data.SkipFirst);

      renameDialog.SetCardOrder(RenameDialog.StringToCard(data.CardOrder[0]),
                                RenameDialog.StringToCard(data.CardOrder[1]),
                                RenameDialog.StringToCard(data.CardOrder[2]));

      // TODO
      renameDialog.Preview();
    }
  }

  public class RenameData
  {
    [TableAliases("Main[]?text", "Main")]
    public string Main { get; set; }
    [TableAliases("Additional[]?text", "Additional")]
    public string Additional { get; set; }
    [TableAliases("Before[]?sequence")]
    public string BeforeSequence { get; set; }
    [TableAliases("After[]?sequence")]
    public string AfterSequence { get; set; }
    [TableAliases("Skip[]?first", "Skip")]
    public bool SkipFirst { get; set; }
    [TableAliases("Card[]?order", "Order")]
    public string[] CardOrder { get; set; }
  }
}