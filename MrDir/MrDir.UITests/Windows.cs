﻿using System;
using System.Globalization;
using System.Linq;
using MrDir.UITests.WindowObjects;
using MrDir.UITests.WindowObjects.Dialogs;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.Utility;

namespace MrDir.UITests
{
  public static class Windows
  {
    private static Application m_application;

    public static MainWindow Main => new MainWindow(GetWindow("MrDir"));
    public static SettingsDialog Settings => new SettingsDialog(GetWindow(nameof(Settings)));
    public static NotificationsDialog Notifications => new NotificationsDialog(GetWindow(nameof(Notifications)));
		public static RenameDialog Rename => new RenameDialog(GetWindow(nameof(Rename)));
		public static ResolveConflictsDialog ResolveConflicts => new ResolveConflictsDialog(GetWindow(nameof(ResolveConflicts)));
		public static CultureInfo Culture { get; private set; }

    public static void Init(Application application)
    {
      m_application = application;
			Culture = new CultureInfo(MrDir.Properties.Settings.Default.Language);
      m_application.WaitWhileBusy();
    }

    public static Window GetWindow(string title)
    {
      return Retry.For(
        () => m_application.GetWindows()?.FirstOrDefault(x => x.Id.Contains(title)),
        TimeSpan.FromSeconds(5));
    }
  }
}
