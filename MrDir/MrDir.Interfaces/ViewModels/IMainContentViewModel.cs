﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using MrDir.Interfaces.Models;
using MrDir.Interfaces.Views;

namespace MrDir.Interfaces.ViewModels
{
  public interface IMainContentViewModel<T>
		: IViewModel
		where T : IViewModel
  {
    /// <summary>
    ///   Main page content
    /// </summary>
    IWindowView<T> WindowView { get; set; }

    /// <summary>
    ///   Ribbon tabs
    /// </summary>
    ObservableCollection<IRibbonTabItem> RibbonTabItems { get; set; }

    int NotificationsCount { get; set; }

    ICommand OpenNotificationsCommand { get; }
  }
}