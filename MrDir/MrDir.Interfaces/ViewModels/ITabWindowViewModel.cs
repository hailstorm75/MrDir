﻿using System.Collections.ObjectModel;
using Dragablz;

namespace MrDir.Interfaces.ViewModels
{
  public interface ITabWindowViewModel
		: IViewModel
  {
		/// <summary>
		/// Tab items
		/// </summary>
		ObservableCollection<ITabItem> Items { get; }
  }
}