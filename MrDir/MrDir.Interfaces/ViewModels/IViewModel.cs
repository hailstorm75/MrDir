﻿using System.ComponentModel;

namespace MrDir.Interfaces.ViewModels
{
	/// <summary>
	/// View model interface for <see cref="MrDir.Interfaces.Views.IView{T}"/>
	/// </summary>
	public interface IViewModel
		: INotifyPropertyChanged
	{
	}
}
