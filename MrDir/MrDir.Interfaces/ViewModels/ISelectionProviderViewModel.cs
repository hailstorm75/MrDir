﻿using System.Collections.Generic;

namespace MrDir.Interfaces.ViewModels
{
  public interface ISelectionProvider
  {
    IEnumerable<object> Selection { get; }
  }
}