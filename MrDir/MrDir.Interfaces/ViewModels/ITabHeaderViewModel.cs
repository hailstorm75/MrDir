﻿using System;
using System.Windows;

namespace MrDir.Interfaces.ViewModels
{
  public interface ITabHeaderViewModel
    : IDisposable
  {
    string Header { get; set; }
    string ToolTip { get; set; }

    // This should not be here
    bool IsSelected { get; set; }
    Visibility CheckBoxVisiblility { get; set; }
    event EventHandler SelectionChanged;
  }
}