﻿using System.Windows.Input;

namespace MrDir.Interfaces.ViewModels
{
  public interface IFocusableViewModel
  {
    bool IsFocused { get; set; }

    ICommand GotFocusCommand { get; }
    ICommand LostFocusCommand { get; }
  }
}