﻿using MrDir.Types;
using System;

namespace MrDir.Interfaces.ViewModels
{
	/// <summary>
	/// View model interface for <see cref="MrDir.Interfaces.Views.IPanelView{T}"/>
	/// </summary>
	/// <seealso cref="MrDir.Interfaces.ViewModels.IViewModel" />
	/// <seealso cref="System.IDisposable" />
	public interface IPanelViewModel
    : IViewModel, IDisposable
  {
		/// <summary>
		/// Occurs when panel requests a new title
		/// </summary>
    event EventHandler<EventArgs<Tuple<string, string>>> RequestedNewTitle;
  }
}