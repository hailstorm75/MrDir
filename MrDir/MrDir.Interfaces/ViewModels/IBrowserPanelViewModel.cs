﻿using MrDir.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace MrDir.Interfaces.ViewModels
{
	/// <summary>
	/// View model interface for <see cref="MrDir.Interfaces.Views.IBrowserPanelView"/>
	/// </summary>
	public interface IBrowserPanelViewModel
    : IPanelViewModel
  {
    IList<IFSEntity> Selection { get; set; }
    int SelectedEntitiesCount { get; }
    event EventHandler SelectedEntitiesCountChanged;
  }
}