﻿namespace MrDir.Interfaces.ViewModels
{
  public interface IActivatable
  {
    void Activate();
    void Deactivate();
  }
}