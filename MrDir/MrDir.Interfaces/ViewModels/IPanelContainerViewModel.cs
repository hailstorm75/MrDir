﻿using MrDir.Types;
using MrDir.Interfaces.Views;
using System;

namespace MrDir.Interfaces.ViewModels
{
	/// <summary>
	/// Interface for <see cref="IPanelContainerView"/> view models
	/// </summary>
	/// <seealso cref="MrDir.Interfaces.ViewModels.IViewModel" />
	/// <seealso cref="MrDir.Interfaces.ViewModels.ISelectionProvider" />
	/// <seealso cref="MrDir.Interfaces.ViewModels.IActivatable" />
	/// <seealso cref="System.IDisposable" />
	public interface IPanelContainerViewModel
    : IViewModel, ISelectionProvider, IActivatable, IDisposable
  {
		/// <summary>
		/// Contained panel
		/// </summary>
		IPanelView<IPanelViewModel> Content { get; }
		/// <summary>
		/// Occurs when contained panel gets focus
		/// </summary>
		event EventHandler GotFocus;
		/// <summary>
		/// Occurs when contained panel requests a new title
		/// </summary>
    event EventHandler<EventArgs<Tuple<string, string>>> RequestedNewTitle;
  }
}