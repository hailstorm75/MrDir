﻿using System;
using System.ComponentModel;

namespace MrDir.Interfaces
{
  public interface IPanelViewMetadata
  {
    [DefaultValue("Unknown panel")]
		string Name { get; }

    [DefaultValue("")]
		string Guid { get; }

		[DefaultValue(typeof(object))]
		Type Type { get; }
  }
}