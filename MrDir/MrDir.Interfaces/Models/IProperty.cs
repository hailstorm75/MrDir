﻿namespace MrDir.Interfaces.Models
{
  public interface IProperty
  {
    object Value { get; set; }
    string Name { get; }
		string Grouping { get; }
		bool IsReadOnly { get; }
  }
}