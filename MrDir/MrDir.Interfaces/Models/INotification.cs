﻿using MrDir.Enums;

namespace MrDir.Interfaces.Models
{
	public interface INotification
	{
		NotificationType Type { get; }
		string Message { get; }
		string Details { get; }
	}
}