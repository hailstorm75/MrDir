﻿namespace MrDir.Interfaces.Models
{
  public interface IFile
    : IFSEntity
  {
    string Extension { get; }
  }
}