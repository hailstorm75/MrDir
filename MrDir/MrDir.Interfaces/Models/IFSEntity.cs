﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Media;
using MrDir.Attributes;
using MrDir.Cultures;

namespace MrDir.Interfaces.Models
{
  /// <summary>
  ///   File system entity
  /// </summary>
  public interface IFSEntity
    : INotifyPropertyChanged, IDisposable
  {
    ImageSource Icon { get; }
    string Name { get; }
    string Path { get; }
    string ToolTip { get; }

    [DataGridColumn(nameof(Resources.Size), false)]
    string Size { get; }

    [DataGridColumn(nameof(Resources.DateCreated), false)]
    DateTime CreationDate { get; }

    [DataGridColumn(nameof(Resources.DateModified), false)]
    DateTime ModifiedDate { get; }

    [DataGridColumn(nameof(Resources.DateAccessed), false)]
    DateTime AccessDate { get; }

    [DataGridColumn(nameof(Resources.AccessRights), false)]
    string Access { get; }

		Task Refresh();
  }
}