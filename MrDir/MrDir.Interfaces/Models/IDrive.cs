﻿using System.Windows.Media;

namespace MrDir.Interfaces.Models
{
  public interface IDrive
  {
    ImageSource Icon { get; }
    string Path { get; }
    string Label { get; }
    string Name { get; }
  }
}