﻿using System;
using Fluent;

namespace MrDir.Interfaces.Models
{
  /// <summary>
  /// Interface for ribbon tabs
  /// </summary>
  public interface IRibbonTabItem
    : IComparable<IRibbonTabItem>, IEquatable<IRibbonTabItem>
  {
    /// <summary>
    /// Tab order weight
    /// </summary>
    int Order { get; }

    /// <summary>
    /// Tab header
    /// </summary>
    string Header { get; }

    /// <summary>
    /// Tab presenter
    /// </summary>
    RibbonTabItem View { get; }
  }
}