﻿using System.Windows.Media;

namespace MrDir.Interfaces.Models
{
	/// <summary>
	/// Interface for bookmarks
	/// </summary>
	public interface IBookmark
  {
		/// <summary>
		/// Bookmark label
		/// </summary>
		string Label { get; }
		/// <summary>
		/// Path to bookmarked ite
		/// </summary>
		string Path { get; }
		/// <summary>
		/// Bookmark icon
		/// </summary>
		ImageSource Icon { get; }

		/// <summary>
		/// Rename bookmark
		/// </summary>
		/// <param name="newName">The new name</param>
		void Rename(string newName);
  }
}