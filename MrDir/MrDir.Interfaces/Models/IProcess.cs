﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using MrDir.Enums;

namespace MrDir.Interfaces.Models
{
	/// <summary>
	/// Interface for Processes
	/// </summary>
	public interface IProcess
  {
		/// <summary>
		/// Process name
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		string Name { get; }
		/// <summary>
		/// Creation time of process
		/// </summary>
		/// <value>
		/// The creation time.
		/// </value>
		DateTime CreationTime { get; }
		/// <summary>
		/// Running process progress
		/// </summary>
		/// <value>
		/// The progress.
		/// </value>
		double Progress { get; set; }
		/// <summary>
		/// Gets a value indicating whether this instance is complete.
		/// </summary>
		/// <value>
		/// <c>true</c> if this instance is complete; otherwise, <c>false</c>.
		/// </value>
		bool IsComplete { get; }
		/// <summary>
		/// Bindable command for process cancellation
		/// </summary>
		/// <value>
		/// The cancel command.
		/// </value>
		ICommand CancelCommand { get; }

		/// <summary>
		/// Cancels the running process
		/// </summary>
		void Cancel();
		/// <summary>
		/// Runs the process
		/// </summary>
		/// <returns>Process result</returns>
		Task<ProcessResult> Run();
  }
}