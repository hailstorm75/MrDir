﻿using MrDir.Interfaces.ViewModels;
using MrDir.Interfaces.Views;

namespace MrDir.Interfaces
{
	/// <summary>
	/// Interface for panel providers
	/// </summary>
	public interface IPanelProvider
  {
		/// <summary>
		/// Provides a new instance of a panel
		/// </summary>
		/// <returns>New panel instance</returns>
		IPanelView<IPanelViewModel> Provide();
  }
}