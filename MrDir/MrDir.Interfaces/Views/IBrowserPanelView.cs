﻿using MrDir.Interfaces.Models;
using MrDir.Interfaces.ViewModels;
using System;
using System.Collections.Generic;

namespace MrDir.Interfaces.Views
{
	/// <summary>
	/// Interface for specific panel type of browser
	/// </summary>
	/// <seealso cref="MrDir.Interfaces.Views.IPanelView{MrDir.Interfaces.ViewModels.IBrowserPanelViewModel}" />
	public interface IBrowserPanelView
		: IPanelView<IBrowserPanelViewModel>
	{
		/// <summary>
		/// Gets the selected entities count in browser
		/// </summary>
		int SelectedCount { get; }
		/// <summary>
		/// Gets the selection
		/// </summary>
		IEnumerable<IFSEntity> Selection { get; }

		/// <summary>
		/// Occurs when selected entities count changes
		/// </summary>
		event EventHandler SelectedEntitiesCountChanged;
	}
}
