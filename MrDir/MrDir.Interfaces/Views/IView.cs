﻿using MrDir.Interfaces.ViewModels;

namespace MrDir.Interfaces.Views
{
	/// <summary>
	/// User Interface
	/// </summary>
	/// <typeparam name="T"><see cref="IViewModel"/> type</typeparam>
	public interface IView<out T>
		where T : IViewModel
	{
		/// <summary>
		/// DataContext provider of given View
		/// </summary>
		T ViewModel { get; }
	}
}
