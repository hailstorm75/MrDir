﻿using MrDir.Interfaces.ViewModels;
using System.Windows.Controls;

namespace MrDir.Interfaces.Views
{
  public interface IWindowView<T>
		: IView<T> where T : IViewModel
  {
    Control MainControl { get; }
  }
}