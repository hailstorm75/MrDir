﻿using System;
using MrDir.Interfaces.ViewModels;

namespace MrDir.Interfaces.Views
{
	/// <summary>
	/// Interface for panel containers
	/// </summary>
	/// <seealso cref="MrDir.Interfaces.ViewModels.IActivatable" />
	/// <seealso cref="System.IDisposable" />
	/// <seealso cref="MrDir.Interfaces.Views.IView{MrDir.Interfaces.ViewModels.IPanelContainerViewModel}" />
	public interface IPanelContainerView
    : IActivatable, IDisposable, IView<IPanelContainerViewModel>
  {
		/// <summary>
		/// Container panel tab header
		/// </summary>
		ITabHeaderViewModel Header { get; set; }
  }
}