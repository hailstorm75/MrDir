﻿using MrDir.Types;
using MrDir.Interfaces.ViewModels;
using System;

namespace MrDir.Interfaces.Views
{
	/// <summary>
	/// Interface for panels
	/// </summary>
	/// <typeparam name="T">Type of IPanelViewModel</typeparam>
	/// <seealso cref="IActivatable" />
	/// <seealso cref="IDisposable" />
	/// <seealso cref="Views.IView{T}" />
	public interface IPanelView<out T>
    : IActivatable, IDisposable, IView<T> where T : IPanelViewModel
  {
		/// <summary>
		/// Occurs when panel content gets focus
		/// </summary>
		event EventHandler ContentGotFocus;
		/// <summary>
		/// Occurs when panel content requests a new title
		/// </summary>
		event EventHandler<EventArgs<Tuple<string, string>>> RequestedNewTitle;
  }
}