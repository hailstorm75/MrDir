﻿using MrDir.Attributes;
using MrDir.Cultures;

namespace MrDir.Enums
{
	public enum SearchMode
	{
		[EnumInfo(nameof(Glob), nameof(Resources.ToolTipGlob))]
		Glob,
		[EnumInfo(nameof(RegEx), nameof(Resources.ToolTipRegex))]
		RegEx
	}
}
