﻿using MrDir.Attributes;

namespace MrDir.Enums
{
	public enum NotificationType
	{
		[NotificationMetadata(nameof(Information), "Information")]
		Information,

		[NotificationMetadata(nameof(Warning), "Warning")]
		Warning,

		[NotificationMetadata(nameof(Error), "Stop")]
		Error
	}
}
