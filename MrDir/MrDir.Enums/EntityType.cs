﻿namespace MrDir.Enums
{
	public enum EntityType
	{
		File,
		CompoundFile,
		Directory
	}
}
