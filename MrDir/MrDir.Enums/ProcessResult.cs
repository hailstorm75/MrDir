﻿namespace MrDir.Enums
{
	public enum ProcessResult
	{
		NotRun,
		Finished,
		FinishedWithErrors,
		Cancelled,
	}
}
