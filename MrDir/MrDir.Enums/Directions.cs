﻿namespace MrDir.Enums
{
	public enum Directions
	{
		None,
		Left,
		Right,
		Up,
		Down
	}
}
