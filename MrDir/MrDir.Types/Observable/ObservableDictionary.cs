﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MrDir.Types.Observable
{
  [Serializable]
  public class ObservableDictionary<TKey, TValue>
    : IDictionary<TKey, TValue>, INotifyPropertyChanged
    where TKey : IEquatable<TKey>
  {
    #region Constructors

    /// <summary>
    /// Creates a ContentLocatorPart with the specified type name and namespace.
    /// </summary>
    public ObservableDictionary()
      => m_dictionary = new Dictionary<TKey, TValue>();

    public ObservableDictionary(Dictionary<TKey, TValue> dictionary)
      => m_dictionary = dictionary;

    #endregion Constructors

    #region Methods

    /// <summary>
    /// Adds a key/value pair to the ContentLocatorPart.  If a value for the key already
    /// exists, the old value is overwritten by the new value.
    /// </summary>
    /// <param name="key">key</param>
    /// <param name="val">value</param>
    /// <exception cref="ArgumentNullException">key or val is null</exception>
    /// <exception cref="ArgumentException">a value for key is already present in the locator part</exception>
    public void Add(TKey key, TValue val)
    {
      if (key == null || val == null)
        throw new ArgumentNullException(key == null ? "key" : "val");

      m_dictionary.Add(key, val);
      OnPropertyChanged(nameof(Keys));
    }

    /// <summary>
    /// Removes all name/value pairs from the ContentLocatorPart.
    /// </summary>
    public void Clear()
    {
      var count = m_dictionary.Count;

      if (count <= 0) return;

      m_dictionary.Clear();

      // Only fire changed event if the dictionary actually changed
      OnPropertyChanged(nameof(Keys));
    }

    /// <summary>
    /// Returns whether or not a value of the key exists in this ContentLocatorPart.
    /// </summary>
    /// <param name="key">the key to check for</param>
    /// <returns>true - yes, false - no</returns>
    public bool ContainsKey(TKey key)
      => m_dictionary.ContainsKey(key);

    /// <summary>
    ///     Removes the key and its value from the ContentLocatorPart.
    /// </summary>
    /// <param name="key">key to be removed</param>
    /// <returns>true - the key was found in the ContentLocatorPart, false o- it wasn't</returns>
    public bool Remove(TKey key)
    {
      var exists = m_dictionary.Remove(key);

      // Only fire changed event if the key was actually removed
      if (exists)
        OnPropertyChanged(nameof(Keys));

      return exists;
    }

    /// <summary>
    /// Returns an enumerator for the key/value pairs in this ContentLocatorPart.
    /// </summary>
    /// <returns>an enumerator for the key/value pairs; never returns null</returns>
    IEnumerator IEnumerable.GetEnumerator()
      => m_dictionary.GetEnumerator();

    /// <summary>
    /// Returns an enumerator forthe key/value pairs in this ContentLocatorPart.
    /// </summary>
    /// <returns>an enumerator for the key/value pairs; never returns null</returns>
    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
      => ((IEnumerable<KeyValuePair<TKey, TValue>>)m_dictionary).GetEnumerator();

    /// <summary>
    ///
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">key is null</exception>
    public bool TryGetValue(TKey key, out TValue value)
    {
      if (key == null)
        throw new ArgumentNullException(nameof(key));

      return m_dictionary.TryGetValue(key, out value);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="pair"></param>
    /// <exception cref="ArgumentNullException">pair is null</exception>
    void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> pair)
    {
      ((ICollection<KeyValuePair<TKey, TValue>>)m_dictionary).Add(pair);
      OnPropertyChanged(nameof(Keys));
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="pair"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">pair is null</exception>
    bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> pair)
    {
      return ((ICollection<KeyValuePair<TKey, TValue>>)m_dictionary).Contains(pair);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="pair"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">pair is null</exception>
    bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> pair)
    {
      OnPropertyChanged(nameof(Keys));
      return ((ICollection<KeyValuePair<TKey, TValue>>)m_dictionary).Remove(pair);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="target"></param>
    /// <param name="startIndex"></param>
    /// <exception cref="ArgumentNullException">target is null</exception>
    /// <exception cref="ArgumentOutOfRangeException">startIndex is less than zero or greater than the lenght of target</exception>
    void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] target, int startIndex)
    {
      if (target == null)
        throw new ArgumentNullException(nameof(target));
      if (startIndex < 0 || startIndex > target.Length)
        throw new ArgumentOutOfRangeException(nameof(startIndex));

      ((ICollection<KeyValuePair<TKey, TValue>>)m_dictionary).CopyTo(target, startIndex);
    }

    #endregion

    #region Properties

    /// <summary>
    /// The number of name/value pairs in this ContentLocatorPart.
    /// </summary>
    /// <value>count of name/value pairs</value>
    public int Count => m_dictionary.Count;

    /// <inheritdoc />
    /// <summary>
    /// Indexer provides lookup of values by key.
    /// Gets or sets the value in the ContentLocatorPart for the specified key.
    /// If the key does not exist in the ContentLocatorPart,
    /// </summary>
    /// <param name="key">key</param>
    /// <returns>the value stored in this locator part for key</returns>
    public TValue this[TKey key]
    {
      get
      {
        if (key == null)
          throw new ArgumentNullException(nameof(key));

        m_dictionary.TryGetValue(key, out var value);
        return value;
      }
      set
      {
        if (key == null)
          throw new ArgumentNullException(nameof(key));

        if (value == null)
          throw new ArgumentNullException(nameof(value));

        m_dictionary.TryGetValue(key, out var oldValue);

        // If the new value is actually different, then we add it and fire
        // a change notification
        if ((oldValue != null)
         && (oldValue.Equals(value))) return;
        m_dictionary[key] = value;
        OnPropertyChanged(nameof(Values));
      }
    }

    /// <summary>
    ///
    /// </summary>
    public bool IsReadOnly => false;

    /// <summary>
    ///     Returns a collection of all the keys in this ContentLocatorPart.
    /// </summary>
    /// <value>keys</value>
    public ICollection<TKey> Keys => m_dictionary.Keys;

    /// <summary>
    ///     Returns a collection of all the values in this ContentLocatorPart.
    /// </summary>
    /// <value>values</value>
    public ICollection<TValue> Values => m_dictionary.Values;

    #endregion

    #region Events

    public event PropertyChangedEventHandler PropertyChanged;

    #endregion

    #region Methods

    /// <summary>
    /// Notify the owner this ContentLocatorPart has changed.
    /// </summary>
    private void OnPropertyChanged([CallerMemberName] string propertyName
      = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

    #endregion

    #region Fields

    /// <summary>
    /// The internal data structure.
    /// </summary>
    private Dictionary<TKey, TValue> m_dictionary;

    #endregion
  }
}

