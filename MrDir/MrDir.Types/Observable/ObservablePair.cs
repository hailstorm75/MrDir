﻿using MrDir.Types.Notifiers;
using System;

namespace MrDir.Types.Observable
{
  [Serializable]
  public class ObservablePair<T1, T2>
    : NotifyPropertyChanged
  {
    private T2 m_right;
    private T1 m_left;

    public T1 Left
    {
      get => m_left;
      set
      {
        m_left = value;
        OnPropertyChanged(nameof(Left));
      }
    }

    public T2 Right
    {
      get => m_right;
      set
      {
        m_right = value;
        OnPropertyChanged(nameof(Right));
      }
    }

    public ObservablePair(T1 left, T2 right)
    {
      Left = left;
      Right = right;
    }
  }
}