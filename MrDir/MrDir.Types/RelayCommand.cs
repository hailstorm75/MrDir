﻿using System;
using System.Windows.Input;

namespace MrDir.Types
{
	/// <summary>
	/// Command which is invoked by a View and executes a defined action
	/// </summary>
	public class RelayCommand
		: ICommand
	{
		#region Events

		/// <inheritdoc cref="ICommand.CanExecuteChanged"/>
		public event EventHandler CanExecuteChanged;

		/// <summary>
		/// Notify whether <see cref="RelayCommand"/> can execute the action
		/// </summary>
		public virtual void OnCanExecuteChanged()
			=> CanExecuteChanged?.Invoke(this, EventArgs.Empty);

		#endregion

		#region Fields

		private readonly Action m_action;
		private readonly Func<bool> m_executionRegulator;

		#endregion

		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="action">Action to execute</param>
		/// <param name="executionRegulator">Function which regulates whether given command is to be executed</param>
		public RelayCommand(Action action, Func<bool> executionRegulator = null)
		{
			m_action = action;
			m_executionRegulator = executionRegulator ?? (() => true);
		}

		#region Methods

		/// <inheritdoc cref="ICommand.CanExecute(object)"/>
		public bool CanExecute(object parameter)
			=> m_executionRegulator();

		/// <inheritdoc cref="ICommand.Execute(object)"/>
		public void Execute(object parameter)
			=> m_action();

		#endregion
	}

	/// <summary>
	/// Command which is invoked by a View with a CommandParameter and executes a defined action
	/// </summary>
	/// <typeparam name="T">CommandParameter type</typeparam>
	public class RelayCommand<T>
		: ICommand
	{
		#region Events

		/// <inheritdoc cref="ICommand.CanExecuteChanged"/>
		public event EventHandler CanExecuteChanged;

		/// <summary>
		/// Notify whether <see cref="RelayCommand{T}"/> can execute the action
		/// </summary>
		public virtual void OnCanExecuteChanged()
			=> CanExecuteChanged?.Invoke(this, EventArgs.Empty);

		#endregion

		#region Fields

		private readonly Action<T> m_action;
		private readonly Func<T, bool> m_executionRegulator;

		#endregion

		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="action">Action to execute</param>
		/// <param name="executionRegulator">Function which regulates whether given command is to be executed</param>
		public RelayCommand(Action<T> action, Func<T, bool> executionRegulator = null)
		{
			m_action = action;
			m_executionRegulator = executionRegulator ?? (_ => true);
		}

		#region Methods

		/// <inheritdoc cref="ICommand.CanExecute(object)"/>
		public bool CanExecute(object parameter)
			=> m_executionRegulator((T)parameter);

		/// <inheritdoc cref="ICommand.Execute(object)"/>
		public void Execute(object parameter)
			=> m_action((T)parameter);

		#endregion
	}
}