﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace MrDir.Types.Notifiers
{
	/// <summary>
	/// Base for classes which interact with Views including property validation logic
	/// </summary>
	public abstract class NotifyValidPropertyChanged
    : NotifyPropertyChanged, INotifyDataErrorInfo
  {
    [field:NonSerialized]
    protected readonly ConcurrentDictionary<string, string> m_propertyErrors = new ConcurrentDictionary<string, string>();
    [field:NonSerialized]
    protected Dictionary<string, Func<Task<bool>>> m_propertyCheckers;

		/// <inheritdoc cref="INotifyDataErrorInfo.HasErrors"/>
    [JsonIgnore]
    public bool HasErrors
    {
      get
      {
        try
        {
          var propErrorsCount = m_propertyErrors.Values.FirstOrDefault(r => r != null);
          return propErrorsCount != null;
        }
        catch { }
        return true;
      }
    }

		/// <inheritdoc cref="INotifyDataErrorInfo.ErrorsChanged"/>
    [field:NonSerialized]
    public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

    [field:NonSerialized]
    private event EventHandler ValidationRequired;

    protected NotifyValidPropertyChanged()
    {
      ValidationRequired += ReceiveValidationRequired;
    }

    protected virtual void ReceiveValidationRequired(object sender, EventArgs eventArgs)
    {

    }

		/// <inheritdoc cref="INotifyDataErrorInfo.GetErrors(string)"/>
    public IEnumerable GetErrors(string propertyName)
    {
      if (propertyName == null)
        yield break;

      m_propertyErrors.TryGetValue(propertyName, out var error);
      if (error != null)
        yield return error;
    }

    protected async Task<bool> Validate(string propertyName, bool skip, params Func<Task<string>>[] checks)
    {
      string error = null;
      if (!skip)
        foreach (var check in checks)
        {
          error = await check().ConfigureAwait(false);
          if (error != null)
            break;
        }

      m_propertyErrors.AddOrUpdate(propertyName, error, (x, _) => error);

      OnPropertyErrorsChanged(propertyName);
      OnPropertyChanged(nameof(HasErrors));

      return error == null;
    }

    protected void ClearErrors()
    {
      m_propertyErrors.Clear();
      OnPropertyChanged(nameof(HasErrors));
    }

    protected void SendValidationRequired()
      => ValidationRequired?.Invoke(this, EventArgs.Empty);

    protected void OnPropertyErrorsChanged(string propertyName)
      => ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));

    protected async void OnPropertyChanged([CallerMemberName] string propertyName = "", Func<Task> validation = null)
    {
      base.OnPropertyChanged(propertyName);
      if (validation != null)
        await validation().ConfigureAwait(false);
    }
  }
}
