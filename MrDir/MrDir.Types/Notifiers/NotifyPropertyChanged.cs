﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MrDir.Types.Notifiers
{
	/// <summary>
	/// Base for classes which interact with Views
	/// </summary>
	[Serializable]
  public class NotifyPropertyChanged
    : INotifyPropertyChanged
  {
		/// <inheritdoc cref="INotifyPropertyChanged.PropertyChanged"/>
		[field:NonSerialized]
    public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Notify the View of changes to a given <paramref name="property"/> value
		/// </summary>
		/// <param name="property">
		/// Name of property
		/// </param>
		/// <example>
		/// Use the nameof keyword instead of typing the <paramref name="property"/> name by hand:
		/// <code>OnPropertyChanged(nameof(MyProperty)</code>
		/// <para/>
		/// or leave empty to use the <see cref="CallerMemberNameAttribute"/>
		/// </example>
		protected void OnPropertyChanged([CallerMemberName] string property = "")
      => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
  }
}