﻿using System.Windows;

namespace MrDir.Types
{
	public class BindingProxy
		: Freezable
	{
		public static readonly DependencyProperty DataProperty
			= DependencyProperty.Register(nameof(Data), typeof(object), typeof(BindingProxy));

		public object Data
		{
			get => GetValue(DataProperty);
			set => SetValue(DataProperty, value);
		}

		protected override Freezable CreateInstanceCore()
			=> new BindingProxy();
	}
}
