using MrDir.Enums;
using System;
using System.IO;

namespace MrDir.Types.NETShares
{
  /// <summary>
  /// Information about a local share
  /// </summary>
  public class Share
  {
		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public Share(string server, string netName, string path, ShareType shareType, string remark)
    {
      if (ShareType.Special == shareType && "IPC$" == netName)
        shareType |= ShareType.IPC;

      Server = server;
      NetName = netName;
      Path = path;
      ShareType = shareType;
      Remark = remark;
    }

		#endregion

		#region Properties

		/// <summary>
		/// The name of the computer that this share belongs to
		/// </summary>
		public string Server { get; }

		/// <summary>
		/// Share name
		/// </summary>
		public string NetName { get; }

		/// <summary>
		/// Local path
		/// </summary>
		public string Path { get; }

		/// <summary>
		/// Share type
		/// </summary>
		public ShareType ShareType { get; }

		/// <summary>
		/// Comment
		/// </summary>
		public string Remark { get; }

		/// <summary>
		/// Returns true if this is a file system share
		/// </summary>
		public bool IsFileSystem
    {
      get
      {
        // Shared device
        if (0 != (ShareType & ShareType.Device)) return false;
        // IPC share
        if (0 != (ShareType & ShareType.IPC)) return false;
        // Shared printer
        if (0 != (ShareType & ShareType.Printer)) return false;
        // Standard disk share
        if (0 == (ShareType & ShareType.Special)) return true;

				// Special disk share (e.g. C$)
				return ShareType == ShareType.Special
						&& NetName != null
						&& NetName.Length != 0;
			}
    }

		/// <summary>
		/// Get the root of a disk-based share
		/// </summary>
		public DirectoryInfo Root =>
			IsFileSystem
				? Server == null || 0 == Server.Length
					? Path == null || 0 == Path.Length
						? new DirectoryInfo(ToString())
						: new DirectoryInfo(Path)
					: new DirectoryInfo(ToString())
				: null;

		#endregion

		/// <summary>
		/// Returns the path to this share
		/// </summary>
		public override string ToString()
			=> string.IsNullOrEmpty(Server)
				? $@"\\{Environment.MachineName}\{NetName}"
        : $@"\\{Server}\{NetName}";

		/// <summary>
		/// Returns true if this share matches the local path
		/// </summary>
		/// <param name="path"></param>
		public bool MatchesPath(string path)
    {
      if (!IsFileSystem) return false;
      if (string.IsNullOrEmpty(path)) return true;

      return path.StartsWith(Path, StringComparison.CurrentCultureIgnoreCase);
    }
  }
}
