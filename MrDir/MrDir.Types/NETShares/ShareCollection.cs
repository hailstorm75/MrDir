﻿using MrDir.Enums;
using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;

namespace MrDir.Types.NETShares
{
  #region Share Collection

  /// <summary>
  /// A collection of shares
  /// </summary>
  public class ShareCollection
    : ReadOnlyCollectionBase
  {
    #region Platform

    /// <summary>
    /// Is this an NT platform?
    /// </summary>
    protected static bool IsNT
      => (PlatformID.Win32NT == Environment.OSVersion.Platform);

    /// <summary>
    /// Returns true if this is Windows 2000 or higher
    /// </summary>
    protected static bool IsW2KUp
    {
      get
      {
        var os = Environment.OSVersion;
        return PlatformID.Win32NT == os.Platform && os.Version.Major >= 5;
      }
    }

    #endregion

    #region Interop

    #region Constants

    /// <summary>Maximum path length</summary>
    protected const int MAX_PATH = 260;
    /// <summary>No error</summary>
    protected const int NO_ERROR = 0;
    /// <summary>Access denied</summary>
    protected const int ERROR_ACCESS_DENIED = 5;
    /// <summary>Access denied</summary>
    protected const int ERROR_WRONG_LEVEL = 124;
    /// <summary>More data available</summary>
    protected const int ERROR_MORE_DATA = 234;
    /// <summary>Not connected</summary>
    protected const int ERROR_NOT_CONNECTED = 2250;
    /// <summary>Level 1</summary>
    protected const int UNIVERSAL_NAME_INFO_LEVEL = 1;
    /// <summary>Max extries (9x)</summary>
    protected const int MAX_SI50_ENTRIES = 20;

    #endregion

    #region Structures

    /// <summary>Unc name</summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    protected struct UNIVERSAL_NAME_INFO
    {
      [MarshalAs(UnmanagedType.LPTStr)]
      public string lpUniversalName;
    }

    /// <summary>Share information, NT, level 2</summary>
    /// <remarks>
    /// Requires admin rights to work.
    /// </remarks>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    protected struct SHARE_INFO_2
    {
      [MarshalAs(UnmanagedType.LPWStr)]
      public string NetName;
      public ShareType ShareType;
      [MarshalAs(UnmanagedType.LPWStr)]
      public string Remark;
      public int Permissions;
      public int MaxUsers;
      public int CurrentUsers;
      [MarshalAs(UnmanagedType.LPWStr)]
      public string Path;
      [MarshalAs(UnmanagedType.LPWStr)]
      public string Password;
    }

    /// <summary>Share information, NT, level 1</summary>
    /// <remarks>
    /// Fallback when no admin rights.
    /// </remarks>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    protected struct SHARE_INFO_1
    {
      [MarshalAs(UnmanagedType.LPWStr)]
      public string NetName;
      public ShareType ShareType;
      [MarshalAs(UnmanagedType.LPWStr)]
      public string Remark;
    }

    /// <summary>Share information, Win9x</summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    protected struct SHARE_INFO_50
    {
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 13)]
      public readonly string NetName;

      public readonly byte bShareType;
      public readonly ushort Flags;

      [MarshalAs(UnmanagedType.LPTStr)]
      public readonly string Remark;
      [MarshalAs(UnmanagedType.LPTStr)]
      public readonly string Path;

      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 9)]
      public readonly string PasswordRW;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 9)]
      public readonly string PasswordRO;

      public ShareType ShareType => (ShareType)((int)bShareType & 0x7F);
    }

    /// <summary>Share information level 1, Win9x</summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    protected struct SHARE_INFO_1_9x
    {
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 13)]
      public readonly string NetName;
      public readonly byte Padding;

      public readonly ushort bShareType;

      [MarshalAs(UnmanagedType.LPTStr)]
      public readonly string Remark;

      public ShareType ShareType => (ShareType)((int)bShareType & 0x7FFF);
    }

    #endregion

    #region Functions

    /// <summary>Get a UNC name</summary>
    [DllImport("mpr", CharSet = CharSet.Auto)]
    protected static extern int WNetGetUniversalName(string lpLocalPath,
      int dwInfoLevel, ref UNIVERSAL_NAME_INFO lpBuffer, ref int lpBufferSize);

    /// <summary>Get a UNC name</summary>
    [DllImport("mpr", CharSet = CharSet.Auto)]
    protected static extern int WNetGetUniversalName(string lpLocalPath,
      int dwInfoLevel, IntPtr lpBuffer, ref int lpBufferSize);

    /// <summary>Enumerate shares (NT)</summary>
    [DllImport("netapi32", CharSet = CharSet.Unicode)]
    internal static extern int NetShareEnum(string lpServerName, int dwLevel,
      out IntPtr lpBuffer, int dwPrefMaxLen, out int entriesRead,
      out int totalEntries, ref int hResume);

    /// <summary>Enumerate shares (9x)</summary>
    [DllImport("svrapi", CharSet = CharSet.Ansi)]
    internal static extern int NetShareEnum(
      [MarshalAs(UnmanagedType.LPTStr)] string lpServerName, int dwLevel,
      IntPtr lpBuffer, ushort cbBuffer, out ushort entriesRead,
      out ushort totalEntries);

    /// <summary>Free the buffer (NT)</summary>
    [DllImport("netapi32")]
    internal static extern int NetApiBufferFree(IntPtr lpBuffer);

    #endregion

    #region Enumerate shares

    /// <summary>
    /// Enumerates the shares on Windows NT
    /// </summary>
    /// <param name="server">The server name</param>
    /// <param name="shares">The ShareCollection</param>
    protected static void EnumerateSharesNT(string server, ShareCollection shares)
    {
      var level = 2;
      var hResume = 0;
      var pBuffer = IntPtr.Zero;

      try
      {
        var nRet = NetShareEnum(server, level, out pBuffer, -1,
          out var entriesRead, out var totalEntries, ref hResume);

        if (ERROR_ACCESS_DENIED == nRet)
        {
          //Need admin for level 2, drop to level 1
          level = 1;
          nRet = NetShareEnum(server, level, out pBuffer, -1,
            out entriesRead, out totalEntries, ref hResume);
        }

        if (NO_ERROR == nRet && entriesRead > 0)
        {
          var t = (2 == level) ? typeof(SHARE_INFO_2) : typeof(SHARE_INFO_1);
          var offset = Marshal.SizeOf(t);

          for (int i = 0, lpItem = pBuffer.ToInt32(); i < entriesRead; i++, lpItem += offset)
          {
            var pItem = new IntPtr(lpItem);
            if (1 == level)
            {
              var si = (SHARE_INFO_1)Marshal.PtrToStructure(pItem, t);
              shares.Add(si.NetName, string.Empty, si.ShareType, si.Remark);
            }
            else
            {
              var si = (SHARE_INFO_2)Marshal.PtrToStructure(pItem, t);
              shares.Add(si.NetName, si.Path, si.ShareType, si.Remark);
            }
          }
        }

      }
      finally
      {
        // Clean up buffer allocated by system
        if (IntPtr.Zero != pBuffer)
          NetApiBufferFree(pBuffer);
      }
    }

    /// <summary>
    /// Enumerates the shares on Windows 9x
    /// </summary>
    /// <param name="server">The server name</param>
    /// <param name="shares">The ShareCollection</param>
    protected static void EnumerateShares9x(string server, ShareCollection shares)
    {
      var level = 50;

      var t = typeof(SHARE_INFO_50);
			var size = Marshal.SizeOf(t);
      var cbBuffer = (ushort)(MAX_SI50_ENTRIES * size);
      //On Win9x, must allocate buffer before calling API
      var pBuffer = Marshal.AllocHGlobal(cbBuffer);

      try
      {
        var nRet = NetShareEnum(server, level, pBuffer, cbBuffer, out var entriesRead, out var totalEntries);

        if (ERROR_WRONG_LEVEL == nRet)
        {
          level = 1;
          t = typeof(SHARE_INFO_1_9x);
          size = Marshal.SizeOf(t);

          nRet = NetShareEnum(server, level, pBuffer, cbBuffer, out entriesRead, out totalEntries);
        }

        if (NO_ERROR == nRet || ERROR_MORE_DATA == nRet)
        {
          for (int i = 0, lpItem = pBuffer.ToInt32(); i < entriesRead; i++, lpItem += size)
          {
            var pItem = new IntPtr(lpItem);

            if (1 == level)
            {
              var si = (SHARE_INFO_1_9x)Marshal.PtrToStructure(pItem, t);
              shares.Add(si.NetName, string.Empty, si.ShareType, si.Remark);
            }
            else
            {
              var si = (SHARE_INFO_50)Marshal.PtrToStructure(pItem, t);
              shares.Add(si.NetName, si.Path, si.ShareType, si.Remark);
            }
          }
        }
        else
          Console.WriteLine(nRet);

      }
      finally
      {
        //Clean up buffer
        Marshal.FreeHGlobal(pBuffer);
      }
    }

    /// <summary>
    /// Enumerates the shares
    /// </summary>
    /// <param name="server">The server name</param>
    /// <param name="shares">The ShareCollection</param>
    protected static void EnumerateShares(string server, ShareCollection shares)
    {
      if (!string.IsNullOrEmpty(server) && !IsW2KUp)
      {
        server = server.ToUpper();

        // On NT4, 9x and Me, server has to start with "\\"
        if (!('\\' == server[0] && '\\' == server[1]))
          server = @"\\" + server;
      }

      if (IsNT)
        EnumerateSharesNT(server, shares);
      else
        EnumerateShares9x(server, shares);
    }

    #endregion

    #endregion

    #region Static methods

    /// <summary>
    /// Returns true if fileName is a valid local file-name of the form:
    /// X:\, where X is a drive letter from A-Z
    /// </summary>
    /// <param name="fileName">The filename to check</param>
    public static bool IsValidFilePath(string fileName)
    {
      if (string.IsNullOrEmpty(fileName)) return false;

      var drive = char.ToUpper(fileName[0]);
      if ('A' > drive || drive > 'Z')
        return false;

      if (Path.VolumeSeparatorChar != fileName[1])
        return false;
      if (Path.DirectorySeparatorChar != fileName[2])
        return false;
      return true;
    }

    /// <summary>
    /// Returns the UNC path for a mapped drive or local share.
    /// </summary>
    /// <param name="fileName">The path to map</param>
    /// <returns>The UNC path (if available)</returns>
    public static string PathToUnc(string fileName)
    {
      if (string.IsNullOrEmpty(fileName)) return string.Empty;

      fileName = Path.GetFullPath(fileName);
      if (!IsValidFilePath(fileName)) return fileName;

      var nRet = 0;
      var rni = new UNIVERSAL_NAME_INFO();
      var bufferSize = Marshal.SizeOf(rni);

      nRet = WNetGetUniversalName(
        fileName, UNIVERSAL_NAME_INFO_LEVEL,
        ref rni, ref bufferSize);

      if (ERROR_MORE_DATA == nRet)
      {
        var pBuffer = Marshal.AllocHGlobal(bufferSize); ;
        try
        {
          nRet = WNetGetUniversalName(
            fileName, UNIVERSAL_NAME_INFO_LEVEL,
            pBuffer, ref bufferSize);

          if (NO_ERROR == nRet)
          {
            rni = (UNIVERSAL_NAME_INFO)Marshal.PtrToStructure(pBuffer,
              typeof(UNIVERSAL_NAME_INFO));
          }
        }
        finally
        {
          Marshal.FreeHGlobal(pBuffer);
        }
      }

      switch (nRet)
      {
        case NO_ERROR:
          return rni.lpUniversalName;

        case ERROR_NOT_CONNECTED:
          //Local file-name
          var shi = LocalShares;
          var share = shi?[fileName];
          var path = share?.Path;
          if (string.IsNullOrEmpty(path)) return fileName;
          var index = path.Length;
          if (Path.DirectorySeparatorChar != path[path.Length - 1])
            index++;

          fileName = index < fileName.Length ? fileName.Substring(index) : string.Empty;

          fileName = Path.Combine(share.ToString(), fileName);

          return fileName;

        default:
          Console.WriteLine("Unknown return value: {0}", nRet);
          return string.Empty;
      }
    }

    /// <summary>
    /// Returns the local <see cref="Share"/> object with the best match
    /// to the specified path.
    /// </summary>
    /// <param name="fileName"></param>
    public static Share PathToShare(string fileName)
    {
      if (string.IsNullOrEmpty(fileName)) return null;

      fileName = Path.GetFullPath(fileName);
      return !IsValidFilePath(fileName) ? null : LocalShares?[fileName];
    }

    #endregion

    #region Local shares

    /// <summary>The local shares</summary>
    private static ShareCollection m_local;

		/// <summary>
		/// Return the local shares
		/// </summary>
		public static ShareCollection LocalShares
			=> m_local ?? (m_local = new ShareCollection());

		/// <summary>
		/// Return the shares for a specified machine
		/// </summary>
		/// <param name="server"></param>
		public static ShareCollection GetShares(string server)
			=> new ShareCollection(server);

		#endregion

		#region Constructor

		/// <summary>
		/// Default constructor - local machine
		/// </summary>
		public ShareCollection()
    {
      Server = string.Empty;
      EnumerateShares(Server, this);
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="server"></param>
    public ShareCollection(string server)
    {
      Server = server;
      EnumerateShares(Server, this);
    }

		#endregion

		#region Add

		protected void Add(Share share)
			=> InnerList.Add(share);

		protected void Add(string netName, string path, ShareType shareType, string remark)
			=> InnerList.Add(new Share(Server, netName, path, shareType, remark));

		#endregion

		#region Properties

		/// <summary>
		/// Returns the name of the server this collection represents
		/// </summary>
		public string Server { get; }

		public bool IsReadOnly => throw new NotImplementedException();

		/// <summary>
		/// Returns the <see cref="Share"/> at the specified index.
		/// </summary>
		public Share this[int index]
			=> (Share)InnerList[index];

		/// <summary>
		/// Returns the <see cref="Share"/> which matches a given local path
		/// </summary>
		/// <param name="path">The path to match</param>
		public Share this[string path]
    {
      get
      {
        if (string.IsNullOrEmpty(path)) return null;

        path = Path.GetFullPath(path);
        if (!IsValidFilePath(path)) return null;

        Share match = null;

        foreach (var item in InnerList)
        {
          var s = (Share)item;

          if (!s.IsFileSystem || !s.MatchesPath(path)) continue;
          //Store first match
          if (match == null)
          {
            match = s;
          }
          // If this has a longer path,
          // and this is a disk share or match is a special share,
          // then this is a better match
          else if (match.Path.Length < s.Path.Length
                   && (ShareType.Disk == s.ShareType || ShareType.Disk != match.ShareType))
          {
            match = s;
          }
        }

        return match;
      }
    }

		#endregion

		#region Implementation of ICollection

		/// <summary>
		/// Copy this collection to an array
		/// </summary>
		/// <param name="array"></param>
		/// <param name="index"></param>
		public void CopyTo(Share[] array, int index)
			=> InnerList.CopyTo(array, index);

    #endregion
	}

  #endregion
}
