﻿using System;
using System.Linq;

namespace MrDir.Attributes
{
	[AttributeUsage(AttributeTargets.Property)]
	public class DataGridColumnAttribute
		: Attribute
	{
		public DataGridColumnAttribute(string key, bool useKeyAsHeader = true)
		{
			if (string.IsNullOrEmpty(key?.Trim()))
				throw new ArgumentException($"'{nameof(key)}' argument cannot be empty");
			Header = key;
		}

		public string Header { get; }

		public static DataGridColumnAttribute GetDataGridColumn<T>(string propertyName)
		{
			var prop = typeof(T).GetProperties().FirstOrDefault(x => x.Name == propertyName);
			if (prop == null) return null;

			var attributes = (DataGridColumnAttribute[])prop.GetCustomAttributes(typeof(DataGridColumnAttribute), false);
			return attributes.FirstOrDefault();
		}
	}
}