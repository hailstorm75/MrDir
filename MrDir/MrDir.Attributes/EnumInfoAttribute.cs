﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MrDir.Attributes
{
	[AttributeUsage(AttributeTargets.Field)]
	public class EnumInfoAttribute
		: DescriptionAttribute, IEquatable<EnumInfoAttribute>
	{
		public string Name { get; set; }

		public EnumInfoAttribute(string name = "", string description = "")
			: base(description)
		{
			Name = name;
		}

		public bool Equals(EnumInfoAttribute other)
			=> Name.Equals(other.Name) && Description.Equals(other.Description);
	}
}
