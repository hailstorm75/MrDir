﻿using System;
using System.Linq;

namespace MrDir.Attributes
{
  [AttributeUsage(AttributeTargets.Field)]
  public class NotificationMetadataAttribute
    : Attribute
  {
    public NotificationMetadataAttribute(string nameKey, string iconKey)
    {
      Name = nameKey;
      IconKey = iconKey;
    }

    public string Name { get; }
    public string IconKey { get; }

    public static Tuple<string, string> Metadata(Enum value)
    {
      var attributes = value.GetType()
        .GetField(value.ToString())
        .GetCustomAttributes(typeof(NotificationMetadataAttribute), false);
      if (!attributes.Any()) return new Tuple<string, string>("#Name", "#Icon");

      var result = attributes.First() as NotificationMetadataAttribute;
      return new Tuple<string, string>(result?.Name ?? string.Empty, result?.IconKey ?? string.Empty);
    }
  }
}